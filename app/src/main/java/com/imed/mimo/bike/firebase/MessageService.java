package com.imed.mimo.bike.firebase;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.imed.mimo.bike.R;
import com.imed.mimo.bike.activities.JoinActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;


public class MessageService extends FirebaseMessagingService {
    private static final String TAG = "_FcmMessageService";

    public static String token = "";
    public static final String NOTIFICATION_CHANNEL_ID = "122";

    @Override
    public void onNewToken(String s) {
        token = s;
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        try {
            Map<String, String> data = remoteMessage.getData();
            JSONObject jsonObject = new JSONObject(remoteMessage.getData());

//            if (isAppRunning) {
//                sendBroadcastForType(data.get(R.string.myType));
//            } else

        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    private void showNotification(JSONObject data, String type, String newSmth,Map<String, String> data1) {
        try {
            String title = data.getString("title");
            String message = data.getString("message");

            NotificationCompat.Builder builder = getNotificationBuilder(data1)
                    .setContentTitle(title)
                    .setContentText(message);
            Intent intent = new Intent(this, JoinActivity.class);
            intent.putExtra("type", type);
            PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentIntent(contentIntent);
            NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                NotificationChannel channel = new NotificationChannel("default",
//                        newSmth,
//                        NotificationManager.IMPORTANCE_DEFAULT);
//                channel.setDescription("Channel description");
//                mNotificationManager.createNotificationChannel(channel);

                NotificationChannel notificationChannel = getNotificationChannel(newSmth);
                builder.setChannelId(NOTIFICATION_CHANNEL_ID);
                mNotificationManager.createNotificationChannel(notificationChannel);
            }

//            Toast.makeText(getApplicationContext(), "new message", Toast.LENGTH_LONG).show();
            mNotificationManager.notify(message.hashCode(), builder.build());
        }
        catch (JSONException e){
            e.printStackTrace();
        }
    }

    private NotificationCompat.Builder getNotificationBuilder(Map<String, String> data) {
        String vibrate = data.get("vibrate");
        String sound = data.get("sound");
        String type = data.get("type");

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this,NOTIFICATION_CHANNEL_ID);

        builder.setSmallIcon(R.drawable.notif_logo);
        if ("1".equals(vibrate)) {
            builder.setVibrate(new long[]{500, 500, 500});
        }
        if ("1".equals(sound)) {
            builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        }
        builder.setAutoCancel(true);
        Intent intent = new Intent(this, JoinActivity.class);
        intent.putExtra("type", type);
        intent.putExtra("target", type);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);
        return builder;
    }

    private NotificationChannel getNotificationChannel(String channnelName) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channnelName, importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            return notificationChannel;
        }
        return null;
    }


}
