package com.imed.mimo.bike.listeners;

public interface ClusterListener {
    void itemClustered();
}
