package com.imed.mimo.bike.model

import com.google.gson.annotations.SerializedName

data class BindingModel(
    @SerializedName("currency")
    internal var currency: String,
    @SerializedName("deviceId")
    internal var deviceId: String,
    @SerializedName("locale")
    internal var locale: String,
    @SerializedName("userId")
    internal var userId: String
){
    @SerializedName("orderId")
   internal var orderId: String? = null

    @SerializedName("formUrl")
   internal var formUrl: String? = null
}

