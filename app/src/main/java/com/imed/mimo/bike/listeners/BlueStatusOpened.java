package com.imed.mimo.bike.listeners;

public interface BlueStatusOpened {
    void opened(boolean opened);
}
