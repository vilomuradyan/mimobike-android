package com.imed.mimo.bike.listeners;

import android.graphics.Bitmap;

public interface UpdateProfileListener {
    void updateProfile(String name, String surname, String birthday,String sex, String email,String plan);
}
