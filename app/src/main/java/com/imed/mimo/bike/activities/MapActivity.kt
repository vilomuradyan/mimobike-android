package com.imed.mimo.bike.activities

import android.Manifest
import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.app.ActivityManager
import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.*
import android.graphics.drawable.ColorDrawable
import android.location.Location
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.Network
import android.net.Uri
import android.os.*
import android.util.Base64
import android.util.Log
import android.view.*
import android.view.View.*
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.AnticipateOvershootInterpolator
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.graphics.BitmapCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.Target
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.maps.android.clustering.ClusterManager
import com.imed.mimo.bike.R
import com.imed.mimo.bike.activities.SplashActivity.Companion.activeOrNo
import com.imed.mimo.bike.activities.SplashActivity.Companion.bikeId
import com.imed.mimo.bike.activities.SplashActivity.Companion.bookRequested
import com.imed.mimo.bike.activities.SplashActivity.Companion.bookedBikeId
import com.imed.mimo.bike.activities.SplashActivity.Companion.bookedBikeLat
import com.imed.mimo.bike.activities.SplashActivity.Companion.bookedBikeLong
import com.imed.mimo.bike.activities.SplashActivity.Companion.bookedBikeTime
import com.imed.mimo.bike.activities.SplashActivity.Companion.callback
import com.imed.mimo.bike.activities.SplashActivity.Companion.calories
import com.imed.mimo.bike.activities.SplashActivity.Companion.debt
import com.imed.mimo.bike.activities.SplashActivity.Companion.distance_km
import com.imed.mimo.bike.activities.SplashActivity.Companion.freeMinutes
import com.imed.mimo.bike.activities.SplashActivity.Companion.inBook
import com.imed.mimo.bike.activities.SplashActivity.Companion.inRide
import com.imed.mimo.bike.activities.SplashActivity.Companion.isInSplash
import com.imed.mimo.bike.activities.SplashActivity.Companion.mode
import com.imed.mimo.bike.activities.SplashActivity.Companion.paired
import com.imed.mimo.bike.activities.SplashActivity.Companion.profileOk
import com.imed.mimo.bike.activities.SplashActivity.Companion.qrscanned
import com.imed.mimo.bike.activities.SplashActivity.Companion.rideBikeId
import com.imed.mimo.bike.activities.SplashActivity.Companion.trees_saved
import com.imed.mimo.bike.activities.SplashActivity.Companion.userId
import com.imed.mimo.bike.adapters.OwnIconRendered
import com.imed.mimo.bike.connections.Requests
import com.imed.mimo.bike.fragments.*
import com.imed.mimo.bike.helpers.*
import com.imed.mimo.bike.helpers.Constants.Companion.camera_permission
import com.imed.mimo.bike.helpers.Constants.Companion.camera_permission_passp
import com.imed.mimo.bike.helpers.Constants.Companion.internetLost
import com.imed.mimo.bike.helpers.Constants.Companion.mWebSocket
import com.imed.mimo.bike.helpers.Constants.Companion.reportFragmentCreated
import com.imed.mimo.bike.helpers.Constants.Companion.select_photo
import com.imed.mimo.bike.helpers.Constants.Companion.sharedRequestCode
import com.imed.mimo.bike.helpers.Constants.Companion.startedRide
import com.imed.mimo.bike.helpers.Utils.openKeyboard
import com.imed.mimo.bike.listeners.*
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_drawer.*
import kotlinx.android.synthetic.main.activity_map.*
import kotlinx.android.synthetic.main.bottom_fragment.*
import kotlinx.android.synthetic.main.custom_dialog_yes_no.view.*
import kotlinx.android.synthetic.main.nav_header_constraint.view.*
import me.grantland.widget.AutofitHelper
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.util.*
import kotlin.collections.ArrayList

@Suppress("DEPRECATION")
class MapActivity : AppCompatActivity(), OnMapReadyCallback,
    ActivityCompat.OnRequestPermissionsResultCallback,
    GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
    LocationListener {

  //  lateinit var animator: ObjectAnimator

    private lateinit var mGoogleApiClient: GoogleApiClient
    private var mLocationManager: LocationManager? = null
    private var mLocationX: Double = 0.0
    private var mLocationY: Double = 0.0
    private lateinit var mMap: GoogleMap
    private lateinit var mLocation: Location
    private lateinit var markerView: RelativeLayout
    private lateinit var markerImage: CircleImageView
    private var lastClicked: Marker? = null
    private var bookedOrServiceMarker: Marker? = null
    private lateinit var mClusterManager: ClusterManager<Bike>
    private lateinit var ownIconRendered: OwnIconRendered
    private lateinit var animation: ValueAnimator
    private lateinit var animationFade: Animation
    private lateinit var infoWindowContainer: View
    private lateinit var infoWindowLayoutListener: ViewTreeObserver.OnGlobalLayoutListener
    private var trackedPosition: LatLng? = null
    private var popupXOffset: Int = 0
    private var popupYOffset: Int = 0
    private var markerHeight: Int = 0
    @Suppress("DEPRECATION")
    private lateinit var overlayLayoutParams: AbsoluteLayout.LayoutParams
    private var handler: Handler? = null
    private var passpBase64 = ""
    private lateinit var positionUpdaterRunnable: Runnable
    private var mapClicked = false
    private lateinit var mBehavior: BottomSheetBehavior<LinearLayout>
    private var bookedOrService = false
    private var booked = false
    private var serviced = false
    private var collapsed = true
    private var bookedMarker = false
    private var inBookStatus = false
    private var bikeIsInBookOrService = false
    private lateinit var manager: ConnectivityManager
    private var openedMarkerId = ""
    private var oneTime = 1
    private var mapFragment: SupportMapFragment? = null
    private lateinit var profileFragment: ProfileFragment
    private lateinit var pricesFragment: PricesFragment
    private lateinit var cardFragment: CardFragment
    private lateinit var tripsFragment: TripsFragment
    private var reportFragment: ReportFragment? = null
    private var id = ""
    private var bookClickedId = ""
    private var ids = ArrayList<String>()
    private var idsToRemove = ArrayList<String>()
    private var bookedIds = ArrayList<String>()
    private var bikes = ArrayList<Bike>()
    private var oneRoutes = true
    private var loggedOut = false
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var onCreateRunned = false
    private var dialogRideInfo: Dialog? = null
    private lateinit var dialogConnect: Dialog
    private lateinit var stopbookDialog: Dialog
    private lateinit var noMarkerDialog: Dialog
    private var languageLocale = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //for file provider
        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())

        try {
            languageLocale =
                getSharedPreferences("mimo_user_data", Context.MODE_PRIVATE).getString("lang", "")!!
            if (languageLocale != "") {
                changeLanguage(languageLocale)
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        setContentView(R.layout.activity_drawer)
        scan_the_qr_code.isEnabled = false
        scan_the_qr_code.isClickable = false
        if (!getSharedPreferences("mimo_user_data", Context.MODE_PRIVATE).getBoolean("agreed", false)) {
            startActivity(Intent(this, AgreementActivity::class.java))
        }
        mWebSocket.changeContext(this)
        setLanguageImage()
        activeLayout()
        activeUserListener()
        getLocation()
        if (internetLost) {
            dialogConnect =
                Utils.showConnectionDialog(this, getString(R.string.you_are_offline_internet))
        }

        scanButton()
        openPrivacy()
        statusBarThings()
        navigationThings()
        profileButtonListener()
        taskManagerIcon()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            manager = Utils2.connectivityManager(networkCallback!!, this)
        }
        profileThings()
        updatePhotoListener()
        ameriaLinkListener()
        payDebtListener()
        payDebtStatusListener()
        ameriaAttachListener()
        reportLayout()
        updateUserError()
        completeProfile()
        Utils.isPermissionGranted(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Constants.location_permission,
            this
        )
        mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
        onCreateRunned = true
        isInSplash = false

    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap) {
        if (Utils.isPermissionGranted(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Constants.location_permission,
                this
            )
        ) {
            getLocation()
            googleMap.isMyLocationEnabled = true
            googleMap.uiSettings.isCompassEnabled = true
            googleMap.isTrafficEnabled = true
            googleMap.isBuildingsEnabled = true
            googleMap.uiSettings.isMapToolbarEnabled = false
            mMap = googleMap
            mapThings()
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
            fusedLocationClient.lastLocation.addOnSuccessListener { location: Location? ->
                try {
                    mLocationX = location!!.latitude
                    mLocationY = location.longitude
                    mLocation = location
                    mMap.moveCamera(
                        CameraUpdateFactory.newLatLngZoom(
                            LatLng(
                                mLocationX,
                                mLocationY
                            ), 18.0f
                        )
                    )
                } catch (e: Exception) {
                    e.printStackTrace()
                } catch (e: KotlinNullPointerException) {
                    e.printStackTrace()
                }
            }
            Requests.getBikes()
            noMarkerDialog = Utils.showConnectionDialog(this, getString(R.string.findingbikes))
            readListener()
            bikeBookListener()
            bikeStopBookListener()
            clusterMarkers()
            infoWindow()
            getFreeListener()
            inBookStatusListener("", false, -1)
            bookRideStatus()
            scanButtonCallback()
            markerListener()
            mapClicked()

            // created circle zone

//            mMap.addCircle(
//                CircleOptions()
//                    .center(LatLng(40.200096, 44.565191))
//                    .radius(2000.0)
//                    .strokeWidth(0f)
//                    .fillColor(Color.argb(120, 155, 235, 190))
//            )
//
//            mMap.addCircle(
//                CircleOptions()
//                    .center(LatLng(40.202652, 44.575509))
//                    .radius(2000.0)
//                    .strokeWidth(0f)
//                    .fillColor(Color.argb(120, 246, 172, 172))
//            )
//
//            mMap.addCircle(
//                CircleOptions()
//                    .center(LatLng(40.187808, 44.567938))
//                    .radius(1500.0)
//                    .strokeWidth(0f)
//                    .fillColor(Color.argb(120, 246, 247, 201))
//            )
        }
    }

    private fun getLoc() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        fusedLocationClient.lastLocation.addOnSuccessListener { location: Location? ->
            try {
                mLocationX = location!!.latitude
                mLocationY = location.longitude
                mLocation = location
            } catch (e: Exception) {
                e.printStackTrace()
            } catch (e: KotlinNullPointerException) {
                e.printStackTrace()
            }
        }
    }

    private fun navigationThings() {
        ring_button.setOnClickListener {
            Utils.showAlarmDialog(
                this,
                getString(R.string.booked_bike_alarm_message)
            )
            Requests.alarm(bookedBikeId)
        }
        go_to_maps.setOnClickListener {
            val intent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse(
                    "http://maps.google.com/maps?w=saddr="
                            + mLocationX + "," + mLocationY + "&daddr="
                            + bookedBikeLat + "," + bookedBikeLong
                )
            )
            startActivity(intent)
        }
        mimo_button.bringToFront()
        if (debt) {
            unpaid_trips_warn2.visibility = VISIBLE
        } else {
            unpaid_trips_warn2.visibility = GONE
        }
        paired = getSharedPreferences("mimo_user_data", Context.MODE_PRIVATE).getBoolean("paired", false)
        if (userId != "" && paired) {
            toolbar_drawer.visibility = VISIBLE
            toolbar_drawer.bringToFront()
            drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
            setSupportActionBar(toolbar_drawer)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowCustomEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
            val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar_drawer, R.string.open,
                R.string.close
            ).apply {
                //drawer_layout.addDrawerListener(this)
                this.syncState()
            }
            toggle.drawerArrowDrawable.color =
                ContextCompat.getColor(this@MapActivity, android.R.color.black)
            drawer_layout.addDrawerListener(object : DrawerLayout.DrawerListener {
                override fun onDrawerStateChanged(newState: Int) {
                }

                override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                }

                override fun onDrawerClosed(drawerView: View) {
                }

                override fun onDrawerOpened(drawerView: View) {
                    Requests.free()
                }
            })
            navHeaderClicks()
        } else {
            toolbar_drawer.visibility = GONE
            drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        }
    }

    private fun setLanguageImage() {
        if (languageLocale != "") {
            when (languageLocale) {
                "en" -> nav_view.getHeaderView(0).language_button.setImageResource(R.drawable.flag_america)
                "ru" -> nav_view.getHeaderView(0).language_button.setImageResource(R.drawable.flag_russian_federation)
                "hy" -> nav_view.getHeaderView(0).language_button.setImageResource(R.drawable.flag_armenia)
            }
        } else {
            when {
                Locale.getDefault().language == "en" -> nav_view.getHeaderView(0)
                    .language_button.setImageResource(R.drawable.flag_america)
                Locale.getDefault().language == "ru" -> nav_view.getHeaderView(0)
                    .language_button.setImageResource(R.drawable.flag_russian_federation)
                Locale.getDefault().language == "hy" -> nav_view.getHeaderView(0)
                    .language_button.setImageResource(R.drawable.flag_armenia)
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun navHeaderClicks() {
        try {
            nav_view.getHeaderView(0).button_free_minutes.text =
                freeMinutes.toString() + " " + getString(R.string._0_free_minutes)
            nav_view.getHeaderView(0).distance_text.text = String.format("%.1f", distance_km)
            nav_view.getHeaderView(0).trees_saved_text.text = trees_saved.toString()
            nav_view.getHeaderView(0).cal_text.text = String.format("%.1f", calories)
            if (debt) {
                nav_view.getHeaderView(0).unpaid_trips_warn.visibility = VISIBLE
                unpaid_trips_warn2.visibility = VISIBLE
            } else {
                nav_view.getHeaderView(0).unpaid_trips_warn.visibility = GONE
                unpaid_trips_warn2.visibility = GONE
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        AutofitHelper.create(nav_view.getHeaderView(0).textView1579)
        nav_view.getHeaderView(0).language_button.setOnClickListener {
            changeLanguageDropDown(nav_view.getHeaderView(0).language_button)
        }
        nav_view.getHeaderView(0).language_spinner.setOnClickListener {
            changeLanguageDropDown(nav_view.getHeaderView(0).language_spinner)
        }
    }

    private fun getFreeListener() {
        callback.getFreeListener(GetFreeListener {
            runOnUiThread {
                if (it) {
                    if (mode == "student") {
                        student_button.visibility = VISIBLE
                        student_button.bringToFront()
                    }
                } else {
                    nav_view.getHeaderView(0).button_free_minutes.text =
                        freeMinutes.toString() + " " + getString(R.string._0_free_minutes)
                    nav_view.getHeaderView(0).distance_text.text =
                        String.format("%.1f", distance_km)
                    nav_view.getHeaderView(0).trees_saved_text.text = trees_saved.toString()
                    nav_view.getHeaderView(0).cal_text.text = String.format("%.1f", calories)
                    if (debt) {
                        nav_view.getHeaderView(0).unpaid_trips_warn.visibility = VISIBLE
                        unpaid_trips_warn2.visibility = VISIBLE
                    } else {
                        nav_view.getHeaderView(0).unpaid_trips_warn.visibility = GONE
                        unpaid_trips_warn2.visibility = GONE
                    }
                }
            }
        })
    }

    @SuppressLint("RestrictedApi")
    private fun changeLanguageDropDown(imageView: ImageView) {
        try {
            val popup = PopupMenu(this@MapActivity, imageView)
            popup.inflate(R.menu.language_menu)
            popup.setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.america_flag -> {
                        if (languageLocale != "en") {
                            nav_view.getHeaderView(0)
                                .language_button.setImageResource(R.drawable.flag_america)
                            getSharedPreferences("mimo_user_data", Context.MODE_PRIVATE).edit().putString("lang", "en").apply()
                            changeLanguage("en")
                            finish()
                            overridePendingTransition(0, 0)
                            startActivity(intent)
                            overridePendingTransition(0, 0)
                            Requests.getProfile()
                        }
                        true
                    }
                    R.id.russia_flag -> {
                        if (languageLocale != "ru") {
                            nav_view.getHeaderView(0)
                                .language_button.setImageResource(R.drawable.flag_russian_federation)
                            getSharedPreferences("mimo_user_data", Context.MODE_PRIVATE).edit().putString("lang", "ru").apply()
                            changeLanguage("ru")
                            finish()
                            overridePendingTransition(0, 0)
                            startActivity(intent)
                            overridePendingTransition(0, 0)
                            Requests.getProfile()
                        }
                        true
                    }
                    R.id.armenia_flag -> {
                        if (languageLocale != "hy") {
                            nav_view.getHeaderView(0)
                                .language_button.setImageResource(R.drawable.flag_armenia)
                            getSharedPreferences("mimo_user_data", Context.MODE_PRIVATE).edit().putString("lang", "hy").apply()
                            changeLanguage("hy")
                            finish()
                            overridePendingTransition(0, 0)
                            startActivity(intent)
                            overridePendingTransition(0, 0)
                            Requests.getProfile()
                        }
                        true
                    }
                    else -> false
                }
            }
            popup.show()
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    private fun changeLanguage(string: String) {
        val locale = Locale(string)
        val config = Configuration()
        config.locale = locale
        baseContext.resources.updateConfiguration(config, baseContext.resources.displayMetrics)
    }

    private fun mapThings() {
        val parent =
            mapFragment?.view?.findViewWithTag<View>("GoogleMapMyLocationButton")!!.parent as ViewGroup
        val view = parent.getChildAt(4)
        val rlp = view.layoutParams as RelativeLayout.LayoutParams
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0)
        rlp.addRule(RelativeLayout.ALIGN_PARENT_END, RelativeLayout.TRUE)
        rlp.addRule(RelativeLayout.ALIGN_PARENT_START, 0)
        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            rlp.bottomMargin = 500
            rlp.rightMargin = 40
        } else {
            rlp.bottomMargin = 350
            rlp.rightMargin = 35
        }
        view.requestLayout()
        val parentView = map.view!!.findViewById<View>(Integer.parseInt("1")).parent as View
        val locButton = parentView.findViewById<View>(Integer.parseInt("2"))
        locButton.visibility = GONE
        buttonMyLocation.setOnClickListener {
            locButton?.callOnClick()
        }
        buttonMyLocation_icon.setOnClickListener {
            locButton?.callOnClick()
        }
        buttonMyLocation_button.setOnClickListener {
            locButton?.callOnClick()
        }
        buttonMyLocation_icon.background.setColorFilter(
            ContextCompat.getColor(
                this,
                R.color.gps_icon_color
            ), PorterDuff.Mode.SRC_ATOP
        )
        buttonMyLocation_icon.bringToFront()
        val params = buttonMyLocation.layoutParams as RelativeLayout.LayoutParams
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)
        params.addRule(RelativeLayout.ALIGN_PARENT_END)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            params.bottomMargin = 350
            params.rightMargin = 45
        } else {
            params.bottomMargin = 250
            params.rightMargin = 37
        }
    }

    private fun updatePhotoListener() {
        try {
            callback.photoListener(UpdatePhotoListener { bitmap ->
                runOnUiThread {
                    try {
                        getSharedPreferences("mimo_user_data", Context.MODE_PRIVATE).edit()
                            .putString("imagedata", Base64.encodeToString(bitmap, Base64.DEFAULT))
                            .apply()
                        try {
                            (supportFragmentManager.findFragmentByTag("fragment_profile") as ProfileFragment).updatePhoto(
                                bitmap!!
                            )
                        } catch (e: TypeCastException) {
                            e.printStackTrace()
                        }
                        try {
                            Glide.with(this)
                                .load(bitmap)
                                .override(Target.SIZE_ORIGINAL)
                                .into(nav_view.getHeaderView(0).biker_image_active)
                        } catch (e: java.lang.Exception) {
                            e.printStackTrace()
                        }
                    } catch (e: NullPointerException) {
                        e.printStackTrace()
                    }
                }
            })
        } catch (e: UninitializedPropertyAccessException) {
            e.printStackTrace()
        }
    }

    private fun ameriaLinkListener() {
        try {
            callback.evocaListener(EvocaLinkListener { str ->
                runOnUiThread {
                    try {
                        (supportFragmentManager.findFragmentByTag("card_fragment") as CardFragment).cancelDialog()
                        if (str != "error") {
                            EvocaFragment.newInstance(str, "attach", "")
                                .show(supportFragmentManager, "evoca_fragment")
                        } else {
                            Utils.showDialog(this, getString(R.string.unable_to_attach_card))
                        }
                    } catch (e: IllegalStateException) {
                        e.printStackTrace()
                    }
                }
            })
        } catch (e: UninitializedPropertyAccessException) {
            e.printStackTrace()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        }
    }

    private fun payDebtListener() {
        try {
            callback.evocaPayDebtListener(EvocaPayDebtListener { str, tripId ->
                runOnUiThread {
                    try {
                        if (str != "error") {
                            if (str != "") {
                                EvocaFragment.newInstance(str, "debt", tripId)
                                    .show(supportFragmentManager, "ameria_fragment")
                            } else {
                                Utils.showDialog(this, getString(R.string.unable_to_pay))
                            }
                        } else {
                            Utils.showDialog(this, getString(R.string.cannot_pay))
                        }
                    } catch (e: IllegalStateException) {
                        e.printStackTrace()
                    }
                }
            })
        } catch (e: UninitializedPropertyAccessException) {
            e.printStackTrace()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        }
    }

    private fun payDebtStatusListener() {
        try {
            callback.payDebtStatusListener(PayDebtStatusListener { status ->
                runOnUiThread {
                    try {
                        if (status == "OK") {
//                        try {
//                            (supportFragmentManager.findFragmentByTag("trips_fragment") as TripsFragment).close()
//                        } catch (e: TypeCastException) {
//                            e.printStackTrace()
//                        }
//                        tripsFragment = TripsFragment()
                            Requests.getTrips()
                            Requests.free()
//                        tripsFragment.show(supportFragmentManager, "trips_fragment")
                        } else {
                            Utils.showDialog(this, getString(R.string.unable_to_pay))
                        }
                    } catch (e: IllegalStateException) {
                        e.printStackTrace()
                    }
                }
            })
        } catch (e: UninitializedPropertyAccessException) {
            e.printStackTrace()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        }
    }

    private fun inBookStatusListener(it: String, closed: Boolean, duration: Int) {
        when {
            inRide -> {
                booked = false
                inBook = false
                bookedBikeId = ""
                bookedBikeLat = 0.0
                bookedBikeLong = 0.0
                booked_layout.visibility = INVISIBLE
                reserve_layout.visibility = INVISIBLE
                if (!startedRide) {
                    startedRide = true
                    startActivity(Intent(this, RideActivity::class.java))
                }
            }
            inBook -> {
                inBookStatus()
                scan_the_qr_code.isEnabled = true
                scan_the_qr_code.isClickable = true
            }
            else -> {
                scan_the_qr_code.isEnabled = true
                scan_the_qr_code.isClickable = true
            }
        }
        if (it == "none") {
            if (inBook) {
                openedMarkerId = bikeId
                booked = false
                inBook = false
                bookedBikeLat = 0.0
                bookedBikeLong = 0.0
                cancel_button_book.isEnabled = true
                cancel_button_book.isClickable = true
                cancel_button_book.alpha = 1f
                booked_layout.visibility = INVISIBLE
                reserve_layout.visibility = VISIBLE
                try {
                    stopbookDialog.cancel()
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
                markerListener()
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun inBookStatus() {
        bookedIds.add(id)
        bookedOrService = true
        val targetLocation = Location("")
        targetLocation.latitude = bookedBikeLat
        targetLocation.longitude = bookedBikeLong
        inRide = false
        booked = true
        cancelBook(bookedBikeId)
        if (!ids.contains(bookedBikeId)) {
            ids.add(bookedBikeId)
            bookedMarker = true
            addMarker(
                LatLng(
                    bookedBikeLat,
                    bookedBikeLong
                ), bookedBikeId
            )
        }
        infoWindowContainer.visibility = VISIBLE
        trackedPosition = LatLng(bookedBikeLat, bookedBikeLong)
        booked_layout.visibility = VISIBLE
        reserve_layout.visibility = INVISIBLE
        if (bookedBikeTime != 1) {
            less_than_text.text = getString(R.string.less_than) + " " + bookedBikeTime + " " + getString(R.string.minutes_left)
        } else {
            less_than_text.text = getString(R.string.less_than_a_minute_left)
        }
        try {
            fusedLocationClient.lastLocation.addOnSuccessListener { location: Location? ->
                try {
                    mLocationX = location!!.latitude
                    mLocationY = location.longitude
                } catch (e: Exception) {
                    e.printStackTrace()
                } catch (e: KotlinNullPointerException) {
                    e.printStackTrace()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: KotlinNullPointerException) {
            e.printStackTrace()
        }
    }

    fun completeProfile() {
        try {
            val sp = getSharedPreferences("mimo_user_data", Context.MODE_PRIVATE)
            val name = sp.getString("name", "")!!
            val surname = sp.getString("surname", "")!!
            val birthday = sp.getString("birthday", "")!!
            val email = sp.getString("email", "")!!
            if (name == "" || surname == "" || birthday == "" || email == "") {
                nav_view.getHeaderView(0).complete_profile_autofit.visibility = VISIBLE
                inactive_layout.visibility = VISIBLE
                active_layout.visibility = GONE
            } else {
                nav_view.getHeaderView(0).complete_profile_autofit.visibility = GONE
                inactive_layout.visibility = GONE
                active_layout.visibility = VISIBLE
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    private fun bookRideStatus() {
        try {
            callback.bookRideStatus(BookRideStatus { it, closed, duration ->
                runOnUiThread {
                    inBookStatusListener(it, closed, duration)
                }
            })
        } catch (e: UninitializedPropertyAccessException) {
            e.printStackTrace()
        }
    }

    private fun activeUserListener() {
        try {
            callback.activeUserListener(ActiveOrNotListener {
                runOnUiThread {
                    if (activeOrNo && profileOk) {
                        inactive_layout.visibility = GONE
                        active_layout.visibility = VISIBLE
                    } else {
                        inactive_layout.visibility = VISIBLE
                        active_layout.visibility = GONE
                    }
                    if (activeOrNo) {
                        nav_view.getHeaderView(0).complete_wallet_autofit.visibility = GONE
                    } else {
                        nav_view.getHeaderView(0).complete_wallet_autofit.visibility = VISIBLE
                    }
                    if (profileOk) {
                        nav_view.getHeaderView(0).complete_profile_autofit.visibility = GONE
                    } else {
                        nav_view.getHeaderView(0).complete_profile_autofit.visibility = VISIBLE
                    }
                }
            })
        } catch (e: UninitializedPropertyAccessException) {
            e.printStackTrace()
        }
    }

    private fun ameriaAttachListener() {
        try {
            callback.ameriaAttachListener(AmeraAttachedListener { attached, _ ->
                runOnUiThread {
                    if (attached) {
                        try {
                            try {
                                (supportFragmentManager.findFragmentByTag("card_fragment") as CardFragment).refresh(
                                    attached
                                )
                            } catch (e: TypeCastException) {
                                e.printStackTrace()
                            }
                            nav_view.getHeaderView(0).complete_header.visibility = GONE
                            Utils.showDialog(
                                this@MapActivity,
                                getString(R.string.your_card_accepted)
                            )
                            inactive_layout.visibility = GONE
                            active_layout.visibility = VISIBLE
                        } catch (e: java.lang.Exception) {
                            e.printStackTrace()
                        }
                    } else {
                        try {
                            (supportFragmentManager.findFragmentByTag("card_fragment") as CardFragment).refresh(
                                attached
                            )
                        } catch (e: TypeCastException) {
                            e.printStackTrace()
                        }
                        nav_view.getHeaderView(0).complete_header.visibility = VISIBLE
                        Utils.showDialog(
                            this@MapActivity,
                            getString(R.string.unable_to_attach_card)
                        )
                    }
                }
            })
        } catch (e: UninitializedPropertyAccessException) {
            e.printStackTrace()
        }
    }

    private fun bikeBookListener() {
        try {
            callback.bookListener(BookListener { description, bikeId, latlng, book ->
                runOnUiThread {
                    reserve_button.isEnabled = true
                    reserve_button.isClickable = true
                    if (book) {
                        less_than_text.text = getString(R.string.less_than_5_minutes_left)
                        openedMarkerId = bikeId
                        booked = true
                        booked_layout.visibility = VISIBLE
                        reserve_layout.visibility = INVISIBLE
                        cancelBook(bikeId)
                        try {
                            val targetLocation = Location("")
                            targetLocation.latitude = latlng.latitude
                            targetLocation.longitude = latlng.longitude
                        } catch (e: java.lang.IllegalArgumentException) {
                            e.printStackTrace()
                        }
                    } else {
                        cannotBook(description)
                    }
                }
            })
        } catch (e: UninitializedPropertyAccessException) {
            e.printStackTrace()
        }
    }

    private fun cancelBook(bikeId: String) {
        cancel_button_book.setOnClickListener {
            cancel_button_book.isEnabled = false
            cancel_button_book.isClickable = false
            cancel_button_book.alpha = 0.5f
            stopbookDialog =
                Utils.showConnectionDialog(this@MapActivity, getString(R.string.please_Wait))
            Requests.cancelBook(bikeId, mLocationX, mLocationY)
            Constants.bookStoped = true
        }
    }

    private fun updateUserError() {
        try {
            callback.updateUserError(UpdateUserError {
                runOnUiThread {
                    Utils.showDialog(this@MapActivity, getString(R.string.unable_to_update))
                }
            })
        } catch (e: UninitializedPropertyAccessException) {
            e.printStackTrace()
        }
    }

    private fun cannotBook(description: String) {
        reserve_button.isEnabled = true
        reserve_button.isClickable = true
        when (description) {
            "232" -> Utils.showDialog(this@MapActivity, getString(R.string.daily_limit_reached))
            "234" -> Utils.showDialog(this@MapActivity, getString(R.string.sorry_bike_service))
            "236" -> Utils.showDialog(this@MapActivity, getString(R.string.too_far_from_bike))
            "235" -> Utils.showDialog(this@MapActivity, getString(R.string.you_have_taken))
            "500" -> Utils.showDialog(this@MapActivity, getString(R.string.no_active_card_found))
            "501" -> Utils.showDialog(this@MapActivity, getString(R.string.unpaid_ride_with_this_card))
            "502" -> Utils.showDialog(this@MapActivity, getString(R.string.unpaid_ride_on_this_Device))
            else -> Utils.showDialog(this@MapActivity, getString(R.string.unable_to_book))
        }
    }

    private fun reportLayout() {
        val params = reported_layout.layoutParams as RelativeLayout.LayoutParams
        params.addRule(RelativeLayout.ALIGN_PARENT_END)
        params.rightMargin = 22
        report.setOnClickListener {
            if (!reportFragmentCreated) {
                reportFragment = ReportFragment()
                reportFragment!!.show(supportFragmentManager, "Report Bottom Sheet")
                fragmentManager?.executePendingTransactions()
            }
        }
        report_image.setOnClickListener {
            if (!reportFragmentCreated) {
                reportFragment = ReportFragment()
                reportFragment!!.show(supportFragmentManager, "Report Bottom Sheet")
                fragmentManager?.executePendingTransactions()
            }
        }
        reported_layout.setOnClickListener {
            if (!reportFragmentCreated) {
                reportFragment = ReportFragment()
                reportFragment!!.show(supportFragmentManager, "Report Bottom Sheet")
                fragmentManager?.executePendingTransactions()
            }
        }
    }

    fun openReportActivity(layout: String) {
        val intent = Intent(this, ReportActivity::class.java)
        intent.putExtra("layout", layout)
        startActivity(intent)
    }

    private fun openPrivacy() {
        privacy_policy.setOnClickListener {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("http://mimobike.com/privacy")))
        }
        how_to_use.setOnClickListener {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("http://store.mimobike.com/ssfmb/videos/L66212ihwxc2SjVUfhTL8LEM3usn9Mpe.mp4?fbclid=IwAR01L8uv08vwpAEOxfFBWmjopC_WEXXHrNgUELqlurK094ZHTE9DUqRmdOs")
                )
            )
        }
    }

    private fun bikeStopBookListener() {
        try {
            callback.bookStopListener(BookStopListener { bikeId ->
                runOnUiThread {
                    try {
                        openedMarkerId = bikeId
                        booked = false
                        inBook = false
                        bookedBikeLat = 0.0
                        bookedBikeLong = 0.0
                        cancel_button_book.isEnabled = true
                        cancel_button_book.isClickable = true
                        cancel_button_book.alpha = 1f
                        booked_layout.visibility = INVISIBLE
                        reserve_layout.visibility = VISIBLE
                        try {
                            stopbookDialog.cancel()
                        } catch (e: java.lang.Exception) {
                            e.printStackTrace()
                        }
                        markerListener()
                        animationFade.cancel()
                    } catch (e: UninitializedPropertyAccessException) {
                        e.printStackTrace()
                    }
                }
            })
        } catch (e: UninitializedPropertyAccessException) {
            e.printStackTrace()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun profileThings() {
        profileFragment = ProfileFragment()
        if (userId != "" && paired) {
            if (activeOrNo && profileOk) {
                inactive_layout.visibility = GONE
                active_layout.visibility = VISIBLE
            } else {
                inactive_layout.visibility = VISIBLE
                active_layout.visibility = GONE
            }
            if (activeOrNo) {
                nav_view.getHeaderView(0).complete_wallet_autofit.visibility = GONE
            } else {
                nav_view.getHeaderView(0).complete_wallet_autofit.visibility = VISIBLE
            }
            if (profileOk) {
                nav_view.getHeaderView(0).complete_profile_autofit.visibility = GONE
            } else {
                nav_view.getHeaderView(0).complete_profile_autofit.visibility = VISIBLE
            }
            logOut()
            verify()
            val bitmap = getSharedPreferences("mimo_user_data", Context.MODE_PRIVATE).getString("imagedata", "")
            if (bitmap != "") {
                val b = Base64.decode(bitmap, Base64.DEFAULT)
                val bip = BitmapFactory.decodeByteArray(b, 0, b.size)
                try {
                    nav_view.getHeaderView(0).biker_image_active.setImageBitmap(bip)
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun infoWindow() {
        infoWindowContainer = findViewById(R.id.container_popup)
        infoWindowLayoutListener = InfoWindowLayoutListener()
        book_text.text =
            getString(R.string.book_for_5_minutes_for_free)
        infoWindowContainer.viewTreeObserver.addOnGlobalLayoutListener(infoWindowLayoutListener)
        handler = Handler(Looper.getMainLooper())
        positionUpdaterRunnable = PositionUpdaterRunnable()
        handler!!.post(positionUpdaterRunnable)
    }

    override fun onDestroy() {
        super.onDestroy()
        try {
            @Suppress("DEPRECATION")
            infoWindowContainer.viewTreeObserver.removeGlobalOnLayoutListener(
                infoWindowLayoutListener
            )
            handler!!.removeCallbacks(positionUpdaterRunnable)
            handler = null
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    private fun taskManagerIcon() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                setTaskDescription(ActivityManager.TaskDescription("", R.drawable.logo))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun statusBarThings() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.navigationBarColor = ContextCompat.getColor(this, R.color.colorPrimary)
            window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimary)
        } else {
            window.setFlags(
                WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS
            )
            val statusBarHeight = Utils.getStatusBarHeight(this)
            val view = View(this)
            view.layoutParams = FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            view.layoutParams.height = statusBarHeight
            (window.decorView as ViewGroup).addView(view)
            view.background = resources.getDrawable(R.drawable.navibg)
            val layoutParams = CoordinatorLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
            layoutParams.setMargins(0, Utils.getStatusBarHeight(this), 0, 0)
            relat.layoutParams = layoutParams
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility =
                SYSTEM_UI_FLAG_LIGHT_STATUS_BAR or SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR
        }
    }

    @SuppressLint("InflateParams")
    override fun onBackPressed() {
        try {
            val adb = android.app.AlertDialog.Builder(this, R.style.AlertDialogCustom)
            val view = layoutInflater.inflate(R.layout.custom_dialog_yes_no, null) as RelativeLayout
            adb.setView(view)
            val dialog = adb.create()
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.show()
            val tvCount = view.findViewById<TextView>(R.id.dialogText)
            tvCount.text = getString(R.string.do_you_Want_to_leave)

//            val textname = view.findViewById<AppCompatTextView>(R.id.textName)
//            val sp = getSharedPreferences("mimo_user_data", Context.MODE_PRIVATE)
//            val name = sp.getString("name", "")!!
//            val textMessage = view.findViewById<AppCompatTextView>(R.id.textMessage)
//            val imageBike = view.findViewById<AppCompatImageView>(R.id.imageBike)
//            val greenZone = view.findViewById<CircleImageView>(R.id.green)
//            val redZone = view.findViewById<CircleImageView>(R.id.red)
//            var startWeight = 0f
//            var endWeight = 0f
//            textname.text = resources.getString(R.string.dear) + " $name,"
//            textMessage.text = resources.getString(R.string.text_zoning)
//
//            val viewTreeObservableRed = redZone.viewTreeObserver
//            viewTreeObservableRed.addOnGlobalLayoutListener {
//                val widthRed: Int = redZone.measuredWidth
//                startWeight = widthRed.toFloat()
//            }
//            val vewTreeObservable = greenZone.viewTreeObserver
//            vewTreeObservable.addOnGlobalLayoutListener{
//                val widthGreen: Int = greenZone.measuredWidth
//                endWeight = widthGreen.toFloat()
//            }
//
//            animator = ObjectAnimator.ofFloat(
//                imageBike,
//                "translationX",
//                -230f,
//                230f
//            )
//            animator.repeatCount = ValueAnimator.INFINITE
//            animator.interpolator = AccelerateDecelerateInterpolator()
//            animator.duration = 3000
//            animator.start()

            view.button_no.setOnClickListener {
                dialog.dismiss()
            }
            view.button_yes.setOnClickListener {
                dialog.dismiss()
                finish()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun mapClicked() {
        mMap.setOnMapClickListener {
            reserve_button.isEnabled = false
            reserve_button.isClickable = false
            if (!booked && !inRide) {
                infoWindowContainer.visibility = INVISIBLE
                if (inBookStatus) {
                    try {
                        Utils2.resetColorsOfMarker(
                            ownIconRendered.getMarker(
                                bikes[ids.indexOf(
                                    bookedBikeId
                                )]
                            ), this
                        )
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        e.printStackTrace()
                    } catch (e: IllegalStateException) {
                        e.printStackTrace()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    inBookStatus = false
                    bookedBikeId = ""
                }
                if (lastClicked != null && !mapClicked) {
                    reverseAnimation()
                    mapClicked = true
                }
            }
        }
    }

    private fun clusterMarkers() {
        try {
            mClusterManager = ClusterManager(this, mMap)
            mMap.setOnCameraIdleListener(mClusterManager)
            mMap.setOnMarkerClickListener(mClusterManager)
            ownIconRendered = OwnIconRendered(
                applicationContext,
                mMap,
                mClusterManager
            )
            mClusterManager.renderer = ownIconRendered
            mClusterManager.setOnClusterClickListener {
                Utils.animateZoomCluster(mMap, it)
                return@setOnClusterClickListener true
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    private val networkCallback = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        object : ConnectivityManager.NetworkCallback() {
            override fun onAvailable(network: Network) {
                super.onAvailable(network)
                try {
                    Log.i(
                        "vvvm",
                        "connected to " + if (manager.isActiveNetworkMetered) "LTE" else "WIFI"
                    )
                } catch (e: UninitializedPropertyAccessException) {
                    e.printStackTrace()
                }
                try {
                    if (internetLost) {
                        internetLost = false
                        try {
                            dialogConnect.cancel()
                        } catch (e: java.lang.IllegalArgumentException) {
                            e.printStackTrace()
                        }
                    }
                } catch (e: UninitializedPropertyAccessException) {
                    e.printStackTrace()
                }
            }

            override fun onLost(network: Network) {
                super.onLost(network)
                Log.i("vvv", "losing active connection")
                try {
                    dialogConnect =
                        Utils.showConnectionDialog(
                            this@MapActivity,
                            getString(R.string.you_are_offline_internet)
                        )
                    internetLost = true
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
        }
    } else {
        null
    }

    private fun scanButtonCallback() {
        Utils.scanButtonCallback {
            scan_the_qr_code.alpha = 1f
            scan_the_qr_code.isEnabled = true
            scan_the_qr_code.isClickable = true
            scan_the_qr_code.setOnClickListener {
                startActivity(Intent(this, BluetoothActivity::class.java))
            }
        }
    }

    override fun onResume() {
        super.onResume()
            if (Constants.isRating) {
                Handler().postDelayed({
                    Constants.isRating = false
                    showRating()

                }, 1500)
            }
        bookRideStatus()
        mWebSocket.changeContext(this)
        try {
            noMarkerDialog.cancel()
        } catch (e: Exception) {
        }
        try {
            dialogRideInfo?.cancel()
            dialogRideInfo = null
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
//            if (mBehavior.state == BottomSheetBehavior.STATE_COLLAPSED) {
            if (userId != "" && paired) {
                complete_profile_layout.visibility = VISIBLE
                if (activeOrNo && profileOk) {
                    inactive_layout.visibility = GONE
                    active_layout.visibility = VISIBLE
                } else {
                    inactive_layout.visibility = VISIBLE
                    active_layout.visibility = GONE
                }
                if (activeOrNo) {
                    nav_view.getHeaderView(0).complete_wallet_autofit.visibility = GONE
                } else {
                    nav_view.getHeaderView(0).complete_wallet_autofit.visibility = VISIBLE
                }
                if (profileOk) {
                    nav_view.getHeaderView(0).complete_profile_autofit.visibility = GONE
                } else {
                    nav_view.getHeaderView(0).complete_profile_autofit.visibility = VISIBLE
                }
            }
//            }
        } catch (e: UninitializedPropertyAccessException) {
            e.printStackTrace()
        }
    }

    private fun showRating(){
        try {
            val adb = android.app.AlertDialog.Builder(this, R.style.AlertDialogCustom)
            val view = layoutInflater.inflate(R.layout.custom_dialog_rating, null) as ConstraintLayout
            adb.setView(view)
            val dialog = adb.create()
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.show()
            val closeButton = view.findViewById<AppCompatImageView>(R.id.buttonClose)
            val playStoreButton = view.findViewById<AppCompatImageView>(R.id.buttonPlayStore)
            val appStoreButton = view.findViewById<AppCompatImageView>(R.id.buttonAppStore)
            val faceBookButton = view.findViewById<AppCompatImageView>(R.id.imageFacebook)
            val instagramButton = view.findViewById<AppCompatImageView>(R.id.imageInstagram)
            val youtubeButton = view.findViewById<AppCompatImageView>(R.id.imageYoutube)
            youtubeButton.setOnClickListener{
                referLink(Constants.YOUTUBE_LINK)
            }
            instagramButton.setOnClickListener{
                referLink(Constants.INSTAGRAM_LINK)
            }
            faceBookButton.setOnClickListener{
                referLink(Constants.FACEBOOK_LINK)
            }
            appStoreButton.setOnClickListener{
                referLink(Constants.APP_STORE)
            }
            playStoreButton.setOnClickListener{
                referLink(Constants.PLAY_STORE)
            }
            closeButton.setOnClickListener{
                dialog.dismiss()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun referLink(link : String){
        when(link){
            Constants.PLAY_STORE -> {
                val appPackageName = packageName
                try {
                    startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$appPackageName")))
                } catch (e: ActivityNotFoundException) {
                    startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")))
                }
            }
            else ->{
                try {
                    startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(link)))
                } catch (e: ActivityNotFoundException) {
                    e.printStackTrace()
                }
            }
        }
    }

    private fun readListener() {
        try {
            callback.bikesListener(BikesListener { jsonString ->
                runOnUiThread {
                    val json = JSONObject(jsonString)
                    val jsonArray = json.getJSONArray("bikes")
                    if (jsonArray.length() > 0) {
                        try {
                            noMarkerDialog.cancel()
                        } catch (e: UninitializedPropertyAccessException) {
                            e.printStackTrace()
                        }
                        for (i in 0 until jsonArray.length()) {
                            idsToRemove.add(jsonArray.getJSONObject(i).getString("bike_id"))
                            when {
                                !inBook && !inRide -> try {
                                    if (jsonArray.getJSONObject(i).has("bike_id")) {
                                        id = jsonArray.getJSONObject(i).getString("bike_id")
                                        if (!ids.contains(id) && id != "") {
                                            ids.add(id)
                                            addMarkerFreeBooked(json, i, id)
                                        } else {
                                            animateExistingMarker(json, i, id)
                                        }
                                    }
                                } catch (e: org.json.JSONException) {
                                    if (!qrscanned) {
                                        mClusterManager.clearItems()
                                        mMap.clear()
                                        ids = ArrayList()
                                        bikes = ArrayList()
                                        e.printStackTrace()
                                        qrscanned = true
                                    }
                                }
                                inBook -> inBookBikes(json, i)
                            }
                        }
                        removeNonExistingMarkers()
                    }
                }
            })
        } catch (e: UninitializedPropertyAccessException) {
            e.printStackTrace()
        }
    }

    private fun removeNonExistingMarkers() {
        if (!inRide) {
            try {
                val removeId = ArrayList(ids)
                removeId.removeAll(idsToRemove)
                for (i2 in 0 until removeId.size) {
                    if (removeId[i2] != rideBikeId && !qrscanned) {
                        try {
                            ownIconRendered.getMarker(bikes[ids.indexOf(removeId[i2])]).remove()
                        } catch (e: IndexOutOfBoundsException) {
                            e.printStackTrace()
                        }
                        try {
                            bikes.remove(bikes[i2])
                        } catch (e: java.lang.NullPointerException) {
                            e.printStackTrace()
                        }
                    }
                }
                ids.removeAll(removeId)
                removeId.clear()
                idsToRemove.clear()
                mClusterManager.cluster()
            } catch (e: NullPointerException) {
                e.printStackTrace()
            } catch (e: IndexOutOfBoundsException) {
                e.printStackTrace()
            }
        }
    }

    fun cancelAmeriaDialog() {
        try {
            (supportFragmentManager.findFragmentByTag("card_fragment") as CardFragment).cancelDialog()
        } catch (e: TypeCastException) {
            e.printStackTrace()
        }
    }

    private fun addMarkerFreeBooked(json: JSONObject, i: Int, id: String) {
        bookedOrService = false
        bookedMarker = false
        if (!inBook) {
            addMarker(
                LatLng(
                    json.getJSONArray("bikes").getJSONObject(i).getDouble("bike_x"),
                    json.getJSONArray("bikes").getJSONObject(i).getDouble("bike_y")
                ), id
            )
        }
    }

    @SuppressLint("SetTextI18n")
    private fun animateExistingMarker(json: JSONObject, i: Int, id: String) {
        try {
            val marker = ownIconRendered.getMarker(bikes[ids.indexOf(id)])
            val latLng = LatLng(
                json.getJSONArray("bikes").getJSONObject(i).getDouble("bike_x"),
                json.getJSONArray("bikes").getJSONObject(i).getDouble("bike_y")
            )
            bookedOrService = false
            try {
                Utils.animateMarker(marker, latLng, mMap)
            } catch (e: java.lang.Exception) {
//                    e.printStackTrace()
            }
            if (!inBook) {
                if (id == openedMarkerId) {
                    trackedPosition = latLng
                }
            }
            try {
                mClusterManager.cluster()
            } catch (e: OutOfMemoryError) {
                e.printStackTrace()
            }
            animateMarkerCases(marker)
        } catch (e: java.lang.Exception) {
//            e.printStackTrace()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun animateMarkerCases(marker: Marker) {
        try {
            marker.alpha = 1f
        } catch (e: java.lang.IllegalStateException) {
        }
        reserve_button.alpha = 1f
        reserve_button.isEnabled = true
        reserve_button.isClickable = true
        scanButton()
        book_text.text =
            getString(R.string.book_for_5_minutes_for_free)
        if (!inBook && !inRide) {
            if (bookedIds.contains(id)) {
                bookedIds.remove(id)
            }
        }
    }

    private fun inBookBikes(json: JSONObject, i: Int) {
        try {
            if (json.getJSONArray("bikes").getJSONObject(i).getString("bike_id") == bookedBikeId) {
                if (json.getJSONArray("bikes").getJSONObject(i).getDouble("bike_x") != bookedBikeLat ||
                    json.getJSONArray("bikes").getJSONObject(i).getDouble("bike_y") != bookedBikeLong
                ) {
                    bookedBikeLat = json.getJSONArray("bikes").getJSONObject(i).getDouble("bike_x")
                    bookedBikeLong = json.getJSONArray("bikes").getJSONObject(i).getDouble("bike_y")
                    val marker = ownIconRendered.getMarker(bikes[ids.indexOf(bookedBikeId)])
                    val latLng = LatLng(bookedBikeLat, bookedBikeLong)
                    Utils.animateMarker(marker, latLng, mMap)
                    trackedPosition = latLng
                    mClusterManager.cluster()
                    infoWindowContainer.visibility = VISIBLE
                }
            } else {
                val id = json.getJSONArray("bikes").getJSONObject(i).getString("bike_id")
                if (!ids.contains(id)) {
                    if (id != "" && id != bookedBikeId) {
                        ids.add(id)
                        addMarkerFreeBooked(json, i, id)
                    }
                } else {
                    animateExistingMarker(json, i, id)
                }
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    @SuppressLint("InflateParams", "SetTextI18n")
    private fun markerListener() {
        val inflater = LayoutInflater.from(this)
        val view1 = inflater.inflate(R.layout.marker_view, null, false)
        val markerView = view1.findViewById<RelativeLayout>(R.id.markerView)
        val markerImage = view1.findViewById<CircleImageView>(R.id.imageViewMarker)
        val markerLayout = view1.findViewById<RelativeLayout>(R.id.markerLayout)
        val markerShadow = view1.findViewById<RelativeLayout>(R.id.shadow_pin)
        mClusterManager.markerCollection.setOnMarkerClickListener {
            try {
                if (!inRide && !inBook) {
                    if (lastClicked != it) {
                        if (lastClicked != null && !mapClicked) {
                            reverseAnimation()
                            infoWindowContainer.visibility = INVISIBLE
                        }
                        var newBitMap: Bitmap?
                        animation = ValueAnimator.ofFloat(1F, 1.3F)
                        animation.duration = 400
                        animation.interpolator = AnticipateOvershootInterpolator()
                        animation.addUpdateListener { animationState ->
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                markerLayout.backgroundTintList =
                                    ContextCompat.getColorStateList(this, android.R.color.white)
                                markerImage.backgroundTintList =
                                    ContextCompat.getColorStateList(this, R.color.colorBlack)
                                markerShadow.backgroundTintList =
                                    ContextCompat.getColorStateList(this, R.color.colorBlack)
                            } else {
                                markerLayout.background.setColorFilter(
                                    ContextCompat.getColor(
                                        this,
                                        android.R.color.white
                                    ), PorterDuff.Mode.SRC_ATOP
                                )
                                markerImage.background.setColorFilter(
                                    ContextCompat.getColor(
                                        this,
                                        android.R.color.black
                                    ), PorterDuff.Mode.SRC_ATOP
                                )
                                markerShadow.background.setColorFilter(
                                    ContextCompat.getColor(
                                        this,
                                        android.R.color.black
                                    ), PorterDuff.Mode.SRC_ATOP
                                )
                            }
                            val scaleFactor = animationState.animatedValue as Float
                            newBitMap = Bitmap.createScaledBitmap(
                                Utils.createBitmapFromLayout(markerView),
                                (markerView.width * scaleFactor).toInt(),
                                (markerView.height * scaleFactor).toInt(),
                                true
                            )
                            try {
                                it.setIcon(BitmapDescriptorFactory.fromBitmap(newBitMap))
                            } catch (e: IllegalArgumentException) {
                                e.printStackTrace()
                            }
                        }
                        animation.addListener(object : AnimatorListenerAdapter() {
                            override fun onAnimationEnd(animation: Animator?) {
                                if (!mapClicked) {
                                    lastClicked = it
                                }
                            }
                        })
                        trackedPosition = it.position
                        infoWindowContainer.visibility = VISIBLE
                        if (bookedIds.contains(ownIconRendered.getClusterItem(it).id1)) {
                            bookedOrServiceMarker = it
                            it.alpha = 1f
                            it.alpha = 0.5f
                            reserve_button.alpha = 0.5f
                            reserve_button.isEnabled = false
                            reserve_button.setOnClickListener {
                            }
                            reserve_button.isClickable = false
                            if (serviced) {
                                book_text.text = getString(R.string.bike_serviced)
                                serviced = false
                            } else {
                                book_text.text = getString(R.string.bike_booked)
                            }
                            bikeIsInBookOrService = true
                        } else {
                            scanButton()
                            it.alpha = 1f
                            reserve_button.alpha = 1f
                            reserve_button.isEnabled = true
                            reserve_button.isClickable = true
                            book_text.text =
                                getString(R.string.book_for_5_minutes_for_free)
                            bikeIsInBookOrService = false
                        }
                        openedMarkerId = ownIconRendered.getClusterItem(it).id1
                        bookClickedId = ownIconRendered.getClusterItem(it).id1
                        reserve_layout.visibility = VISIBLE
                        infoWindowContainer.startAnimation(
                            AnimationUtils.loadAnimation(
                                this,
                                R.anim.scale
                            )
                        )
                        animation.start()
                        mapClicked = false
                    }
                }
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
            true
        }
    }

    @SuppressLint("InflateParams")
    private fun reverseAnimation() {
        try {
            animation.reverse()
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        val inflater = LayoutInflater.from(this@MapActivity)
        val view1 = inflater.inflate(R.layout.marker_view, null, false)
        val markerView = view1.findViewById<RelativeLayout>(R.id.markerView)
        val markerLayout = view1.findViewById<RelativeLayout>(R.id.markerLayout)
        val markerShadow = view1.findViewById<RelativeLayout>(R.id.shadow_pin)
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            markerLayout.background.setColorFilter(
                ContextCompat.getColor(
                    this@MapActivity,
                    R.color.colorPrimary
                ), PorterDuff.Mode.SRC_ATOP
            )
            markerImage.background.setColorFilter(
                ContextCompat.getColor(
                    this@MapActivity,
                    android.R.color.black
                ), PorterDuff.Mode.SRC_ATOP
            )
            markerShadow.background.setColorFilter(
                ContextCompat.getColor(
                    this@MapActivity,
                    android.R.color.black
                ), PorterDuff.Mode.SRC_ATOP
            )
        }
        val newBitMap = Bitmap.createScaledBitmap(
            Utils.createBitmapFromLayout(markerView),
            markerView.width, markerView.height, false
        )
        try {
            animation.addListener(object : AnimatorListenerAdapter() {
                @SuppressLint("InflateParams")
                override fun onAnimationEnd(animation: Animator?) {
                    try {
                        lastClicked!!.setIcon(BitmapDescriptorFactory.fromBitmap(newBitMap))
                        if (bookedOrService || bikeIsInBookOrService) {
                            bookedOrServiceMarker!!.alpha = 0.5f
                            bikeIsInBookOrService = false
                        }
                    } catch (e: Exception) {
                    }
                    if (mapClicked) {
                        addEmptyMarker()
                    }
                }
            })
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        infoWindowContainer.startAnimation(AnimationUtils.loadAnimation(this, R.anim.scale_out))
    }

    override fun onLocationChanged(location1: Location) {
        mLocationX = location1.latitude
        mLocationY = location1.longitude
    }

    private fun getLocation() {
        mGoogleApiClient = GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build()
        if (!checkLocation()) {
            Utils2.enableLoc(mGoogleApiClient, this)
        }
        mLocationManager = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    }

    @SuppressLint("MissingPermission")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        var image: Bitmap?
        when (requestCode) {
            199 -> {
                val fusedLocationProviderClient:
                        FusedLocationProviderClient =
                    LocationServices.getFusedLocationProviderClient(this)
                fusedLocationProviderClient.lastLocation
                    .addOnSuccessListener(this) { location ->
                        if (location != null) {
                            mLocation = location
                            mLocationX = mLocation.latitude
                            mLocationY = mLocation.longitude
                            mMap.moveCamera(
                                CameraUpdateFactory.newLatLngZoom(
                                    LatLng(
                                        mLocationX,
                                        mLocationY
                                    ), 18.0f
                                )
                            )
                        }
                    }
            }
            select_photo -> {
                try {
                    val selectedImage = data!!.data
                    val rotation =
                        Utils.getRotation(applicationContext, selectedImage, false).toFloat()
                    val bm = Utils.decodeBitmap(applicationContext, selectedImage)
                    image = Utils.rotate(bm, rotation) as Bitmap
                    sendChangeAvatarRequest(image)
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
            sharedRequestCode -> {
                try {
                    (supportFragmentManager.findFragmentByTag("trip_route_fragment") as TripRouteFragment).reverseLayouts()
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
            camera_permission -> {
                try {
                    image = data?.extras!!.get("data") as Bitmap
                    sendChangeAvatarRequest(image)
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
            camera_permission_passp -> {
                try {
                    val imageFile = Utils.getTemporalFile()
                    val uri = PhotoProvider.getPhotoUri(imageFile)
                    val rotation = Utils.getRotation(applicationContext, uri, true).toFloat()
                    image = Utils.decodeBitmap(applicationContext, uri)
                    image = Utils.rotate(image, rotation) as Bitmap
                    val stream = ByteArrayOutputStream()
                    image.compress(Bitmap.CompressFormat.JPEG, 80, stream)
                    val bitmapByteCount = BitmapCompat.getAllocationByteCount(image)
                    passpBase64 = Base64.encodeToString(stream.toByteArray(), Base64.DEFAULT)
                    if (passpBase64 != "") {
                        try {
                            (supportFragmentManager.findFragmentByTag("card_fragment") as CardFragment).passpAttached(passpBase64)
                        } catch (e: TypeCastException) {
                            e.printStackTrace()
                        }
                    }
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        activeLayout()
        if (Utils.isPermissionGranted(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Constants.location_permission,
                this
            )
        ) {
            mGoogleApiClient.connect()
        }
    }

    override fun onRestart() {
        super.onRestart()
        activeLayout()
    }

    private fun activeLayout() {
        try {
            if (userId != "" && paired) {
                complete_profile_layout.visibility = VISIBLE
                if (activeOrNo && profileOk) {
                    inactive_layout.visibility = GONE
                    active_layout.visibility = VISIBLE
                } else {
                    inactive_layout.visibility = VISIBLE
                    active_layout.visibility = GONE
                }
                if (activeOrNo) {
                    nav_view.getHeaderView(0).complete_wallet_autofit.visibility = GONE
                } else {
                    nav_view.getHeaderView(0).complete_wallet_autofit.visibility = VISIBLE
                }
                if (profileOk) {
                    nav_view.getHeaderView(0).complete_profile_autofit.visibility = GONE
                } else {
                    nav_view.getHeaderView(0).complete_profile_autofit.visibility = VISIBLE
                }
            }
        } catch (e: UninitializedPropertyAccessException) {
            e.printStackTrace()
        }
    }

    override fun onStop() {
        super.onStop()
        try {
            if (mGoogleApiClient.isConnected) {
                mGoogleApiClient.disconnect()
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    private fun checkLocation(): Boolean {
        return isLocationEnabled()
    }

    private fun isLocationEnabled(): Boolean {
        mLocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return mLocationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER) || mLocationManager!!.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    override fun onConnected(p0: Bundle?) {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        val fusedLocationProviderClient:
                FusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        fusedLocationProviderClient.lastLocation
            .addOnSuccessListener(this) { location ->
                if (location != null) {
                    mLocation = location
                    mLocationX = mLocation.latitude
                    mLocationY = mLocation.longitude
                    if (inBook) {
                        if (oneRoutes) {
                            try {
                                val targetLocation = Location("")
                                targetLocation.latitude = bookedBikeLat
                                targetLocation.longitude = bookedBikeLong
                            } catch (e: java.lang.IllegalArgumentException) {
                                e.printStackTrace()
                            }
                            oneRoutes = false
                        }
                    }
                }
            }
    }

    override fun onConnectionSuspended(p0: Int) {
        Log.i("tag", " Suspended")
        mGoogleApiClient.connect()
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        Log.i("tag", "Connection failed. Error: " + p0.errorCode)
    }

    override fun onPause() {
        super.onPause()
        onCreateRunned = false
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == Constants.location_permission) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                finish()
                overridePendingTransition(0, 0)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        } else if (requestCode == camera_permission) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Utils2.imageCapture(this)
            }
        } else if (requestCode == camera_permission_passp) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Utils2.imageCapturePassport(this)
            }
        } else if (requestCode == Constants.storage_permission) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Utils2.imagePicker(this)
            }
        } else if (requestCode == Constants.storage_permission_route) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                (supportFragmentManager.findFragmentByTag("trip_route_fragment") as TripRouteFragment).share()
            }
        } else if (requestCode == Constants.call_permission) {
            try {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Utils2.call(this)
                }
            } catch (e: ArrayIndexOutOfBoundsException) {
                e.printStackTrace()
            }
        }
    }

    private fun scanButton() {
        reserve_button.setOnClickListener {
            getLoc()
            if (activeOrNo) {
                nav_view.getHeaderView(0).complete_header.visibility = GONE
                reserve_button.isEnabled = false
                reserve_button.isClickable = false
                Requests.book(bookClickedId, mLocationX, mLocationY)
            } else {
                nav_view.getHeaderView(0).complete_header.visibility = VISIBLE
                Utils.showDialog(this@MapActivity, getString(R.string.you_wont_be_Able_to_use))
            }
        }
    }

    private fun profileButtonListener() {
        mBehavior = BottomSheetBehavior.from(bottom_sheet_parent)
        join_mimo.setOnClickListener {
            mBehavior.peekHeight = 1100
            phoneLayout.visibility = VISIBLE
            join_mimo.visibility = GONE
            openKeyboard(applicationContext)
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
            phone_edit_text.requestFocus()
        }
        checkNumberListener()
        sendRegisterClient()
        getCountryCode()
        verifyLoginListener()
        verifyButton()
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_DOWN) {
            val v = currentFocus
            if (v is EditText) {
                val outRect = Rect()
                v.getGlobalVisibleRect(outRect)
                if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
                    v.clearFocus()
                    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0)
                }
            }
        }
        return super.dispatchTouchEvent(event)
    }

    @SuppressLint("SetTextI18n")
    private fun sendRegisterClient() {
        continue_button.setOnClickListener {
            continue_button.alpha = 0.5f
            continue_button.isEnabled = false
            continue_button.isClickable = false
//            Requests.sendSms(phone_edit_text.text.toString(), this@MapActivity)
            Requests.enter(mLocationX, mLocationY, phone_edit_text.text.toString())
            try {
                goVerifyListener()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun goVerifyListener() {
        callback.verifyBindListener(GoVerifyListner {
            runOnUiThread {
                mBehavior.peekHeight = 1200
                phoneLayout.visibility = GONE
                verifyLayout.visibility = VISIBLE
                phoneNumber.text = phone_edit_text.text.toString()
                editText6.requestFocus()
                sendAgainTimer()
            }
        })
    }

    private fun verifyLoginListener() {
        try {
            callback.bindListener(VerifyLoginListener {
                runOnUiThread {
                    verify()
                    logOut()
                }
            })
        } catch (e: UninitializedPropertyAccessException) {
            e.printStackTrace()
        }
    }

    fun scanClicked(view: View) {
        if (!inRide)
            startActivity(Intent(this@MapActivity, BluetoothActivity::class.java))
    }

    private fun verify() {
        navigationThings()
        if (userId != "" && paired) {
            complete_profile_layout.visibility = VISIBLE
            if (activeOrNo && profileOk) {
                inactive_layout.visibility = GONE
                active_layout.visibility = VISIBLE
            } else {
                inactive_layout.visibility = VISIBLE
                active_layout.visibility = GONE
            }
            if (activeOrNo) {
                nav_view.getHeaderView(0).complete_wallet_autofit.visibility = GONE
            } else {
                nav_view.getHeaderView(0).complete_wallet_autofit.visibility = VISIBLE
            }
            if (profileOk) {
                nav_view.getHeaderView(0).complete_profile_autofit.visibility = GONE
            } else {
                nav_view.getHeaderView(0).complete_profile_autofit.visibility = VISIBLE
            }
        }
        phone_number.visibility = GONE
        mBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(p0: View, p1: Float) {
                if (oneTime == 1) {
                    Utils.hideKeyboard(this@MapActivity)
                    oneTime = 2
                }
            }

            override fun onStateChanged(p0: View, p1: Int) {
                if (mBehavior.state == BottomSheetBehavior.STATE_EXPANDED) {
                    oneTime = 1
                    complete_profile_layout.visibility = GONE
                } else if (mBehavior.state == BottomSheetBehavior.STATE_COLLAPSED) {
                    oneTime = 1
                    collapsed = true
                    complete_profile_layout.visibility = VISIBLE
                    if (activeOrNo && profileOk) {
                        inactive_layout.visibility = GONE
                        active_layout.visibility = VISIBLE
                    } else {
                        inactive_layout.visibility = VISIBLE
                        active_layout.visibility = GONE
                    }
                    if (activeOrNo) {
                        nav_view.getHeaderView(0).complete_wallet_autofit.visibility = GONE
                    } else {
                        nav_view.getHeaderView(0).complete_wallet_autofit.visibility = VISIBLE
                    }
                    if (profileOk) {
                        nav_view.getHeaderView(0).complete_profile_autofit.visibility = GONE
                    } else {
                        nav_view.getHeaderView(0).complete_profile_autofit.visibility = VISIBLE
                    }
                }
            }
        })
    }

    @SuppressLint("SimpleDateFormat")
    fun sendChangeAvatarRequest(bitmap: Bitmap?) {
        val newBitmap = Utils.cropImage(bitmap)
        val stream = ByteArrayOutputStream()
        newBitmap!!.compress(Bitmap.CompressFormat.JPEG, 40, stream)
        (supportFragmentManager.findFragmentByTag("fragment_profile") as ProfileFragment).updatePhoto(
            stream.toByteArray()
        )
        nav_view.getHeaderView(0).biker_image_active.setImageBitmap(newBitmap)
        try {
            Requests.uploadPhoto("avatar", stream.toByteArray())
        } catch (e: UninitializedPropertyAccessException) {
            e.printStackTrace()
        }
        getSharedPreferences("mimo_user_data", Context.MODE_PRIVATE).edit()
            .putString("imagedata", Base64.encodeToString(stream.toByteArray(), Base64.DEFAULT))
            .apply()
    }

    @SuppressLint("InflateParams")
    private fun logOut() {
        logout_click.setOnClickListener {
            try {
                val adb = android.app.AlertDialog.Builder(this, R.style.AlertDialogCustom)
                val view =
                    layoutInflater.inflate(R.layout.custom_dialog_yes_no, null) as RelativeLayout
                adb.setView(view)
                val dialog = adb.create()
                dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                dialog.show()
                val tvCount = view.findViewById<TextView>(R.id.dialogText)
                tvCount.text = getString(R.string.are_you_sure_logout)
                view.button_no.setOnClickListener {
                    dialog.dismiss()
                }
                view.button_yes.setOnClickListener {
                    getSharedPreferences("mimo_user_data", Context.MODE_PRIVATE).edit()
                        .putString("phone", "")
//                        .putString("activeOrI", "")
                        .putBoolean("paired", false)
                        .putString("name", "")
                        .putString("surname", "")
                        .putString("birthday", "")
                        .putString("sex", "")
                        .putString("email", "")
                        .putString("imagedata", "")
                        .putString("avatar", "")
                        .apply()
                    debt = false
                    paired = false
                    userId = ""
                    bookRequested = false
                    bookedBikeId = ""
                    freeMinutes = 0
                    rideBikeId = ""
                    bikeId = ""
                    inBook = false
                    inRide = false
                    activeOrNo = false
                    profileOk = false
                    loggedOut = true
                    startActivity(Intent(this, JoinActivity::class.java))
                    dialog.dismiss()
                    finish()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun verifyButton() {
        verify_button.setOnClickListener {
            val smsNumbers = editText6.text.toString()
            Requests.validate(phoneNumber.text.toString(), smsNumbers)
        }
    }

    private fun checkNumberListener() {
        try {
            callback.checkNumberListener(CheckNumberListener {
                Utils.showDialog(this@MapActivity, getString(R.string.auth_error))
            })
        } catch (e: UninitializedPropertyAccessException) {
            internetLost = true
            e.printStackTrace()
        }
    }

    private fun getCountryCode() {
        phone_edit_text.setText(country_spiner.selectedCountryCodeWithPlus)
        phone_edit_text.setSelection(phone_edit_text.text.length)
        country_spiner.setOnCountryChangeListener {
            phone_edit_text.setText(country_spiner.selectedCountryCodeWithPlus)
            phone_edit_text.setSelection(phone_edit_text.text.length)
        }
    }

    @SuppressLint("SetTextI18n")
    fun sendAgainTimer() {
        object : CountDownTimer(59000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                if (millisUntilFinished / 1000 > 10) {
                    sendAgain.text =
                        getString(R.string.you_will_receive_the_sms_in_0_) + millisUntilFinished / 1000
                } else {
                    sendAgain.text =
                        getString(R.string.you_will_receive_the_sms_in_0_0) + millisUntilFinished / 1000
                }
            }

            override fun onFinish() {
                sendAgain.text = getString(R.string.resend_the_sms)
                sendAgain.setTextColor(
                    ContextCompat.getColor(
                        applicationContext,
                        R.color.colorPrimary
                    )
                )
                sendAgain.setOnClickListener {
                    Requests.enter(mLocationX, mLocationY, phone_edit_text.text.toString())
                    sendAgain.setOnClickListener { }
                    sendAgain.setTextColor(
                        ContextCompat.getColor(
                            applicationContext,
                            R.color.colorText
                        )
                    )
                }
            }
        }.start()
    }

    @SuppressLint("InflateParams")
    private fun addMarker(ltlng: LatLng, id: String) {
        try {
            val view1 = LayoutInflater.from(this).inflate(R.layout.marker_view, null, false)
            markerView = view1.findViewById(R.id.markerView)
            markerImage = view1.findViewById(R.id.imageViewMarker)
            val markerLayout = view1.findViewById<RelativeLayout>(R.id.markerLayout)
            val markerOptions = MarkerOptions().position(ltlng)
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                markerLayout.background.setColorFilter(
                    ContextCompat.getColor(
                        this,
                        R.color.colorPrimary
                    ), PorterDuff.Mode.SRC_ATOP
                )
            }
            val newMarkerFromView = Utils.createBitmapFromLayout(markerView)
            val bitmap = BitmapDescriptorFactory.fromBitmap(newMarkerFromView)
            if ((inBook && bookedMarker) || inRide) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    markerLayout.backgroundTintList =
                        ContextCompat.getColorStateList(this, android.R.color.white)
                } else {
                    markerLayout.background.setColorFilter(
                        ContextCompat.getColor(
                            this,
                            android.R.color.white
                        ), PorterDuff.Mode.SRC_ATOP
                    )
                }
                val newBitMap = Bitmap.createScaledBitmap(
                    Utils.createBitmapFromLayout(markerView),
                    (markerView.width * 1.3f).toInt(),
                    (markerView.height * 1.3f).toInt(),
                    true
                )
                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(newBitMap))
                inBookStatus = true
            } else {
                markerOptions.icon(bitmap)
            }
            val bike = Bike(markerOptions, id)
            bikes.add(bike)
            mClusterManager.addItem(bike)
            mClusterManager.cluster()
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    @SuppressLint("InflateParams")
    private fun addEmptyMarker() {
        lastClicked = mMap.addMarker(
            MarkerOptions().position(LatLng(mLocationX, mLocationY))
                .icon(
                    BitmapDescriptorFactory.fromBitmap(
                        Utils.createBitmapFromLayout(
                            LayoutInflater.from(this)
                                .inflate(
                                    R.layout.empty_marker_view,
                                    null,
                                    false
                                ).findViewById<RelativeLayout>(R.id.emptyMarkerView)
                        )
                    )
                )
        )
        lastClicked!!.remove()
    }

    private inner class InfoWindowLayoutListener : ViewTreeObserver.OnGlobalLayoutListener {
        override fun onGlobalLayout() {
            popupXOffset = infoWindowContainer.width / 2
            popupYOffset = infoWindowContainer.height
        }
    }

    private inner class PositionUpdaterRunnable : Runnable {
        private var lastXPosition = Integer.MIN_VALUE
        private var lastYPosition = Integer.MIN_VALUE
        override fun run() {
            handler!!.postDelayed(this, 1)
            if (infoWindowContainer.visibility == VISIBLE) {
                if (trackedPosition != null) {
                    val targetPosition = mMap.projection.toScreenLocation(trackedPosition)
                    if (lastXPosition != targetPosition.x || lastYPosition != targetPosition.y) {
                        overlayLayoutParams =
                            infoWindowContainer.layoutParams as AbsoluteLayout.LayoutParams
                        overlayLayoutParams.x = targetPosition.x - popupXOffset
                        overlayLayoutParams.y = targetPosition.y - popupYOffset - markerHeight
                        infoWindowContainer.layoutParams = overlayLayoutParams
                        lastXPosition = targetPosition.x
                        lastYPosition = targetPosition.y
                    }
                }
            }
        }
    }

    fun walletclicked(view: View) {
        cardFragment = CardFragment()
        cardFragment.show(supportFragmentManager, "card_fragment")
    }

    fun profileclicked(view: View) {
        profileFragment = ProfileFragment()
        profileFragment.show(supportFragmentManager, "fragment_profile")
        Utils.hideKeyboard(this@MapActivity)
    }

    fun pricesclicked(view: View) {
        pricesFragment = PricesFragment()
        pricesFragment.show(supportFragmentManager, "prices_fragment")
    }

    fun tripsclicked(view: View) {
        tripsFragment = TripsFragment()
        Requests.getTrips()
        tripsFragment.show(supportFragmentManager, "trips_fragment")
    }
}