package com.imed.mimo.bike.listeners;

public interface AmeraAttachedListener {
    void ameriaCard(boolean attached, String cardMask);
}
