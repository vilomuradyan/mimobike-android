package com.imed.mimo.bike.listeners;

public interface RideInfoError {
    void rideInfoError(String description);
}
