package com.imed.mimo.bike.fragments

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.BitmapFactory
import android.graphics.drawable.StateListDrawable
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.PopupMenu
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.Target
import com.imed.mimo.bike.R
import com.imed.mimo.bike.activities.MapActivity
import com.imed.mimo.bike.activities.SplashActivity
import com.imed.mimo.bike.connections.Requests
import com.imed.mimo.bike.helpers.Utils
import com.imed.mimo.bike.helpers.Utils2
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.custom_dialog_yes_no.view.*
import kotlinx.android.synthetic.main.fragment_profile.view.*
import java.util.*

class ProfileFragment : DialogFragment() {

    private var maleFemale: String = ""
    private var sex = ""
    private var name = ""
    private var surname = ""
    private var email = ""
    private var date = ""
    private var birthday = ""
    private lateinit var profile_avatar : CircleImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(
            STYLE_NORMAL,
            android.R.style.Theme_NoTitleBar_Fullscreen)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_profile, container, false)
        dialog?.window?.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialog?.window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            dialog?.window?.statusBarColor = ContextCompat.getColor((activity as MapActivity),R.color.colorPrimary)
            dialog?.window?.navigationBarColor =
                ContextCompat.getColor((activity as MapActivity), R.color.colorPrimary)
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            dialog?.window?.decorView?.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR or View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR
        }
        dialog?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
        view.button_back_profile.setOnClickListener {
            this.dismiss()
        }
        view.button_done.setOnClickListener {
            this.dismiss()
        }
        profile_avatar = view.profile_avatar
        val bitmap = (activity as MapActivity).getSharedPreferences("mimo_user_data", Context.MODE_PRIVATE)
            .getString("imagedata", "")
        if (bitmap != "") {
            val b = Base64.decode(bitmap, Base64.DEFAULT)
            val bip = BitmapFactory.decodeByteArray(b, 0, b.size)
            profile_avatar.setImageBitmap(bip)
        }
        segmented(view)
        addImageListener(view)
        updateData(view)
        datePickerListener(view.dateLAyout, view.select_button, view.date_number_event)
        if (SplashActivity.userId != "" && SplashActivity.paired) {
            logIn(view)
        }
        Utils.hideKeyboard(activity as MapActivity)
        return view
    }

    private fun datePickerListener(rel: RelativeLayout, button: Button, textView: TextView) {
        button.setOnClickListener {
            date = Utils2.showDialog(textView, birthday, activity as MapActivity, false)
        }
        rel.setOnClickListener {
            date = Utils2.showDialog(textView, birthday, activity as MapActivity, false)
        }
    }

    override fun dismiss() {
        super.dismiss()
        (activity as MapActivity).completeProfile()
    }

    fun updatePhoto(bitmap: ByteArray){
        Glide.with(activity as MapActivity)
            .load(bitmap)
            .override(Target.SIZE_ORIGINAL)
            .into(profile_avatar)
    }

    private fun segmented(view: View) {
        val edited = (activity as MapActivity).getSharedPreferences("mimo_user_data", Context.MODE_PRIVATE).edit()
        view.button_male.buttonDrawable = StateListDrawable()
        view.button_female.buttonDrawable = StateListDrawable()
        view.segmented2.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.button_male -> {
                    maleFemale = "Male"
                    if (sex != "M") {
                        edited.putString("sex", "M").apply()
                        Requests.userDataToSend("sex", "0")
                    }
                }
                R.id.button_female -> {
                    maleFemale = "Female"
                    if (sex != "F") {
                        edited.putString("sex", "F").apply()
                        Requests.userDataToSend("sex", "1")
                    }
                }
            }
        }
    }

    private fun addImageListener(view:View) {
        view.edit_button.setOnClickListener {
            val popup = PopupMenu(context, view.edit_button)
            popup.inflate(R.menu.add_image)
            val bitmap = (activity as MapActivity).getSharedPreferences("mimo_user_data", Context.MODE_PRIVATE).getString("imagedata", "")
            popup.menu.findItem(R.id.delete).isVisible = bitmap != ""
            popup.setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.capture_image -> {
                        Utils2.imageCapture(activity as MapActivity)
                        true
                    }
                    R.id.gallery -> {
                        Utils2.imagePickerGranted(activity as MapActivity)
                        true
                    }
                    R.id.delete -> {
                        try {
                            val adb = android.app.AlertDialog.Builder(activity as MapActivity, R.style.AlertDialogCustom)
                            val view = layoutInflater.inflate(R.layout.custom_dialog_yes_no, null) as RelativeLayout
                            adb.setView(view)
                            val dialog = adb.create()
                            dialog.show()
                            val tvCount = view.findViewById<TextView>(R.id.dialogText)
                            tvCount.text = getString(R.string.are_you_sure_delete_image)
                            view.button_no.setOnClickListener {
                                dialog.dismiss()
                            }
                            view.button_yes.setOnClickListener {
                                profile_avatar.setImageDrawable(ContextCompat.getDrawable(activity as MapActivity,R.drawable.bike_avatar))
                                val edited = (activity as MapActivity).getSharedPreferences("mimo_user_data", Context.MODE_PRIVATE).edit()
                                Requests.deleteAvatar((activity as MapActivity).getSharedPreferences("mimo_user_data", Context.MODE_PRIVATE).getString("avatar","")!!)
                                edited.putString("avatar", "").apply()
                                edited.putString("imagedata", "").apply()
                                dialog.dismiss()
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                        true
                    }
                    else -> false
                }
            }
            popup.show()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun updateData(view: View) {
        view.editTextname.addTextChangedListener(object : TextWatcher {
            private var timer = Timer()
            override fun afterTextChanged(p0: Editable?) {
                timer.cancel()
                timer = Timer()
                timer.schedule(
                    object : TimerTask() {
                        override fun run() {
                            if (view.editTextname.text.toString() != name) {
                                name = view.editTextname.text.toString()
                                Requests.userDataToSend("name", name)
                            }
                        }
                    },
                    1500
                )
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })
        view.editText_surname.addTextChangedListener(object : TextWatcher {
            private var timer = Timer()
            override fun afterTextChanged(p0: Editable?) {
                timer.cancel()
                timer = Timer()
                timer.schedule(
                    object : TimerTask() {
                        override fun run() {
                            if (view.editText_surname.text.toString() != surname) {
                                surname = view.editText_surname.text.toString()
                                Requests.userDataToSend("surname", surname)
                            }
                        }
                    },
                    1500
                )
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })
        view.editText_email.addTextChangedListener(object : TextWatcher {
            private var timer = Timer()
            override fun afterTextChanged(p0: Editable?) {
                timer.cancel()
                timer = Timer()
                timer.schedule(
                    object : TimerTask() {
                        override fun run() {
                            try {
                                if (Utils2.validateEmail(
                                        view.editText_email,
                                        activity as MapActivity
                                    )
                                ) {
                                    if (view.editText_email.text.toString() != email) {
                                        email = view.editText_email.text.toString()
                                        Requests.userDataToSend("email", email)
                                    }
                                }
                            } catch (e: java.lang.Exception){
                                e.printStackTrace()
                            }
                        }
                    },
                    1500
                )
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun logIn(view: View) {
        val sp = (activity as MapActivity).getSharedPreferences("mimo_user_data", Context.MODE_PRIVATE)
        name = sp.getString("name", "")!!
        surname = sp.getString("surname", "")!!
        birthday = sp.getString("birthday", "")!!
        sex = sp.getString("sex", "")!!
        email = sp.getString("email", "")!!
        try {
            view.phone_text.text = SplashActivity.userId
            view.editTextname.setText(name)
            view.editText_surname.setText(surname)
            view.date_number_event.text = birthday
            if (sex == "M") {
                view.segmented2.check(R.id.button_male)
            } else if (sex == "F") {
                view.segmented2.check(R.id.button_female)
            }
            view.editText_email.setText(email)
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }
}
