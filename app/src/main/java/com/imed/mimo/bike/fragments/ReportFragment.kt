package com.imed.mimo.bike.fragments

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.imed.mimo.bike.R
import com.imed.mimo.bike.activities.MapActivity
import com.imed.mimo.bike.helpers.Constants
import com.imed.mimo.bike.helpers.Constants.Companion.reportFragmentCreated
import com.imed.mimo.bike.helpers.Utils
import com.imed.mimo.bike.helpers.Utils2
import kotlinx.android.synthetic.main.fragment_report.view.*

class ReportFragment : BottomSheetDialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        reportFragmentCreated = true
        val view = inflater.inflate(R.layout.fragment_report, container, false)
        view.deffect_layout.setOnClickListener {
            (activity as MapActivity).openReportActivity("bike_deffect")
        }
        view.deffect_button.setOnClickListener {
            (activity as MapActivity).openReportActivity("bike_deffect")
        }
        view.lock_problem_layout.setOnClickListener {
            (activity as MapActivity).openReportActivity("lock_problem")
        }
        view.lock_problem_button.setOnClickListener {
            (activity as MapActivity).openReportActivity("lock_problem")
        }
        view.wrong_parking_layout.setOnClickListener {
            (activity as MapActivity).openReportActivity("wrong_parking")
        }
        view.wrong_parking_button.setOnClickListener {
            (activity as MapActivity).openReportActivity("wrong_parking")
        }
        view.call_layout.setOnClickListener {
            if(Utils.isPermissionGranted(Manifest.permission.CALL_PHONE, Constants.call_permission, activity as MapActivity)) {
                Utils2.call((activity as MapActivity))
            }
        }
        view.call_button.setOnClickListener {
            if(Utils.isPermissionGranted(Manifest.permission.CALL_PHONE, Constants.call_permission, activity as MapActivity)) {
                Utils2.call((activity as MapActivity))
            }
        }
        return view
    }

    override fun onCancel(dialog: DialogInterface) {
        super.onCancel(dialog)
        reportFragmentCreated = false
    }
}
