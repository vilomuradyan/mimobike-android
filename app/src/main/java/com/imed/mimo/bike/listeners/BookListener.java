package com.imed.mimo.bike.listeners;

import com.google.android.gms.maps.model.LatLng;

public interface BookListener {
    void bookListener(String description, String bikeId, LatLng latLng, boolean booked);
}
