package com.imed.mimo.bike.listeners;

public interface TripRouteListener {
    void getTrips(String trips);
}
