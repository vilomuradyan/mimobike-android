package com.imed.mimo.bike.network

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface OrderService {

    @GET("binding/accept")
    fun getStatus(@Header("Authorization") authHeader : String,
                  @Query("orderId") orderId : String) : Observable<Map<String,String>>

    @GET("payment/accept")
    fun getPaymentStatus(@Header("Authorization") authHeader : String,
                  @Query("orderId") orderId : String) : Observable<Map<String, String>>

}