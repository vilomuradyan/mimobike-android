package com.imed.mimo.bike.activities

import android.Manifest
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.app.Dialog
import android.bluetooth.*
import android.bluetooth.le.*
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.PointF
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.view.WindowManager
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.dlazaro66.qrcodereaderview.QRCodeReaderView
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.imed.mimo.bike.R
import com.imed.mimo.bike.activities.SplashActivity.Companion.callback
import com.imed.mimo.bike.activities.SplashActivity.Companion.inBluetoothActivity
import com.imed.mimo.bike.activities.SplashActivity.Companion.inBook
import com.imed.mimo.bike.activities.SplashActivity.Companion.inRide
import com.imed.mimo.bike.activities.SplashActivity.Companion.isOpenLock
import com.imed.mimo.bike.activities.SplashActivity.Companion.lockDevice
import com.imed.mimo.bike.activities.SplashActivity.Companion.macAddress
import com.imed.mimo.bike.activities.SplashActivity.Companion.minimal
import com.imed.mimo.bike.activities.SplashActivity.Companion.mode
import com.imed.mimo.bike.activities.SplashActivity.Companion.ridePrice
import com.imed.mimo.bike.activities.SplashActivity.Companion.studentPrice
import com.imed.mimo.bike.activities.SplashActivity.Companion.weekendPrice
import com.imed.mimo.bike.connections.Requests
import com.imed.mimo.bike.fragments.EnterManuallyFragment
import com.imed.mimo.bike.helpers.BlUtils
import com.imed.mimo.bike.helpers.Constants
import com.imed.mimo.bike.helpers.Constants.Companion.BLE_PERMISSION
import com.imed.mimo.bike.helpers.Constants.Companion.DESCRIPTOR_UUID
import com.imed.mimo.bike.helpers.Constants.Companion.NOTIFY_UUID
import com.imed.mimo.bike.helpers.Constants.Companion.READ_UUID
import com.imed.mimo.bike.helpers.Constants.Companion.SERVICE_UUID
import com.imed.mimo.bike.helpers.Constants.Companion.WRITE_UUID
import com.imed.mimo.bike.helpers.Constants.Companion.camera_permission
import com.imed.mimo.bike.helpers.Utils
import com.imed.mimo.bike.listeners.QrPermittedListener
import com.imed.mimo.bike.listeners.ScanButtonCallback
import kotlinx.android.synthetic.main.activity_bluetooth.*
import java.util.*
import kotlin.NullPointerException
import kotlin.collections.ArrayList

class BluetoothActivity : AppCompatActivity(),
    QRCodeReaderView.OnQRCodeReadListener {

    private var qrScanned: Boolean = false
    private var bleFound: Boolean = false
    private var isScanning: Boolean = false
    private var bleManager: BluetoothManager? = null
    private var bleAdapter: BluetoothAdapter? = null
    private var bleScanner: BluetoothLeScanner? = null
    private var bleGATT: BluetoothGatt? = null
    private lateinit var bleWrite: BluetoothGattCharacteristic
    private lateinit var bleRead: BluetoothGattCharacteristic
    private lateinit var bleNotify: BluetoothGattCharacteristic
    private lateinit var dialog2: Dialog
    lateinit var animator: ObjectAnimator
    private var torchEnabled = false
    private var oneTime = true
    val keyBytes = ArrayList<Byte>()
    private var languageLocale = ""
    var mLocationX = 0.0
    var mLocationY = 0.0

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            languageLocale =
                getSharedPreferences("mimo_user_data", Context.MODE_PRIVATE).getString("lang", "")!!
            if (languageLocale != "") {
                changeLanguage(languageLocale)
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        setContentView(R.layout.activity_bluetooth)
        getLocation()
        statusBarThings()
        var price = when (mode) {
            "student" -> studentPrice.toString()
            else -> ridePrice.toString()
        }
        if(Calendar.getInstance().get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY
            || Calendar.getInstance().get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
            price = weekendPrice.toString()
        }
        unlock_fee_is.text =
            getString(R.string.a_minute_for_riding) + " " + price + " " + getString(R.string.amd) + ". " +
                    getString(R.string.unlock_fee_is) + " " + minimal.toString() + " " + getString(R.string.amd)
        textManually()
        qrPermittedListener()
        didntOpenListener()
        initBLE()
        if (Utils.isPermissionGranted(Manifest.permission.CAMERA, camera_permission, this)) {
            qrCode()
            animateView()
            torchListener()
            inBluetoothActivity = true
            isOpenLock = false
        }
    }

    private fun changeLanguage(string: String) {
        val locale = Locale(string)
        val config = Configuration()
        config.locale = locale
        baseContext.resources.updateConfiguration(config, baseContext.resources.displayMetrics)
    }

    private fun statusBarThings() {
        bl_toolbar.bringToFront()
        buttons_pay_go_layout.bringToFront()
        button_close_qr.setOnClickListener {
            finish()
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimary)
            window.navigationBarColor = ContextCompat.getColor(this, R.color.colorPrimary)
        } else {
            window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            val statusBarHeight = Utils.getStatusBarHeight(this)
            val view = View(this)
            view.layoutParams = FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            view.layoutParams.height = statusBarHeight
            (window.decorView as ViewGroup).addView(view)
            view.background = resources.getDrawable(R.drawable.navibg)
            val layoutParams = FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            layoutParams.setMargins(0, Utils.getStatusBarHeight(this), 0, 0)
            relatt.layoutParams = layoutParams
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR or View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR
        }
    }

    private fun torchListener() {
        buttonFlash.setOnClickListener {
            if (!torchEnabled) {
                torchEnabled = true
                qr_decoder_view.setTorchEnabled(true)
            } else {
                torchEnabled = false
                qr_decoder_view.setTorchEnabled(false)
            }
        }
    }

    private fun getLocation(){
        val fusedLocationProviderClient:
                FusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        fusedLocationProviderClient.lastLocation
            .addOnSuccessListener(this) { location ->
                if (location != null) {
                    mLocationX = location.latitude
                    mLocationY = location.longitude
                }
            }
    }

    private fun initBLE() {
        val bluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        bleAdapter = bluetoothManager.adapter
        if (bleAdapter == null || !bleAdapter!!.isEnabled) {
            val enableBTIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enableBTIntent, 1)
        }
    }

    private fun qrCode() {
        qr_decoder_view.setOnQRCodeReadListener(this)
        qr_decoder_view.setQRDecodingEnabled(true)
        qr_decoder_view.forceAutoFocus()
        qr_decoder_view.setAutofocusInterval(2000)
    }

    override fun onQRCodeRead(text: String?, points: Array<out PointF>?) {
        if (oneTime) {
            try {
                Requests.validate(text!!.substring(text.length - 10), mLocationX, mLocationY)
                oneTime = false
            } catch (e: UninitializedPropertyAccessException) {
                e.printStackTrace()
            }
        }
    }

    private fun qrPermittedListener() {
        try {
            callback.qrPermittedListener(QrPermittedListener { permitted, mac, description, bleKey ->
                runOnUiThread {
                    if (permitted) {
                        qrScanned = true
                        val splited = bleKey.split(".")
                        for (i in splited) {
                            keyBytes.add(i.toInt().toByte())
                        }
                        macAddress = mac
                        bleScan()
                        qr_decoder_view.setOnQRCodeReadListener(null)
                        qr_decoder_view.setQRDecodingEnabled(false)
                        try {
                            animator.cancel()
                        } catch (e: java.lang.Exception) {
                            e.printStackTrace()
                        }
                        inRide = true
                        inBook = false
                        try {
                            dialog2 = Utils.showConnectionDialog(
                                this@BluetoothActivity,
                                getString(R.string.please_Wait)
                            )
                        } catch (e: Exception) {
                            when(e){
                                is WindowManager.BadTokenException,
                                    is NullPointerException, is UninitializedPropertyAccessException -> e.printStackTrace()
                            }
                        }
                        if (this::dialog2.isInitialized){
                            Handler().postDelayed({
                                dialog2.cancel()
//                            if(!startedRide) {
//                                startedRide = true
//                                startActivity(Intent(this, RideActivity::class.java))
//                            }
                                finish()
                            }, 2000)
                        }else {
                            Constants.isRating = true
                        }
                    } else {
                        when (description) {
                            "266" -> Utils.showDialog(
                                this@BluetoothActivity,
                                getString(R.string.ride_busy_error)
                            )
                            "260" -> Utils.showDialog(
                                this@BluetoothActivity,
                                getString(R.string.request_error)
                            )
                            "261" -> Utils.showDialog(
                                this@BluetoothActivity,
                                getString(R.string.request_error)
                            )
                            "267" -> Utils.showDialog(
                                this@BluetoothActivity,
                                getString(R.string.qr_close_error)
                            )
                            "263" -> Utils.showDialog(
                                this@BluetoothActivity,
                                getString(R.string.already_have_error)
                            )
                            "262" -> Utils.showDialog(
                                this@BluetoothActivity,
                                getString(R.string.ride_service_error)
                            )
                            "265" -> Utils.showDialog(
                                this@BluetoothActivity,
                                getString(R.string.unpaid_error)
                            )
                            "264" -> {
                                Utils.showDialog(
                                    this@BluetoothActivity,
                                    getString(R.string.book_far_error)
                                )
                            }
                            "500" -> {
                                Utils.showDialog(
                                    this@BluetoothActivity,
                                    getString(R.string.unpaid_error500)
                                )
                            }
                            "501" -> {
                                Utils.showDialog(
                                    this@BluetoothActivity,
                                   getString(R.string.unpaid_error501)

                                )
                            }
                            "502" -> {
                                Utils.showDialog(
                                    this@BluetoothActivity,
                                    getString(R.string.unpaid_error502)
                                )
                            }
                            else -> {
                                Utils.showDialog(this@BluetoothActivity, getString(R.string.sorry_bike_service))
                            }
                        }
                        qrScanned = false
                        Handler().postDelayed({
                            oneTime = true
                        }, 3000)
                        textView_manually.visibility = VISIBLE
                        qrCode()
                    }
                }
            })
        } catch (e: UninitializedPropertyAccessException) {
            e.printStackTrace()
        }
    }

    private val scanCallback =
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            object : ScanCallback() {
                override fun onScanResult(callbackType: Int, result: ScanResult?) {
                    lockDevice = result?.device as BluetoothDevice
                    val mac = lockDevice?.address
                    if (mac == macAddress) {
                        bleFound = true
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            bleScanner?.stopScan(this)
                        }
                        bleGATT = lockDevice!!.connectGatt(this@BluetoothActivity, false, gattCallback)
                    }
                }
                override fun onScanFailed(errorCode: Int) {
                    super.onScanFailed(errorCode)
                    isScanning = false
                }
            }
        } else {
            null
        }

    private val gattCallback = object : BluetoothGattCallback() {
        override fun onConnectionStateChange(gatt: BluetoothGatt?, status: Int, newState: Int) {
            super.onConnectionStateChange(gatt, status, newState)
            when (newState) {
                BluetoothProfile.STATE_CONNECTED -> {
                    gatt?.discoverServices()
                }
            }
        }

        override fun onServicesDiscovered(gatt: BluetoothGatt?, status: Int) {
            super.onServicesDiscovered(gatt, status)
            val gat = gatt ?: return
            val bleS = gat.getService(SERVICE_UUID)
            if (bleS != null) {
                bleWrite = bleS.getCharacteristic(WRITE_UUID)
                bleRead = bleS.getCharacteristic(READ_UUID)
                bleNotify = bleS.getCharacteristic(NOTIFY_UUID)
                try {
                    gatt.setCharacteristicNotification(bleRead, true)
                    val descriptor = bleNotify.getDescriptor(DESCRIPTOR_UUID)
                    descriptor.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
                    gatt.writeDescriptor(descriptor)
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
        }

        override fun onDescriptorWrite(
            gatt: BluetoothGatt,
            descriptor: BluetoothGattDescriptor,
            status: Int
        ) {
            super.onDescriptorWrite(gatt, descriptor, status)
            val data = BlUtils.encrypt(BlUtils.get(), keyBytes.toByteArray())
            val data1 = BlUtils.encrypt(data, keyBytes.toByteArray())
            bleWrite.value = data1
            gatt.writeCharacteristic(bleWrite)
        }

        override fun onCharacteristicChanged(
            gatt: BluetoothGatt,
            characteristic: BluetoothGattCharacteristic
        ) {
            super.onCharacteristicChanged(gatt, characteristic)
            val realBytes = BlUtils.decrypt(characteristic.value, keyBytes.toByteArray())
            val realBytesArrayList = ArrayList<Byte>()
            bleGATT = gatt
            realBytes.forEach {
                realBytesArrayList.add(it)
            }
            if (realBytesArrayList[0] == 0x05.toByte() && realBytesArrayList[1] == 0x0F.toByte() && realBytesArrayList[2] == 0x01.toByte()
                && realBytesArrayList[3] == 0x00.toByte()
            ) {
                callback.bleStatusOpened.opened(true)
            }
            if (realBytesArrayList[0] == 0x05.toByte() && realBytesArrayList[1] == 0x0F.toByte() && realBytesArrayList[2] == 0x01.toByte()
                && realBytesArrayList[3] == 0x01.toByte()
            ) {
                gatt.disconnect()
                callback.bleStatusOpened.opened(false)
            }
        }
    }

    fun textManually() {
        textManually.setOnClickListener {
            val manuallyFragment = EnterManuallyFragment.newInstance(mLocationX, mLocationY)
            manuallyFragment.show(supportFragmentManager, "Manually Bottom Sheet")
            supportFragmentManager.executePendingTransactions()
        }
    }

    override fun onStart() {
        super.onStart()
        bleManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        bleAdapter = bleManager?.adapter
    }

    override fun onResume() {
        super.onResume()
        qr_decoder_view.startCamera()
        if (qrScanned && !bleFound) {
            bleScan()
        }
    }

    override fun onPause() {
        super.onPause()
        qr_decoder_view.stopCamera()
        if (bleScanner != null && isScanning) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                bleScanner?.stopScan(scanCallback)
            }
            isScanning = false
        }
    }

    override fun onStop() {
        super.onStop()
        if (bleScanner != null && isScanning) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                bleScanner?.stopScan(scanCallback)
            }
            isScanning = false
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (bleScanner != null && isScanning) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                bleScanner?.stopScan(scanCallback)
            }
            isScanning = false
        }
    }

    private fun animateView() {
        val vto = scannerLayout.viewTreeObserver
        vto.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                scannerLayout.viewTreeObserver.removeOnGlobalLayoutListener(this)
                animator = ObjectAnimator.ofFloat(
                    scannerBar,
                    "translationY",
                    0f,
                    scannerLayout.height.toFloat()
                )
                animator.repeatMode = ValueAnimator.REVERSE
                animator.repeatCount = ValueAnimator.INFINITE
                animator.interpolator = AccelerateDecelerateInterpolator()
                animator.duration = 3000
                animator.start()
            }
        })
    }

    private fun bleScan() {
        if (bleAdapter == null || !bleAdapter!!.isEnabled) {
            val intent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(intent, BLE_PERMISSION)
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                bleScanner = bleAdapter?.bluetoothLeScanner
                if (bleScanner != null && qrScanned && !isScanning) {
                    val scanFilter = ScanFilter.Builder().build()
                    val settings =
                        ScanSettings.Builder().setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                            .build()
                    isScanning = true
                    bleScanner?.startScan(listOf(scanFilter), settings, scanCallback)
                }
            }
        }
    }

    private fun didntOpenListener() {
        try {
            callback.scanButtonCallback(ScanButtonCallback {
                try {
                    bleGATT!!.disconnect()
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            })
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        try {
            if (requestCode == camera_permission) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    finish()
                    overridePendingTransition(0, 0)
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                    textManually()
                    qrPermittedListener()
                    didntOpenListener()
                    initBLE()
                    animateView()
                    torchListener()
                    qrCode()
                    inBluetoothActivity = true
                    isOpenLock = false
                }
//                else {
//                    Utils.showDialog(this@BluetoothActivity, getString(R.string.grant_camera))
//                }
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }
}
