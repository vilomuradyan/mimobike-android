package com.imed.mimo.bike.fragments

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import com.imed.mimo.bike.R
import com.imed.mimo.bike.activities.MapActivity
import com.imed.mimo.bike.activities.SplashActivity
import com.imed.mimo.bike.network.OrderService
import com.imed.mimo.bike.network.createService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class EvocaFragment : DialogFragment() {

    private var link = ""
    private var from_where = ""
    private val orderService: OrderService = createService()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(
            STYLE_NORMAL,
            android.R.style.Theme_NoTitleBar_Fullscreen)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_evoca, container, false)
        dialog?.window?.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialog?.window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            dialog?.window?.statusBarColor = ContextCompat.getColor((activity as MapActivity), R.color.ameria_color)
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            dialog?.window?.decorView?.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }
        arguments?.let {
            link = it.getString("ameria_link")!!
            from_where = it.getString("from_where")!!
//            tripId = it.getString("tripId")!!
        }
        dialog?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)

       var webView = view.findViewById<WebView>(R.id.web_view)

        webView.setLayerType(View.LAYER_TYPE_HARDWARE, null)
        webView.settings.domStorageEnabled = true
        webView.settings.javaScriptEnabled = true
        webView.settings.loadWithOverviewMode = true
        webView.webChromeClient = WebChromeClient()
        webView.loadUrl(link)
        webView.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {
                Log.d("TESTING", "Title = " + view.title)
//
//                val title = view.title
//                if(title == "SUCCESS") {
//                    Handler().postDelayed({
//                        try {
//                            try {
//                                fragmentManager!!.beginTransaction().remove(this@EvocaFragment).commit()
//                            } catch (e: Exception){
//                                e.printStackTrace()
//                            }
//                            if(from_where == "attach") {
//                                SplashActivity.callback.ameriaAttachedListener.ameriaCard(
//                                    true,
//                                    "")
//                            } else {
//                                SplashActivity.callback.payDebtStatusListener.payDebtStatusListener("OK")
//                            }
//                        } catch (e: Exception){
//                            e.printStackTrace()
//                        }
//
//                    }, 0)
//                } else if(title == "FAILED") {
//                    Handler().postDelayed({
//                        try {
//                            fragmentManager!!.beginTransaction().remove(this@EvocaFragment)
//                                .commit()
//                        } catch (e: Exception) {
//                            e.printStackTrace()
//                        }
//                        if(from_where == "attach") {
//                            SplashActivity.callback.ameriaAttachedListener.ameriaCard(false, "")
//                        } else {
//                            SplashActivity.callback.payDebtStatusListener.payDebtStatusListener("ERROR")
//                        }
//                    }, 0)
//                }
            }

            @SuppressLint("CheckResult")
            private fun statusPaymentRequest(orderId : String){
                val username = "user"
                val password = "user123"
                val base = "$username:$password"
                val authHeader = "Basic " + Base64.encodeToString(base.toByteArray(), Base64.NO_WRAP)
                orderService.getPaymentStatus(authHeader, orderId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::handleBindingResponse, this::handleBindingError)
            }

            @SuppressLint("CheckResult")
            private fun statusRequest(orderId : String){
                val username = "user"
                val password = "user123"
                val base = "$username:$password"
                val authHeader = "Basic " + Base64.encodeToString(base.toByteArray(), Base64.NO_WRAP)
                orderService.getStatus(authHeader, orderId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::handleBindingResponse, this::handleBindingError)
            }

            private fun handleBindingResponse(status : Map<String,String>){
                    Log.d("STATUS", "STATUS = $status")
                        if(status.getValue("status") == "SUCCESS") {
                            Handler().postDelayed({
                                try {
                                    try {
                                        fragmentManager!!.beginTransaction().remove(this@EvocaFragment).commit()
                                    } catch (e: Exception){
                                        e.printStackTrace()
                                    }
                                    if(from_where == "attach") {
                                        SplashActivity.callback.ameriaAttachedListener.ameriaCard(
                                            true,
                                            "")
                                    } else {
                                        SplashActivity.callback.payDebtStatusListener.payDebtStatusListener("OK")
                                    }
                                } catch (e: Exception){
                                    e.printStackTrace()
                                }

                            }, 0)
                        } else {
                            Handler().postDelayed({
                                try {
                                    fragmentManager!!.beginTransaction().remove(this@EvocaFragment)
                                        .commit()
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }
                                if(from_where == "attach") {
                                    SplashActivity.callback.ameriaAttachedListener.ameriaCard(false, "")
                                } else {
                                    SplashActivity.callback.payDebtStatusListener.payDebtStatusListener("ERROR")
                                }
                            }, 0)
                        }
            }

            private fun handleBindingError(error: Throwable) {
                Log.d("BINDING", "ERROR = $error")
                Handler().postDelayed({
                    try {
                        fragmentManager!!.beginTransaction().remove(this@EvocaFragment)
                            .commit()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    if(from_where == "attach") {
                        SplashActivity.callback.ameriaAttachedListener.ameriaCard(false, "")
                    } else {
                        SplashActivity.callback.payDebtStatusListener.payDebtStatusListener("ERROR")
                    }
                }, 0)
            }

            override fun shouldOverrideUrlLoading(
                view: WebView?,
                url: String?
            ): Boolean {

                webView.loadUrl(url)
                val orderId : String
                if (url!!.contains("orderId", true)){
                    orderId = url.substring(url.indexOf("orderId", 0, true) + 8)
                    if (from_where == "attach"){
                        webView.stopLoading()
                        statusRequest(orderId)
                    }else {
                        webView.stopLoading()
                        statusPaymentRequest(orderId)
                    }
                    Log.d("ORDERID", "ORDER =$orderId")
                }

                return true
            }
        }
        return view
    }

    override fun onCancel(dialog: DialogInterface) {
        super.onCancel(dialog)
        (activity as MapActivity).cancelAmeriaDialog()
    }

    companion object {
        fun newInstance(link: String, from: String, tripId: String): EvocaFragment {
            val args = Bundle()
            args.putString("ameria_link", link)
            args.putString("from_where", from)
//            args.putString("tripId", tripId)
            val fragment = EvocaFragment()
            fragment.arguments = args
            return fragment
        }
    }
}