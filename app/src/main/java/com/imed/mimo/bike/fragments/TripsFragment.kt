package com.imed.mimo.bike.fragments


import android.app.Dialog
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.animation.AnimationUtils
import android.widget.RelativeLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.imed.mimo.bike.R
import com.imed.mimo.bike.activities.MapActivity
import com.imed.mimo.bike.activities.SplashActivity.Companion.calRate
import com.imed.mimo.bike.activities.SplashActivity.Companion.callback
import com.imed.mimo.bike.adapters.TripsAdapter
import com.imed.mimo.bike.listeners.TripRouteListener
import com.imed.mimo.bike.listeners.TripsListener
import kotlinx.android.synthetic.main.fragment_trips.view.*
import org.json.JSONArray
import java.util.*

class TripsFragment : DialogFragment() {

    private val dist = ArrayList<Int>()
    private val start = ArrayList<String>()
    private val end = ArrayList<String>()
    private val sum = ArrayList<Double>()
    private lateinit var included: RelativeLayout
    private val paid = ArrayList<String>()
    private val bonused = ArrayList<String>()
    private val id = ArrayList<String>()
    private val mins = ArrayList<Int>()
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var recycler_view_trips: RecyclerView
    private lateinit var dialog1: Dialog
    private var pos = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(
            STYLE_NORMAL,
            android.R.style.Theme_NoTitleBar_Fullscreen
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_trips, container, false)
        dialog?.window?.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialog?.window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            dialog?.window?.statusBarColor = ContextCompat.getColor((activity as MapActivity), R.color.colorPrimary)
            dialog?.window?.navigationBarColor =
                ContextCompat.getColor((activity as MapActivity), R.color.colorPrimary)
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            dialog?.window?.decorView?.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR or View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR
        }
        view.button_close_trips.setOnClickListener {
            this.dismiss()
        }
        included = view.findViewById(R.id.progress_bar_included)
        recycler_view_trips = view.recycler_view_trips
        tripsListener()
        tripRouteListener()
        return view
    }

    private fun tripsListener() {
        try {
            callback.tripsListener(TripsListener {
                try {
                    trips(it)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            })
        } catch (e: UninitializedPropertyAccessException) {
            e.printStackTrace()
        }
    }

    fun trips(it: JSONArray) {
        start.clear()
        end.clear()
        sum.clear()
        paid.clear()
        mins.clear()
        id.clear()
        bonused.clear()
        dist.clear()
        (activity as MapActivity).runOnUiThread {
            try {
                for (i in 0 until it.length()) {
                    val jsonObject = it.getJSONObject(i)
                    if (jsonObject.has("distance")) {
                        dist.add(jsonObject.getInt("distance"))
                    } else {
                        dist.add(0)
                    }
                    start.add(jsonObject.getString("started"))
                    end.add(jsonObject.getString("finished"))
                    sum.add(jsonObject.getDouble("amount"))
                    paid.add(jsonObject.getInt("status").toString())
                    if(jsonObject.has("bonused")) {
                        bonused.add(jsonObject.getInt("bonused").toString())
                    } else {
                        bonused.add("")
                    }
                    if (jsonObject.has("_id")) {
                        id.add(jsonObject.getString("_id"))
                    } else {
                        id.add("")
                    }
                    if (jsonObject.has("duration")) {
                        mins.add(jsonObject.getInt("duration"))
                    } else {
                        mins.add(0)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            try {
                viewManager = LinearLayoutManager(context)
                viewAdapter = TripsAdapter(
                    activity as MapActivity, dist, start, end, sum, paid, id, mins, bonused
                )

                viewAdapter.notifyDataSetChanged()
                recycler_view_trips.adapter = viewAdapter
                recycler_view_trips.layoutManager = viewManager
                included.visibility = View.GONE
                tripListener()
                val controller =
                    AnimationUtils.loadLayoutAnimation(
                        context,
                        R.anim.layout_animation_fall_dawn
                    )
                recycler_view_trips.layoutAnimation = controller
                recycler_view_trips.scheduleLayoutAnimation()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun close() {
        fragmentManager?.beginTransaction()?.remove(this)?.commit()
    }

    fun tripListener() {
        (viewAdapter as TripsAdapter).setOnItemClickListener { position: Int, v: View? ->
//            if (mode[position] != "B") {
                pos = position
//                Requests.getTripRouteVolley(activity as MapActivity, id.get(position))
//                dialog1 = Utils.showConnectionDialog(activity as MapActivity, getString(R.string.please_Wait))
//            }
        }
    }

    private fun tripRouteListener() {
        try {
            callback.tripRoutsListener(TripRouteListener {
                dialog1.cancel()
                val tripRouteFragment = TripRouteFragment.newInstance(
                    mins.get(pos).toString(),
                    String.format("%.1f", java.lang.Double.valueOf(dist[pos].toDouble()) / 1000),
                    (dist[pos] * calRate / 1000).toString(), it
                )
                tripRouteFragment.show(fragmentManager!!, "trip_route_fragment")
                fragmentManager?.executePendingTransactions()
            })
        } catch (e: UninitializedPropertyAccessException) {
            e.printStackTrace()
        }
    }
}
