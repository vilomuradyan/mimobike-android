package com.imed.mimo.bike.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.imed.mimo.bike.R;
import com.imed.mimo.bike.connections.Requests;
import com.imed.mimo.bike.helpers.Utils;

import java.util.ArrayList;

import static com.imed.mimo.bike.activities.SplashActivity.calRate;

public class TripsAdapter extends RecyclerView.Adapter {

    private static TripsAdapter.ClickListener clickListener;
    private Activity mContext;
    private ArrayList<Integer> dist;
    private ArrayList<String> start;
    private ArrayList<String> end;
    private ArrayList<Double> sum;
    private ArrayList<String> paid;
    private ArrayList<String> id;
    private ArrayList<String> bonus;
    private ArrayList<Integer> mins;

    public TripsAdapter(Activity context, ArrayList<Integer> dist, ArrayList<String> start, ArrayList<String> end,
                        ArrayList<Double> sum, ArrayList<String> paid, ArrayList<String> id, ArrayList<Integer> mins,ArrayList<String> bonus) {
        this.dist = dist;
        this.start = start;
        this.end = end;
        this.mins = mins;
        this.sum = sum;
        this.paid = paid;
        this.id = id;
        this.bonus = bonus;
        mContext = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View feedView = LayoutInflater.from(parent.getContext()).inflate(R.layout.trips_recycler_item, parent, false);
        return new FeedViewHolder(feedView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final FeedViewHolder feedHolder = (FeedViewHolder) holder;
        feedHolder.minutes_number_text.setText(mins.get(position).toString());
        feedHolder.distance_text_trips.setVisibility(View.VISIBLE);
        feedHolder.kcal_text_trips.setVisibility(View.VISIBLE);
        feedHolder.distance_text_trips.setText(String.format("%.1f", Double.valueOf(dist.get(position)) / 1000) + " " + mContext.getString(R.string.km));
        feedHolder.kcal_text_trips.setText(dist.get(position) * calRate / 1000 + " " + mContext.getString(R.string.kcal));
        feedHolder.trips_minutes_layout.setBackground(ContextCompat.getDrawable(mContext,R.drawable.circle_shape_trips));
        feedHolder.general_trips_item.setBackground(ContextCompat.getDrawable(mContext,R.drawable.circle_shape_trip1));
        feedHolder.start_text_view.setText(start.get(position));
        feedHolder.price_number_trips.setText(sum.get(position).toString());
        switch (paid.get(position)) {
            case ("2"):
//                if (end.get(position).equals("")) {
//                    feedHolder.paid_text_trips.setText(mContext.getString(R.string.not_stopped));
//                } else {
                feedHolder.paid_text_trips.setText(mContext.getString(R.string.unpaid));
//                }
                feedHolder.unpaid_text_trips.setVisibility(View.VISIBLE);
                feedHolder.paid_text_trips.setTextColor(ContextCompat.getColor(mContext, R.color.red));
                feedHolder.general_trips_item.setBackground(ContextCompat.getDrawable(mContext,R.drawable.circle_shape_trips_unpaid));
            case ("3"):
//                if (end.get(position).equals("")) {
//                    feedHolder.paid_text_trips.setText(mContext.getString(R.string.not_stopped));
//                } else {
                feedHolder.paid_text_trips.setText(mContext.getString(R.string.unpaid));
//                }
                feedHolder.unpaid_text_trips.setVisibility(View.VISIBLE);
                feedHolder.paid_text_trips.setTextColor(ContextCompat.getColor(mContext, R.color.red));
                feedHolder.general_trips_item.setBackground(ContextCompat.getDrawable(mContext,R.drawable.circle_shape_trips_unpaid));
                break;
            case "1":
                feedHolder.unpaid_text_trips.setVisibility(View.GONE);
                feedHolder.paid_text_trips.setTextColor(ContextCompat.getColor(mContext, R.color.paid_green));
                feedHolder.paid_text_trips.setText(mContext.getString(R.string.paid));
                if (!bonus.get(position).equals("")) {
                    feedHolder.min_for_free.setText("(" + bonus.get(position) + " " + mContext.getString(R.string.min_free));
                }
                break;
            case "4":
                feedHolder.paid_text_trips.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
                feedHolder.paid_text_trips.setText(mContext.getString(R.string.refund));
                break;
        }
        feedHolder.unpaid_text_trips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                feedHolder.unpaid_text_trips.setEnabled(false);
                feedHolder.unpaid_text_trips.setClickable(false);
                Utils.showDialog(mContext, mContext.getString(R.string.please_Wait));
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        feedHolder.unpaid_text_trips.setEnabled(true);
                        feedHolder.unpaid_text_trips.setClickable(true);
                    }
                }, 3000);
              //  Requests.pay(id.get(position));
                Requests.evocaPayment(id.get(position));
            }
        });
        if (end.get(position).equals("")) {
            feedHolder.end_text_view.setText(mContext.getString(R.string.till_now));
            feedHolder.distance_text_trips.setText("--");
            feedHolder.kcal_text_trips.setText("--");
            feedHolder.minutes_number_text.setText("--");
        } else {
            feedHolder.end_text_view.setText(end.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return id.size();
    }

    public void setOnItemClickListener(TripsAdapter.ClickListener clickListener) {
        TripsAdapter.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
    }

    public static class FeedViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ConstraintLayout trips_minutes_layout;
        private TextView minutes_number_text;
        private RelativeLayout general_trips_item;
        private TextView start_text_view;
        private TextView end_text_view;
        private TextView paid_text_trips;
        private TextView price_number_trips;
        private TextView kcal_text_trips;
        private TextView distance_text_trips;
        private TextView min_for_free;
        private TextView unpaid_text_trips;
        private Button go_to_trip;

        FeedViewHolder(View itemView) {
            super(itemView);
            minutes_number_text = itemView.findViewById(R.id.minutes_number_text);
            min_for_free = itemView.findViewById(R.id.min_for_free);
            trips_minutes_layout = itemView.findViewById(R.id.trips_minutes_layout);
            general_trips_item = itemView.findViewById(R.id.general_trips_item);
            start_text_view = itemView.findViewById(R.id.start_text_view);
            end_text_view = itemView.findViewById(R.id.end_text_view);
            paid_text_trips = itemView.findViewById(R.id.paid_text_trips);
            price_number_trips = itemView.findViewById(R.id.price_number_trips);
            kcal_text_trips = itemView.findViewById(R.id.kcal_text_trips);
            unpaid_text_trips = itemView.findViewById(R.id.unpaid_text_trips);
            distance_text_trips = itemView.findViewById(R.id.distance_text_trips);
            go_to_trip = itemView.findViewById(R.id.go_to_trip);
            general_trips_item.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }
    }

}