package com.imed.mimo.bike.adapters;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.widget.TextView;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.imed.mimo.bike.helpers.Bike;
import com.imed.mimo.bike.listeners.ClusterListener;
import com.imed.mimo.bike.listeners.ClusteredListener;

public class OwnIconRendered extends DefaultClusterRenderer<Bike> {

    public OwnIconRendered(Context context, GoogleMap map,
                           ClusterManager<Bike> clusterManager) {
        super(context, map, clusterManager);
    }


    @Override
    protected void onBeforeClusterItemRendered(final Bike item, final MarkerOptions markerOptions) {
        super.onBeforeClusterItemRendered(item, markerOptions);
        markerOptions.icon(item.getImage());
    }


    @Override
    protected void onBeforeClusterRendered(Cluster<Bike> cluster, MarkerOptions markerOptions) {
        super.onBeforeClusterRendered(cluster, markerOptions);
    }

    @Override
    protected String getClusterText(int bucket) {
        return super.getClusterText(bucket).replace("+", "");
    }

    @Override
    protected int getBucket(Cluster<Bike> cluster) {
        return cluster.getSize();
    }

    @Override
    protected int getColor(int clusterSize) {
        return Color.parseColor("#EE302F");
    }
}