package com.imed.mimo.bike.connections

import com.imed.mimo.bike.listeners.*

class Callbacks {

    lateinit var verifyLoginListener: VerifyLoginListener
    fun bindListener(listener: VerifyLoginListener) {
        verifyLoginListener = listener
    }

    lateinit var updatePhotoListener: UpdatePhotoListener
    fun photoListener(listener: UpdatePhotoListener) {
        updatePhotoListener = listener
    }

    lateinit var goVerifyListner: GoVerifyListner
    fun verifyBindListener(listener: GoVerifyListner) {
        goVerifyListner = listener
    }

    lateinit var ameriaLinkListener: AmeriaLinkListener
    fun ameriaListener(listener: AmeriaLinkListener) {
        ameriaLinkListener = listener
    }

    lateinit var evocaLinkListener: EvocaLinkListener
    fun evocaListener(listener: EvocaLinkListener) {
        evocaLinkListener = listener
    }

    lateinit var evocaPayDebtListener: EvocaPayDebtListener
    fun evocaPayDebtListener(listener: EvocaPayDebtListener) {
        evocaPayDebtListener = listener
    }

    lateinit var payDeptListener: PayDebtListener
    fun payDeptListener(listener: PayDebtListener) {
        payDeptListener = listener
    }

    lateinit var payDebtStatusListener: PayDebtStatusListener
    fun payDebtStatusListener(listener: PayDebtStatusListener) {
        payDebtStatusListener = listener
    }

    lateinit var ameriaAttachedListener: AmeraAttachedListener
    fun ameriaAttachListener(listener: AmeraAttachedListener) {
        ameriaAttachedListener = listener
    }

    lateinit var activeUserListener: ActiveOrNotListener
    fun activeUserListener(listener: ActiveOrNotListener) {
        activeUserListener = listener
    }

    lateinit var bookListener: BookListener
    fun bookListener(listener: BookListener) {
        bookListener = listener
    }

    lateinit var updateUserError: UpdateUserError
    fun updateUserError(listener: UpdateUserError) {
        updateUserError = listener
    }

    lateinit var bookStopListener: BookStopListener
    fun bookStopListener(listener: BookStopListener) {
        bookStopListener = listener
    }

    lateinit var qrPermittedListener: QrPermittedListener
    fun qrPermittedListener(listener: QrPermittedListener) {
        qrPermittedListener = listener
    }

    lateinit var checkNumberListener: CheckNumberListener
    fun checkNumberListener(listener: CheckNumberListener) {
        checkNumberListener = listener
    }

    lateinit var bookRideStatus: BookRideStatus
    fun bookRideStatus(listener: BookRideStatus) {
        bookRideStatus = listener
    }

    lateinit var bikesListener: BikesListener
    fun bikesListener(listener: BikesListener) {
        bikesListener = listener
    }

    lateinit var bleStatusOpened: BlueStatusOpened
    fun bleStatusOpened(listener: BlueStatusOpened) {
        bleStatusOpened = listener
    }

    lateinit var scanButtonCallback: ScanButtonCallback
    fun scanButtonCallback(listener: ScanButtonCallback) {
        scanButtonCallback = listener
    }

    lateinit var tripsListener: TripsListener
    fun tripsListener(listener: TripsListener) {
        tripsListener = listener
    }

    lateinit var cardsListener: CardsListener
    fun cardsListener(listener: CardsListener) {
        cardsListener = listener
    }

    lateinit var tripRoutsListener: TripRouteListener
    fun tripRoutsListener(listener: TripRouteListener) {
        tripRoutsListener = listener
    }

    lateinit var getFreeListener: GetFreeListener
    fun getFreeListener(listener: GetFreeListener) {
        getFreeListener = listener
    }

    lateinit var solveListener: SolveListener
    fun solveListener(listener: SolveListener) {
        solveListener = listener
    }

    lateinit var evocaListener: EvocaListener
    fun evocaListener(listener: EvocaListener) {
        evocaListener = listener
    }
}