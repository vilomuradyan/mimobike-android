package com.imed.mimo.bike.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.imed.mimo.bike.R

class BlankFragment : Fragment(){

    private lateinit var titleTextView: TextView
    private lateinit var textTextView: TextView
    private var title = ""
    private var text = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_blank, container, false)
        titleTextView = view.findViewById(R.id.title)
        textTextView = view.findViewById(R.id.text)

        arguments?.let {
            title = it.getString("title")!!
            text = it.getString("text")!!
        }
        titleTextView.text = title
        textTextView.text = text
        return view
    }

    companion object {
        fun newInstance(title: String, text: String): BlankFragment {
            val args = Bundle()
            args.putString("title", title)
            args.putString("text", text)
            val fragment = BlankFragment()
            fragment.arguments = args
            return fragment
        }
    }

}
