package com.imed.mimo.bike.activities

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Configuration
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.webkit.WebChromeClient
import android.widget.FrameLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.imed.mimo.bike.R
import com.imed.mimo.bike.helpers.Utils
import kotlinx.android.synthetic.main.activity_agreement.*
import kotlinx.android.synthetic.main.custom_dialog_yes_no.view.*
import java.util.*
import kotlin.system.exitProcess

class AgreementActivity : AppCompatActivity() {

    private var languageLocale = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            languageLocale =
                getSharedPreferences("mimo_user_data", Context.MODE_PRIVATE).getString("lang", "")!!
            if (languageLocale != "") {
                changeLanguage(languageLocale)
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        setContentView(R.layout.activity_agreement)
        statusBarThings()
        i_accept.setOnClickListener {
            getSharedPreferences("mimo_user_data", Context.MODE_PRIVATE).edit()
                .putBoolean("agreed", true).apply()
            finish()
        }

        agreement.setLayerType(View.LAYER_TYPE_HARDWARE, null)
        agreement.settings.domStorageEnabled = true
        agreement.settings.javaScriptEnabled = true
        agreement.settings.loadWithOverviewMode = true
        agreement.webChromeClient = WebChromeClient()
        if (languageLocale != "") {
            when (languageLocale) {
                "en" -> agreement.loadUrl("https://www.mimobike.com/en/agreement")
                "ru" -> agreement.loadUrl("https://www.mimobike.com/ru/agreement")
                "hy" -> agreement.loadUrl("https://www.mimobike.com/am/agreement")
            }
        } else {
            when {
                Locale.getDefault().language == "en" -> agreement.loadUrl("https://www.mimobike.com/en/agreement")
                Locale.getDefault().language == "ru" -> agreement.loadUrl("https://www.mimobike.com/ru/agreement")
                Locale.getDefault().language == "hy" -> agreement.loadUrl("https://www.mimobike.com/am/agreement")
            }
        }
    }

    private fun changeLanguage(string: String) {
        val locale = Locale(string)
        val config = Configuration()
        config.locale = locale
        baseContext.resources.updateConfiguration(config, baseContext.resources.displayMetrics)
    }

    private fun statusBarThings() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.navigationBarColor = ContextCompat.getColor(this, R.color.colorPrimary)
            window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimary)
        } else {
            window.setFlags(
                WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS
            )
            val statusBarHeight = Utils.getStatusBarHeight(this)
            val view = View(this)
            view.layoutParams = FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            view.layoutParams.height = statusBarHeight
            (window.decorView as ViewGroup).addView(view)
            view.background = resources.getDrawable(R.drawable.navibg)
            val layoutParams = FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
            layoutParams.setMargins(0, Utils.getStatusBarHeight(this), 0, 0)
            relattt.layoutParams = layoutParams
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR or View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR
        }
    }

    @SuppressLint("InflateParams")
    override fun onBackPressed() {
        try {
            val adb = android.app.AlertDialog.Builder(this, R.style.AlertDialogCustom)
            val view = layoutInflater.inflate(R.layout.custom_dialog_yes_no, null) as RelativeLayout
            adb.setView(view)
            val dialog = adb.create()
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.show()
            val tvCount = view.findViewById<TextView>(R.id.dialogText)
            tvCount.text = getString(R.string.do_you_Want_to_leave)
            view.button_no.setOnClickListener {
                dialog.dismiss()
            }
            view.button_yes.setOnClickListener {
                dialog.dismiss()
                this.finishAffinity()
                exitProcess(0)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}