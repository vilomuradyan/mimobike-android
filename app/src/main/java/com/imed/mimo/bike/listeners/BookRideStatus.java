package com.imed.mimo.bike.listeners;

public interface BookRideStatus {
    void bookride(String ride, boolean opened, int duration);
}
