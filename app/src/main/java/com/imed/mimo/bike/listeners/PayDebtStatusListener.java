package com.imed.mimo.bike.listeners;

public interface PayDebtStatusListener {
    void payDebtStatusListener(String status);
}
