package com.imed.mimo.bike.helpers;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterItem;

public class Bike implements ClusterItem {
    private final LatLng mPosition;
    private String mTitle;
    private String id1;
    private BitmapDescriptor image1;

    public Bike(MarkerOptions markerOptions,String id) {
        this.mPosition = markerOptions.getPosition();
        this.mTitle = markerOptions.getTitle();
        this.image1 = markerOptions.getIcon();
        this.id1 = id;
    }

    @Override
    public LatLng getPosition() {
        return mPosition;
    }

    @Override
    public String getTitle() {
        return mTitle;
    }

    public String getId1() {
        return id1;
    }

    @Override
    public String getSnippet() {
        return null;
    }

    public BitmapDescriptor getImage() {
        return image1;
    }

}