package com.imed.mimo.bike.listeners;

import org.json.JSONObject;

public interface CardsListener {
    void cards(JSONObject jsonArray);
}
