package com.imed.mimo.bike.helpers

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.graphics.PorterDuff
import android.net.ConnectivityManager
import android.net.NetworkRequest
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.view.LayoutInflater
import android.widget.EditText
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationSettingsStatusCodes
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.Polyline
import com.imed.mimo.bike.R
import com.imed.mimo.bike.activities.SplashActivity.Companion.support
import com.imed.mimo.bike.connections.Requests
import de.hdodenhof.circleimageview.CircleImageView
import java.security.MessageDigest

class Utils2 {
    companion object {
        fun resetColorsOfMarker(marker: Marker, context: Context) {
            try {
                val inflater = LayoutInflater.from(context)
                val view1 = inflater.inflate(R.layout.marker_view, null, false)
                val markerView = view1.findViewById<RelativeLayout>(R.id.markerView)
                val markerLayout = view1.findViewById<RelativeLayout>(R.id.markerLayout)
                val markerImage = view1.findViewById<CircleImageView>(R.id.imageViewMarker)
                val markerShadow = view1.findViewById<RelativeLayout>(R.id.shadow_pin)
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                    markerLayout.background.setColorFilter(
                        ContextCompat.getColor(
                            context,
                            R.color.colorPrimary
                        ), PorterDuff.Mode.SRC_ATOP
                    )
                    markerImage.background.setColorFilter(
                        ContextCompat.getColor(
                            context,
                            android.R.color.black
                        ), PorterDuff.Mode.SRC_ATOP
                    )
                    markerShadow.background.setColorFilter(
                        ContextCompat.getColor(
                            context,
                            android.R.color.black
                        ), PorterDuff.Mode.SRC_ATOP
                    )
                }
                val newMarkerFromView = Utils.createBitmapFromLayout(markerView)
                val bitmap = BitmapDescriptorFactory.fromBitmap(newMarkerFromView)
                marker.setIcon(bitmap)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        @JvmStatic
        fun hashString(type: String, input: String): String {
            val HEX_CHARS = "0123456789ABCDEF"
            val bytes = MessageDigest
                .getInstance(type)
                .digest(input.toByteArray())
            val result = StringBuilder(bytes.size * 2)
            bytes.forEach {
                val i = it.toInt()
                result.append(HEX_CHARS[i shr 4 and 0x0f]).append(HEX_CHARS[i and 0x0f])
            }
            return result.toString()
        }

        fun validateEmail(editText: EditText, context: Context): Boolean {
            return try {
                if (!Constants.validateEmailAddress(editText.text.toString())) {
                    editText.setTextColor(ContextCompat.getColor(context, R.color.red))
                    false
                } else {
                    editText.setTextColor(ContextCompat.getColor(context, R.color.colorBlack))
                    true
                }
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
                false
            }
        }

        fun imagePicker(activity: Activity) {
            val photoPickerIntent = Intent(Intent.ACTION_PICK)
            photoPickerIntent.type = "image/*"
            activity.startActivityForResult(photoPickerIntent, Constants.select_photo)
        }

        fun call(activity: Activity) {
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:" + support)
            activity.startActivity(intent)
        }


        fun imageCapture(activity: Activity) {
            if (Utils.isPermissionGranted(
                    android.Manifest.permission.CAMERA,
                    Constants.camera_permission,
                    activity
                )
            ) {
                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                activity.startActivityForResult(cameraIntent, Constants.camera_permission)
            }
        }

        fun imageCapturePassport(activity: Activity) {
            if (Utils.isPermissionGranted(
                    android.Manifest.permission.CAMERA,
                    Constants.camera_permission_passp,
                    activity) &&  Utils.isPermissionGranted(
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Constants.camera_permission_passp,
                        activity)
            ) {
                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                val imageFile = Utils.getTemporalFile()
                val uri = Uri.fromFile(imageFile)
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
                activity.startActivityForResult(cameraIntent, Constants.camera_permission_passp)
            }
        }

        fun imagePickerGranted(activity: Activity) {
            if (Utils.isPermissionGranted(
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Constants.storage_permission,
                    activity
                )
            ) {
                imagePicker(activity)
            }
        }

        fun enableLoc(mGoogleApiClient: GoogleApiClient, activity: Activity) {
            val locationRequest = LocationRequest.create()
            locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            locationRequest.interval = 6 * 1000
            locationRequest.fastestInterval = 5 * 1000
            val builder = LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest)
            builder.setAlwaysShow(true)
            val result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build())
            result.setResultCallback {
                val status = it.status
                when (status.statusCode) {
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                        try {
                            status.startResolutionForResult(activity, 199)
                        } catch (e: IntentSender.SendIntentException) {
                            e.printStackTrace()
                        }
                    }
                }
            }
        }

        fun moveToBounds(p: Polyline, map: GoogleMap) {
            val builder = LatLngBounds.Builder()
            val arr = p.points
            for (i in 0 until arr.size) {
                builder.include(arr[i])
            }
            val bounds = builder.build()
            val padding = if (Build.VERSION.SDK_INT <= 23) {
                60
            } else {
                160
            }
            val cu = CameraUpdateFactory.newLatLngBounds(bounds, padding)
            map.animateCamera(cu)
        }

        fun connectivityManager(networkCallback: ConnectivityManager.NetworkCallback, context: Context): ConnectivityManager {
            val manager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                manager.registerDefaultNetworkCallback(networkCallback)
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    val builder = NetworkRequest.Builder()
                    manager.registerNetworkCallback(builder.build(), networkCallback)
                }
            }
            return manager
        }

        fun showDialog(textView: TextView, birthday: String, context: Context, isPassort: Boolean): String {
            val mYear = 1998
            val mMonth = 4
            val mDay = 1
            var date = ""
            val dpd = DatePickerDialog(
                context,
                DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                    val month = if (monthOfYear < 9) {
                        "0" + (monthOfYear + 1).toString()
                    } else {
                        (monthOfYear + 1).toString()
                    }
                    val day = if (dayOfMonth < 10) {
                        "0$dayOfMonth"
                    } else {
                        dayOfMonth.toString()
                    }
                    date = "$day.$month.$year"
                    if(!isPassort) {
                        if (birthday != date) {
                            Requests.userDataToSend("birthday", date)
                        }
                    }
                    textView.text = date
                },
                mYear,
                mMonth,
                mDay
            )
            dpd.datePicker.maxDate = System.currentTimeMillis()
            dpd.show()
            dpd.datePicker.touchables[0].performClick()
            return date
        }
    }
}