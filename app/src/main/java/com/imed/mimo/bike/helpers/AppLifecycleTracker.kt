package com.imed.mimo.bike.helpers

import android.app.Activity
import android.app.Application
import android.os.Bundle
import com.imed.mimo.bike.activities.SplashActivity.Companion.dontopenMap
import com.imed.mimo.bike.activities.SplashActivity.Companion.isInSplash
import com.imed.mimo.bike.helpers.Constants.Companion.fromBackgroundBikes
import com.imed.mimo.bike.helpers.Constants.Companion.fromBackgroundStatus

class AppLifecycleTracker : Application.ActivityLifecycleCallbacks  {
    override fun onActivityPaused(activity: Activity?) {
    }

    override fun onActivityResumed(activity: Activity?) {
    }

    override fun onActivityDestroyed(activity: Activity?) {
    }

    override fun onActivitySaveInstanceState(activity: Activity?, outState: Bundle?) {
    }

    override fun onActivityCreated(activity: Activity?, savedInstanceState: Bundle?) {
    }

    private var numStarted = 0
    private var appStarted = true

    override fun onActivityStarted(activity: Activity?) {
        if (numStarted == 0) {
            if(!appStarted) {
                try {
//                    Requests.getBikesVolley(activity!!)
//                    Requests.bookRideStatusVolley(activity)
                    fromBackgroundStatus = true
                    fromBackgroundBikes = true
                } catch (e: UninitializedPropertyAccessException){
                    e.printStackTrace()
                }
            }
            appStarted = false
        }
        numStarted++
    }

    override fun onActivityStopped(activity: Activity?) {
        numStarted--
        if (numStarted == 0) {
            if(isInSplash){
                dontopenMap = true
            }
        }
    }
}