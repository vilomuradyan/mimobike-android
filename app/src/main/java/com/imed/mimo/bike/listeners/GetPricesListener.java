package com.imed.mimo.bike.listeners;

public interface GetPricesListener {
    void getPrices();
}
