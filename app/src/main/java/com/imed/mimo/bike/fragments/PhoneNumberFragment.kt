package com.imed.mimo.bike.fragments

import android.annotation.SuppressLint
import android.app.Dialog
import android.location.Location
import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.EditText
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.imed.mimo.bike.R
import com.imed.mimo.bike.activities.JoinActivity
import com.imed.mimo.bike.activities.SplashActivity.Companion.callback
import com.imed.mimo.bike.connections.Requests
import com.imed.mimo.bike.helpers.Utils
import com.imed.mimo.bike.listeners.CheckNumberListener
import com.imed.mimo.bike.listeners.GoVerifyListner
import kotlinx.android.synthetic.main.fragment_phone_number.*
import kotlinx.android.synthetic.main.fragment_phone_number.view.*



class PhoneNumberFragment : BottomSheetDialogFragment() {

    lateinit var edittext6: EditText
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var mLocationX: Double = 0.0
    private var mLocationY: Double = 0.0

    override fun getTheme(): Int = R.style.BottomSheetDialogTheme

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_phone_number, container, false)
        edittext6 = view.findViewById(R.id.editText6)
        view.phone_edit_text.requestFocus()
        dialog!!.window!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        goVerifyListener(view)
        getCountryCode(view)
        sendRegisterClient(view)
        checkNumberListener()
        verifyButton(view)
        backVerify(view)
        getLocation()
        return view
    }

    override fun getDialog(): Dialog? {
        val dialog2 = super.getDialog()
        dialog2!!.setOnShowListener { dialog ->
            val d = dialog as BottomSheetDialog
            val bottomSheet =
                d.findViewById(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout?
            BottomSheetBehavior.from(bottomSheet!!).state = BottomSheetBehavior.STATE_EXPANDED
        }
        return dialog2
    }

    private fun getCountryCode(view: View) {
        view.phone_edit_text.setText(view.country_spiner.selectedCountryCodeWithPlus)
        view.phone_edit_text.setSelection(view.phone_edit_text.text.length)
        view.country_spiner.setOnCountryChangeListener {
            view.phone_edit_text.setText(view.country_spiner.selectedCountryCodeWithPlus)
            view.phone_edit_text.setSelection(view.phone_edit_text.text.length)
        }
    }

    private fun getLocation(){
        try {
            fusedLocationClient =
                LocationServices.getFusedLocationProviderClient(activity as JoinActivity)
            fusedLocationClient.lastLocation.addOnSuccessListener { location: Location? ->
                try {
                    if(location != null) {
                        mLocationX = location.latitude
                        mLocationY = location.longitude
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                } catch (e: KotlinNullPointerException) {
                    e.printStackTrace()
                }
            }
        } catch (e: java.lang.Exception){
            e.printStackTrace()
        }
    }

    private fun checkNumberListener() {
        try {
            callback.checkNumberListener(CheckNumberListener {
                (activity as JoinActivity).runOnUiThread {
                    Utils.showDialog(activity as JoinActivity, getString(R.string.auth_error))
                    continue_button.alpha = 1f
                    continue_button.isEnabled = true
                    continue_button.isClickable = true
                }
            })
        } catch (e: UninitializedPropertyAccessException) {
            e.printStackTrace()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun sendRegisterClient(view: View) {
        view.continue_button.setOnClickListener {
            view.continue_button.alpha = 0.5f
            view.continue_button.isEnabled = false
            view.continue_button.isClickable = false
            Requests.enter(mLocationX, mLocationY, view.phone_edit_text.text.toString())
        }
    }

    private fun goVerifyListener(view: View) {
        callback.verifyBindListener(GoVerifyListner {
            (activity as JoinActivity).runOnUiThread {
                this.isCancelable = false
                view.phoneNumber.text = view.phone_edit_text.text.toString()
                edittext6.requestFocus()
                if(view.phoneLayout.visibility != View.GONE) {
                    sendAgainTimer(view)
                }
                view.phoneLayout.visibility = View.GONE
                view.verifyLayout.visibility = View.VISIBLE
            }
        })
    }

    fun backVerify(view: View) {
        view.back_verify.setOnClickListener {
            view.continue_button.alpha = 1f
            view.continue_button.isEnabled = true
            view.continue_button.isClickable = true
            this.isCancelable = true
            view.phoneLayout.visibility = View.VISIBLE
            view.verifyLayout.visibility = View.GONE
        }
    }

    private fun verifyButton(view: View) {
        view.verify_button.setOnClickListener {
            val smsNumbers = edittext6.text.toString()
            Requests.validate(view.phoneNumber.text.toString(), smsNumbers)
        }
    }

    @SuppressLint("SetTextI18n")
    fun sendAgainTimer(view: View) {
        try {
            object : CountDownTimer(59000, 1000) {
                override fun onTick(millisUntilFinished: Long) {
                    if (isAdded) {
                        try {
                            if(millisUntilFinished / 1000 > 10) {
                                view.sendAgain.text =
                                    getString(R.string.you_will_receive_the_sms_in_0_) + millisUntilFinished / 1000
                            } else {
                                view.sendAgain.text =
                                    getString(R.string.you_will_receive_the_sms_in_0_0) + millisUntilFinished / 1000
                            }
                        } catch (e: IllegalStateException) {
                            e.printStackTrace()
                        }
                    }
                }

                override fun onFinish() {
                    if (isAdded) {
                        try {
                            view.sendAgain.text = getString(R.string.resend_the_sms)
                            view.sendAgain.setTextColor(ContextCompat.getColor(context!!, R.color.red))
                            view.sendAgain.setOnClickListener {
//                                (activity as JoinActivity).sendSendAgainClient(view.phone_edit_text.text.toString())
                                Requests.enter(mLocationX, mLocationY, view.phone_edit_text.text.toString())
                                view.sendAgain.setOnClickListener { }
                                view.sendAgain.setTextColor(ContextCompat.getColor(context!!, R.color.colorText))
                            }
                        } catch (e: IllegalStateException) {
                            e.printStackTrace()
                        }
                    }
                }
            }.start()
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }
}