package com.imed.mimo.bike.model

import com.google.gson.annotations.SerializedName

class UpdateModel(
    @SerializedName("appVersion") val appVersion : String,
    @SerializedName("deviceType") val deviceType : String,
    @SerializedName("id") val id : String,
    @SerializedName("osVersion") val osVersion : String
)