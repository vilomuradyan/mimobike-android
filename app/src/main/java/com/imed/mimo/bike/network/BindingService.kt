package com.imed.mimo.bike.network

import com.imed.mimo.bike.model.BindingModel
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST

interface BindingService {

    @Headers("Content-Type:application/json")
    @POST("binding/register")
    fun binding(@Header("Authorization") authHeader : String,
                @Body binding : BindingModel
    ) : Observable<BindingModel>
}