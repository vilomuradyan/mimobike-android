package com.imed.mimo.bike.helpers;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class BlUtils {

    public static byte[] decrypt(byte[] sSrc, byte[] sKey) {

        try {
            SecretKeySpec skeySpec = new SecretKeySpec(sKey, "AES");
            Cipher cipher = Cipher.getInstance("AES/ECB/NoPadding");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec);
            return cipher.doFinal(sSrc);

        } catch (Exception ex) {
            return null;
        }
    }

    public static byte[] encrypt(byte[] sSrc, byte[] sKey) {
        try {
            SecretKeySpec skeySpec = new SecretKeySpec(sKey, "AES");
            Cipher cipher = Cipher.getInstance("AES/ECB/NoPadding");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
            return cipher.doFinal(sSrc);
        } catch (Exception ex) {
            return null;
        }
    }

    public static byte[] get() {
        byte[] command = new byte[16];
        command[0] = (byte) 0x7c;
        command[1] = (byte) 0x90;
        command[2] = (byte) 0xe3;
        command[3] = (byte) 0x19;
        command[4] = (byte) 0x97;
        command[5] = (byte) 0xca;
        command[6] = (byte) 0xa0;
        command[7] = (byte) 0xc8;
        command[8] = (byte) 0xea;
        command[9] = (byte) 0x5c;
        command[10] = (byte) 0x50;
        command[11] = (byte) 0xbb;
        command[12] = (byte) 0x20;
        command[13] = (byte) 0x81;
        command[14] = (byte) 0xe1;
        command[15] = (byte) 0x56;
        return command;
    }
}
