package com.imed.mimo.bike.network

import com.imed.mimo.bike.model.UpdateModel
import com.imed.mimo.bike.model.UpdateResponse
import io.reactivex.Observable
import retrofit2.http.*

interface UpdateService {

    @Headers("Content-Type:application/json")
    @POST("apk-version/{osType}")
    fun update(@Header("Authorization") authHeader : String,
               @Body payment : UpdateModel, @Path("osType") osType : String
    ) : Observable<UpdateResponse>
}