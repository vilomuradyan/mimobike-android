package com.imed.mimo.bike.activities

import android.annotation.SuppressLint
import android.app.ActivityManager
import android.app.Dialog
import android.bluetooth.BluetoothDevice
import android.content.Context
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkRequest
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.text.TextUtils
import android.util.Base64
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.FirebaseApp
import com.google.firebase.crashlytics.CrashlyticsRegistrar
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.google.firebase.crashlytics.internal.common.CrashlyticsCore
import com.google.firebase.iid.FirebaseInstanceId
import com.imed.mimo.bike.R
import com.imed.mimo.bike.connections.Callbacks
import com.imed.mimo.bike.connections.Requests
import com.imed.mimo.bike.helpers.Constants.Companion.dontDoPush
import com.imed.mimo.bike.helpers.Constants.Companion.mWebSocket
import com.imed.mimo.bike.helpers.Constants.Companion.token
import com.imed.mimo.bike.helpers.Utils
import com.imed.mimo.bike.model.UpdateModel
import com.imed.mimo.bike.model.UpdateResponse
import com.imed.mimo.bike.network.UpdateService
import com.imed.mimo.bike.network.createMimoService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.security.MessageDigest
import java.util.*

class SplashActivity : AppCompatActivity(R.layout.activity_splash) {
    private lateinit var manager: ConnectivityManager
    private lateinit var dialog: Dialog
    private var version : String = "null"
    private var deviceName : String = "null"
    private var osTypeDevice : String = "null"
    private var languageLocale = ""
    private var a : String? = null
    private var updateService : UpdateService = createMimoService()
    companion object {
        @JvmField var isUpdate = false
        @JvmField var callback = Callbacks()
        lateinit var sessionID: String
        @JvmField var userId: String = ""
        @JvmField var paired: Boolean = false
        @JvmField var isInSplash: Boolean = true
        @JvmField var dontopenMap: Boolean = false
        var lockDevice: BluetoothDevice? = null
        @JvmField var cardBool = 1
        @JvmField var bookPrice = 0.0
        @JvmField var holdPrice = 0.0
        @JvmField var mode = ""
        @JvmField var debt = false
        @JvmField var cardDebt = false
        @JvmField var ridePrice = 0.0
        @JvmField var studentPrice = 0.0
        @JvmField var bonus = 0
        @JvmField var freeMinPrice = 0
        @JvmField var weekendPrice = 0.0
        @JvmField var distance_km = 0.0
        @JvmField var trees_saved = 0
        @JvmField var calRate = 0
        @JvmField var treeRate = 0
        @JvmField var calories = 0.0
        @JvmField var minimal = 0.0
        @JvmField var freeMinutes = 0
        @JvmField var support = ""
        @JvmField var inBook = false
        @JvmField var inRide = false
        var inBluetoothActivity = false
        @JvmField var inRideStatus = false
        @JvmField var bookedBikeId = ""
        @JvmField var rideBikeId = ""
        @JvmField var rideBikeLat = 0.0
        @JvmField  var macAddress = ""
        @JvmField var rideBikeLong = 0.0
        @JvmField var bookedBikeLat = 0.0
        @JvmField var bookedBikeLong = 0.0
        @JvmField var bookedBikeTime = 0
        @JvmField var bikeId = ""
        @JvmField var activeOrNo = false
        @JvmField var profileOk = false
        @JvmField var isOpenLock = false
        @JvmField var bookRequested = false
        @JvmField var qrscanned = false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Utils.adjustFontScale(resources.configuration, this@SplashActivity)
        firebaseGetToken()
        mWebSocket.changeContext(this)
        getAppVersion()
        updateRequest(UpdateModel(version,deviceName, sessionID, osTypeDevice))
        statusBarThings()
        connectivityManager()
        sendConnectClient()
        taskManagerIcon()
        try {
            languageLocale =
                getSharedPreferences("mimo_user_data", Context.MODE_PRIVATE).getString("lang", "")!!
            if (languageLocale != "") {
                changeLanguage(languageLocale)
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    private fun changeLanguage(string: String) {
        val locale = Locale(string)
        val config = Configuration()
        config.locale = locale
        baseContext.resources.updateConfiguration(config, baseContext.resources.displayMetrics)
    }

   private fun getDeviceName(): String? {
        val manufacturer = Build.MANUFACTURER
        val model = Build.MODEL
        return if (model.startsWith(manufacturer)) {
            capitalize(model)
        } else capitalize(manufacturer) + " " + model
    }

    private fun capitalize(str: String): String {
        if (TextUtils.isEmpty(str)) {
            return str
        }
        val arr = str.toCharArray()
        var capitalizeNext = true
        val phrase = java.lang.StringBuilder()
        for (c in arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase.append(Character.toUpperCase(c))
                capitalizeNext = false
                continue
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true
            }
            phrase.append(c)
        }
        return phrase.toString()
    }

    private fun getAppVersion(){
        try {
            sessionID = hashString(
                "SHA-256",
                Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)
            )
            val pInfo: PackageInfo = applicationContext.packageManager.getPackageInfo(packageName, 0)
            if (pInfo.versionName != null){
                version = pInfo.versionName
            }
            if (getDeviceName()!!.isNotEmpty()){
                deviceName = getDeviceName()!!
            }
            if (getOsTypeDevice().isNotEmpty()){
                osTypeDevice = getOsTypeDevice()
            }

            Log.d("VERSION", "VERSION = $version")
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
    }

    private fun getOsTypeDevice() : String{
        return Build.VERSION.RELEASE
    }

    @SuppressLint("CheckResult")
    private fun updateRequest(update : UpdateModel){
        val username = "api"
        val password = "aPi_Passw0rd-2020"
        val base = "$username:$password"
        val authHeader = "Basic " + Base64.encodeToString(base.toByteArray(), Base64.NO_WRAP)
        updateService.update(authHeader,update, "ANDROID")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::handlePaymentResponse, this::handlePaymentError)
    }

    private fun handlePaymentResponse(data : UpdateResponse){
        if (version.toDouble() < data.version){
            isUpdate = true
        }
        Log.d("TAGUPDATE", "SUCCESS")
        startConnect()
    }

    private fun handlePaymentError(error: Throwable){
        Log.d("TAGUPDATE", "{${error.localizedMessage}}")
        startConnect()
    }

    override fun onResume() {
        super.onResume()
        isInSplash = true
    }

    private fun firebaseGetToken() {
        FirebaseApp.initializeApp(this)
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    return@OnCompleteListener
                }
                token = task.result?.token!!
            })
    }

    private fun startConnect() {
        if (!dontDoPush) {
                mWebSocket.connectWebSocket(this)
        }
    }

    private fun connectivityManager() {
        manager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            manager.registerDefaultNetworkCallback(networkCallback)
        } else  if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            val builder = NetworkRequest.Builder()
            manager.registerNetworkCallback(builder.build(), networkCallback)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            manager.unregisterNetworkCallback(networkCallback)
        }
    }

    private val networkCallback =
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            object : ConnectivityManager.NetworkCallback() {
                override fun onAvailable(network: Network) {
                    super.onAvailable(network)
                    Log.i("vvv", "connected to " + if (manager.isActiveNetworkMetered) "LTE" else "WIFI")
                    try {
                        dialog.cancel()
                    } catch (e: UninitializedPropertyAccessException){
                        e.printStackTrace()
                    }
                }

                override fun onLost(network: Network) {
                    super.onLost(network)
                    Log.i("vvv", "losing active connection")
                    try {
                        dialog = Utils.showConnectionDialog(this@SplashActivity, getString(R.string.you_are_offline_internet))
                    }catch (e: java.lang.Exception) {
                        e.printStackTrace()
                    }
                }
            }
        } else {
            null
        }


    fun sendConnectClient() {
        Requests.free()
    }

    private fun taskManagerIcon(){
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                this.setTaskDescription(ActivityManager.TaskDescription("", R.drawable.logo))
            }
        } catch (e: Exception){
            e.printStackTrace()
        }
    }

    private fun statusBarThings() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.splashCOlor)
            window.navigationBarColor = ContextCompat.getColor(this, R.color.splashCOlor)
        } else {
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        }
        supportActionBar?.hide()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION)
        }
    }

    override fun onBackPressed() {

    }

    fun hashString(type: String, input: String): String {
        val HEX_CHARS = "0123456789ABCDEF"
        val bytes = MessageDigest
            .getInstance(type)
            .digest(input.toByteArray())
        val result = StringBuilder(bytes.size * 2)
        bytes.forEach {
            val i = it.toInt()
            result.append(HEX_CHARS[i shr 4 and 0x0f]).append(HEX_CHARS[i and 0x0f])
        }
        return result.toString()
    }
}
