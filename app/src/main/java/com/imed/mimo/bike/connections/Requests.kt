package com.imed.mimo.bike.connections

import android.annotation.SuppressLint
import android.app.Activity
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Base64
import android.util.Log
import com.imed.mimo.bike.activities.SplashActivity
import com.imed.mimo.bike.activities.SplashActivity.Companion.bikeId
import com.imed.mimo.bike.activities.SplashActivity.Companion.callback
import com.imed.mimo.bike.activities.SplashActivity.Companion.rideBikeId
import com.imed.mimo.bike.activities.SplashActivity.Companion.userId
import com.imed.mimo.bike.helpers.Constants
import com.imed.mimo.bike.helpers.Constants.Companion.mWebSocketClient
import com.imed.mimo.bike.helpers.Constants.Companion.sessionKey
import com.imed.mimo.bike.helpers.Constants.Companion.token
import com.imed.mimo.bike.helpers.Utils2
import com.imed.mimo.bike.model.BindingModel
import com.imed.mimo.bike.model.PaymentModel
import com.imed.mimo.bike.network.BindingService
import com.imed.mimo.bike.network.PaymentService
import com.imed.mimo.bike.network.createService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.json.JSONArray
import org.json.JSONObject
import java.util.*

class Requests {
    companion object {

        @SuppressLint("HardwareIds")
        @JvmStatic
        fun auth(activity: Activity, lat: Double, long: Double) {
            try {
                if (Constants.mWebSocketClient.isOpen) {
                    val auth = JSONObject()
                        .put("action", "auth")
                        .put(
                            "device", Utils2.hashString(
                                "SHA-256",
                                Settings.Secure.getString(
                                    activity.contentResolver,
                                    Settings.Secure.ANDROID_ID
                                )
                            )
                        )
                        .put("user", userId)
                    if (lat != 0.0) {
                        val locArray = JSONArray()
                        locArray.put(lat)
                        locArray.put(long)
                        auth.put("location", locArray)
                    }
                    mWebSocketClient.send(auth.toString())
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        @SuppressLint("HardwareIds")
        @JvmStatic
        fun regPush(activity: Activity) {
            try {
                if (token != "") {
                    if (Constants.mWebSocketClient.isOpen) {
                        val push = JSONObject()
                            .put("action", "push")
                            .put(
                                "device", Utils2.hashString(
                                    "SHA-256",
                                    Settings.Secure.getString(
                                        activity.contentResolver,
                                        Settings.Secure.ANDROID_ID
                                    )
                                )
                            )
                            .put("token", token)
                            .put("platform", 2)
                            .put("version", Build.VERSION.SDK_INT.toString())
                            .put("model", Build.MODEL)
                            .put("name", Build.DEVICE)
                            .put("language", Locale.getDefault().language)
                            .put("badge", true)
                            .put("alert", true)
                            .put("sound", true)
                            .toString()
                        mWebSocketClient.send(push)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        @SuppressLint("HardwareIds")
        @JvmStatic
        fun enter(lat: Double, long: Double, user: String) {
            try {
                if (Constants.mWebSocketClient.isOpen) {
                    val enter = JSONObject()
                        .put("action", "enter")
                        .put("device", SplashActivity.sessionID)
                        .put("user", user)
                    if (lat != 0.0) {
                        val locArray = JSONArray()
                        locArray.put(lat)
                        locArray.put(long)
                        enter.put("location", locArray)
                    }
                    mWebSocketClient.send(enter.toString())
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        @SuppressLint("HardwareIds")
        @JvmStatic
        fun validate(user: String, code: String) {
            try {
                if (Constants.mWebSocketClient.isOpen) {
                    val validate = JSONObject()
                        .put("action", "validate")
                        .put("device", SplashActivity.sessionID)
                        .put("user", user)
                        .put("code", code)
                    mWebSocketClient.send(validate.toString())
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        @SuppressLint("HardwareIds")
        @JvmStatic
        fun getProfile() {
            try {
                if (Constants.mWebSocketClient.isOpen) {
                    if (sessionKey != "") {
                        val validate = JSONObject()
                            .put("action", "get-profile")
                            .put("sk", sessionKey)
                        mWebSocketClient.send(validate.toString())
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        @SuppressLint("HardwareIds")
        @JvmStatic
        fun userDataToSend(updateWhat: String, text: String): JSONObject? {
            try {
                val userDataArray = JSONObject()
                if (sessionKey != "") {
                    userDataArray.put("action", "set-profile")
                    userDataArray.put("sk", sessionKey)
                    when (updateWhat) {
                        "name" -> userDataArray.put("name", text)
                        "surname" -> userDataArray.put("surname", text)
                        "email" -> userDataArray.put("email", text)
                        "sex" -> userDataArray.put("sex", text.toInt())
                        "birthday" -> userDataArray.put("birthday", text)
                    }
                    mWebSocketClient.send(userDataArray.toString())
                }
                return userDataArray
            } catch (e: Exception) {
                e.printStackTrace()
                return null
            }
        }

        @SuppressLint("HardwareIds")
        @JvmStatic
        fun getAvatar(mode: String, imageName: String): JSONObject? {
            try {
                val userDataArray = JSONObject()
                if (sessionKey != "") {
                    userDataArray.put("action", "download-image")
                    userDataArray.put("sk", sessionKey)
                    userDataArray.put("name", imageName)
                    userDataArray.put("mode", mode)
                    userDataArray.put("id", userId)
                    mWebSocketClient.send(userDataArray.toString())
                }
                return userDataArray
            } catch (e: Exception) {
                e.printStackTrace()
                return null
            }
        }

        @SuppressLint("HardwareIds")
        @JvmStatic
        fun uploadPhoto(mode: String, byteArray: ByteArray) {
            try {
                if (Constants.mWebSocketClient.isOpen && sessionKey != "") {
                    val zhukBytes =
                        byteArrayOf(240.toByte(), 159.toByte(), 144.toByte(), 158.toByte())
                    var updatePhoto = "upload-image".toByteArray()
                    updatePhoto = updatePhoto.plus(zhukBytes)
                    updatePhoto = updatePhoto.plus(mode.toByteArray())
                    updatePhoto = updatePhoto.plus(zhukBytes)
                    updatePhoto = updatePhoto.plus(sessionKey.toByteArray())
                    updatePhoto = updatePhoto.plus(zhukBytes)
                    updatePhoto = updatePhoto.plus(userId.toByteArray())
                    updatePhoto = updatePhoto.plus(zhukBytes)
                    updatePhoto = updatePhoto.plus(byteArray)
                    mWebSocketClient.send(updatePhoto)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        @SuppressLint("HardwareIds")
        @JvmStatic
        fun deleteAvatar(imageName: String): JSONObject? {
            return try {
                val userDataArray = JSONObject()
                if (sessionKey != "" && imageName != "") {
                    userDataArray.put("action", "remove-avatar")
                    userDataArray.put("sk", sessionKey)
                    userDataArray.put("name", imageName)
                    mWebSocketClient.send(userDataArray.toString())
                }
                userDataArray
            } catch (e: Exception) {
                e.printStackTrace()
                null
            }
        }

        @SuppressLint("HardwareIds")
        @JvmStatic
        fun attachCard(): JSONObject? {
            return try {
                val userDataArray = JSONObject()
                if (sessionKey != "") {
                    userDataArray.put("action", "attach-card")
                    userDataArray.put("sk", sessionKey)
                    userDataArray.put("lng", Locale.getDefault().language)
                    mWebSocketClient.send(userDataArray.toString())
                }
                userDataArray
            } catch (e: Exception) {
                e.printStackTrace()
                null
            }
        }
        @JvmStatic
        fun evocaBinding(){
            bindingRequest(BindingModel("amd",SplashActivity.sessionID, Locale.getDefault().language,userId))
        }

        private val bindingService: BindingService = createService()
        private val paymentService: PaymentService = createService()

        @SuppressLint("CheckResult")
        private fun bindingRequest(binding : BindingModel){
            val username = "user"
            val password = "user123"
            val base = "$username:$password"
            val authHeader = "Basic " + Base64.encodeToString(base.toByteArray(), Base64.NO_WRAP)
            bindingService.binding(authHeader,binding)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleBindingResponse, this::handleBindingError)
        }

        private fun handleBindingResponse(binding : BindingModel){
            callback.evocaLinkListener.evocaLink(binding.formUrl)
        }

        private fun handleBindingError(error: Throwable) {
            callback.evocaLinkListener.evocaLink("error")
        }

        @JvmStatic
        fun evocaPayment(actionId : String){
            paymentRequest(PaymentModel(actionId, "amd",SplashActivity.sessionID, Locale.getDefault().language, userId))
        }

        @SuppressLint("CheckResult")
        private fun paymentRequest(payment : PaymentModel){
            val username = "user"
            val password = "user123"
            val base = "$username:$password"
            val authHeader = "Basic " + Base64.encodeToString(base.toByteArray(), Base64.NO_WRAP)
            paymentService.payment(authHeader,payment)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handlePaymentResponse, this::handlePaymentError)
        }

        private fun handlePaymentResponse(data : PaymentModel){
            callback.evocaPayDebtListener.evocaLink(data.formUrl, "tripId")
        }

        private fun handlePaymentError(error: Throwable){
            callback.evocaPayDebtListener.evocaLink("error", "tripId")
        }

        @SuppressLint("HardwareIds")
        @JvmStatic
        fun getCards(): JSONObject? {
            return try {
                val userDataArray = JSONObject()
                if (sessionKey != "") {
                    userDataArray.put("action", "get-cards")
                    userDataArray.put("sk", sessionKey)
                    mWebSocketClient.send(userDataArray.toString())
                }
                userDataArray
            } catch (e: Exception) {
                e.printStackTrace()
                null
            }
        }

        @SuppressLint("HardwareIds")
        @JvmStatic
        fun removeCard(cardId: String): JSONObject? {
            return try {
                val userDataArray = JSONObject()
                if (sessionKey != "") {
                    userDataArray.put("action", "remove-card")
                    userDataArray.put("sk", sessionKey)
                    userDataArray.put("card_id", cardId.toInt())
                    mWebSocketClient.send(userDataArray.toString())
                }
                userDataArray
            } catch (e: Exception) {
                e.printStackTrace()
                null
            }
        }

        @SuppressLint("HardwareIds")
        @JvmStatic
        fun getBikes(): JSONObject? {
            return try {
                val userDataArray = JSONObject()
                userDataArray.put("action", "bikes")
                mWebSocketClient.send(userDataArray.toString())
                userDataArray
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
                null
            }
        }

        @SuppressLint("HardwareIds")
        @JvmStatic
        fun book(bikeId: String, lat: Double, long: Double): JSONObject? {
            try {
                val userDataArray = JSONObject()
                if (sessionKey != "") {
                    userDataArray.put("action", "book")
                    userDataArray.put("sk", sessionKey)
                    userDataArray.put("bike_id", bikeId)
                    if (lat != 0.0) {
                        val locArray = JSONArray()
                        locArray.put(lat)
                        locArray.put(long)
                        userDataArray.put("location", locArray)
                    }
                    mWebSocketClient.send(userDataArray.toString())
                }
                return userDataArray
            } catch (e: Exception) {
                e.printStackTrace()
                return null
            }
        }

        @SuppressLint("HardwareIds")
        @JvmStatic
        fun cancelBook(bikeId: String, lat: Double, long: Double): JSONObject? {
            try {
                val userDataArray = JSONObject()
                if (sessionKey != "") {
                    userDataArray.put("action", "cancel-book")
                    userDataArray.put("sk", sessionKey)
                    userDataArray.put("bike_id", bikeId)
                    if (lat != 0.0) {
                        val locArray = JSONArray()
                        locArray.put(lat)
                        locArray.put(long)
                        userDataArray.put("location", locArray)
                    }
                    mWebSocketClient.send(userDataArray.toString())
                }
                return userDataArray
            } catch (e: Exception) {
                e.printStackTrace()
                return null
            }
        }

        @SuppressLint("HardwareIds")
        @JvmStatic
        fun alarm(bikeId: String): JSONObject? {
            return try {
                val userDataArray = JSONObject()
                if (sessionKey != "") {
                    userDataArray.put("action", "beep")
                    userDataArray.put("sk", sessionKey)
                    userDataArray.put("bike_id", bikeId)
                    mWebSocketClient.send(userDataArray.toString())
                }
                userDataArray
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
                null
            }
        }

        @SuppressLint("HardwareIds")
        @JvmStatic
        fun validate(qr: String, lat: Double, long: Double): JSONObject? {
            try {
                val userDataArray = JSONObject()
                if (sessionKey != "") {
                    bikeId = qr
                    userDataArray.put("action", "validate-qr")
                    userDataArray.put("sk", sessionKey)
                    userDataArray.put("qr", qr)
                    if (lat != 0.0) {
                        val locArray = JSONArray()
                        locArray.put(lat)
                        locArray.put(long)
                        userDataArray.put("location", locArray)
                    }
                    mWebSocketClient.send(userDataArray.toString())
                }
                return userDataArray
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
                return null
            }
        }

        @SuppressLint("HardwareIds")
        @JvmStatic
        fun getTrips(): JSONObject? {
            return try {
                val userDataArray = JSONObject()
                if (sessionKey != "") {
                    userDataArray.put("action", "get-trips")
                    userDataArray.put("sk", sessionKey)
                    mWebSocketClient.send(userDataArray.toString())
                }
                userDataArray
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
                null
            }
        }

        @SuppressLint("HardwareIds")
        @JvmStatic
        fun free(): JSONObject? {
            return try {
                val userDataArray = JSONObject()
                if (sessionKey != "") {
                    userDataArray.put("action", "free-minutes")
                    userDataArray.put("sk", sessionKey)
                    mWebSocketClient.send(userDataArray.toString())
                }
                userDataArray
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
                null
            }
        }

        @SuppressLint("HardwareIds")
        @JvmStatic
        fun pay(rideId: String): JSONObject? {
            return try {
                val userDataArray = JSONObject()
                if (sessionKey != "") {
                    userDataArray.put("action", "m-pay")
                    userDataArray.put("sk", sessionKey)
                    userDataArray.put("id", rideId)
                    userDataArray.put("lng", Locale.getDefault().language)
                    mWebSocketClient.send(userDataArray.toString())
                }
                userDataArray
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
                null
            }
        }

        @SuppressLint("HardwareIds")
        @JvmStatic
        fun solve(): JSONObject? {
            return try {
                val userDataArray = JSONObject()
                if (sessionKey != "") {
                    userDataArray.put("action", "solve-ride")
                    userDataArray.put("sk", sessionKey)
                    userDataArray.put("id", rideBikeId)
                    mWebSocketClient.send(userDataArray.toString())
                }
                userDataArray
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
                null
            }
        }

        @SuppressLint("HardwareIds")
        @JvmStatic
        fun evoca(email: String, address: String, scn: String, base64: String, birthday: String): JSONObject? {
            return try {
                val userDataArray = JSONObject()
                if (sessionKey != "") {
                    userDataArray.put("action", "evoca-order")
                    userDataArray.put("sk", sessionKey)
                    userDataArray.put("email", email)
                    userDataArray.put("address", address)
                    userDataArray.put("scn", scn)
                    userDataArray.put("pass64", base64)
                    userDataArray.put("birthday", birthday)
                    mWebSocketClient.send(userDataArray.toString())
//                    Utils.deleteFile()
                }
                userDataArray
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
                null
            }
        }
    }
}