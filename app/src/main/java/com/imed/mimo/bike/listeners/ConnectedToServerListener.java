package com.imed.mimo.bike.listeners;

public interface ConnectedToServerListener {
    void connected();
}
