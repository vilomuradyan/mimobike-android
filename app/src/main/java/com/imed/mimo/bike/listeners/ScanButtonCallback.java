package com.imed.mimo.bike.listeners;

public interface ScanButtonCallback {
    void scanButton();
}
