package com.imed.mimo.bike.listeners;

public interface OfflineListener {
    void offline();
}
