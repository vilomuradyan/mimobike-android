package com.imed.mimo.bike.listeners;

public interface SolveListener {
    void solve(String status);
}
