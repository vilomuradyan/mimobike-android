package com.imed.mimo.bike.activities

import android.annotation.SuppressLint
import android.app.ActivityManager
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.media.AudioManager
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkRequest
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Base64
import android.util.Log
import android.view.WindowManager
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import com.imed.mimo.bike.R
import com.imed.mimo.bike.activities.SplashActivity.Companion.callback
import com.imed.mimo.bike.adapters.ViewPagerAdapter
import com.imed.mimo.bike.fragments.BlankFragment
import com.imed.mimo.bike.fragments.PhoneNumberFragment
import com.imed.mimo.bike.helpers.Constants
import com.imed.mimo.bike.helpers.Constants.Companion.internetLost
import com.imed.mimo.bike.helpers.Utils
import com.imed.mimo.bike.listeners.UpdatePhotoListener
import com.imed.mimo.bike.listeners.VerifyLoginListener
import kotlinx.android.synthetic.main.activity_join.*
import kotlinx.android.synthetic.main.custom_dialog_yes_no.view.*
import java.util.*

class JoinActivity : AppCompatActivity() {

    private lateinit var myPagerAdapter: ViewPagerAdapter
    private lateinit var phoneNumberFragment: PhoneNumberFragment
    private lateinit var runnable: Runnable
    var i = 0
    private lateinit var manager: ConnectivityManager
    private lateinit var dialog1: Dialog
    private var languageLocale = ""

    companion object {
        var isAtJoin = true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            languageLocale = getSharedPreferences("mimo_user_data", Context.MODE_PRIVATE).getString("lang", "")!!
            if (languageLocale != "") {
                changeLanguage(languageLocale)
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        setContentView(R.layout.activity_join)
        if(!getSharedPreferences("mimo_user_data", Context.MODE_PRIVATE).getBoolean("agreed", false)) {
            startActivity(Intent(this, AgreementActivity::class.java))
        }
        statusBarThings()
        setVideoBackground()
        Constants.mWebSocket.changeContext(this)
        viewPager()
        if (internetLost) {
            dialog1 = Utils.showConnectionDialog(this, getString(R.string.you_are_offline_internet))
        }
        openPhoneLayout()
        doItLater()
        connectivityManager()
        taskManagerIcon()
        verifyLoginListener()
        updatePhotoListener()
        isAtJoin = true
    }

    private fun changeLanguage(string: String) {
        val locale = Locale(string)
        val config = Configuration()
        config.locale = locale
        baseContext.resources.updateConfiguration(config, baseContext.resources.displayMetrics)
    }

    private fun connectivityManager() {
        manager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            manager.registerDefaultNetworkCallback(networkCallback)
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            val builder = NetworkRequest.Builder()
            manager.registerNetworkCallback(builder.build(), networkCallback)
        }
    }

    private fun verifyLoginListener() {
        try {
            callback.bindListener(VerifyLoginListener {
                runOnUiThread {
                    openMapActivity()
                    finish()
                }
            })
        } catch (e: UninitializedPropertyAccessException) {
            e.printStackTrace()
        }
    }

    private fun taskManagerIcon() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                setTaskDescription(
                    ActivityManager.TaskDescription("", R.drawable.logo)
                )
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    private fun updatePhotoListener() {
        try {
            callback.photoListener(UpdatePhotoListener { bitmap ->
                runOnUiThread {
                    try {
                        getSharedPreferences("mimo_user_data", Context.MODE_PRIVATE).edit().putString(
                            "imagedata",
                            Base64.encodeToString(bitmap, Base64.DEFAULT)
                        ).apply()
                    } catch (e: NullPointerException) {
                        e.printStackTrace()
                    }
                }
            })
        } catch (e: UninitializedPropertyAccessException) {
            e.printStackTrace()
        }
    }

    private val networkCallback = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        object : ConnectivityManager.NetworkCallback() {
            override fun onAvailable(network: Network) {
                super.onAvailable(network)
                Log.i("vvv", "connected to " + if (manager.isActiveNetworkMetered) "LTE" else "WIFI")
                if (internetLost) {
                    internetLost = false
                    try {
                        dialog1.cancel()
                    } catch (e: java.lang.IllegalArgumentException) {
                        e.printStackTrace()
                    }
                }
//                Requests.regPush(this@JoinActivity)
            }
            override fun onLost(network: Network) {
                super.onLost(network)
                Log.i("vvv", "losing active connection")
                try {
                    dialog1 = Utils.showConnectionDialog(
                        this@JoinActivity,
                        getString(R.string.you_are_offline_internet)
                    )
                    internetLost = true
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
        }
    } else {
        null
    }

    override fun onDestroy() {
        super.onDestroy()
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                manager.unregisterNetworkCallback(networkCallback)
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    private fun statusBarThings() {
        supportActionBar?.hide()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.navigationBarColor = ContextCompat.getColor(this, R.color.colorPrimary)
        } else {
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        }
        window.setFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        )
        window.setSoftInputMode(
            WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        )
    }

    @SuppressLint("InflateParams")
    override fun onBackPressed() {
        try {
            val adb = android.app.AlertDialog.Builder(this, R.style.AlertDialogCustom)
            val view = layoutInflater.inflate(R.layout.custom_dialog_yes_no, null) as RelativeLayout
            adb.setView(view)
            val dialog = adb.create()
            dialog.show()
            val tvCount = view.findViewById<TextView>(R.id.dialogText)
            tvCount.text = getString(R.string.do_you_Want_to_leave)
            view.button_no.setOnClickListener {
                dialog.dismiss()
            }
            view.button_yes.setOnClickListener {
                dialog.dismiss()
                finish()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @Suppress("DEPRECATION")
    private fun setVideoBackground() {
        val uri = Uri.parse("android.resource://com.imed.mimo.bike/raw/video")
        background_video.setVideoURI(uri)
        background_video.requestFocus()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            background_video.setAudioFocusRequest(AudioManager.AUDIOFOCUS_NONE)
        } else {
            val am = getSystemService(Context.AUDIO_SERVICE) as AudioManager
            am.requestAudioFocus(null, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN)
        }
        background_video.start()
        background_video.setOnCompletionListener {
            it.reset()
            background_video.setVideoURI(uri)
            background_video.start()
        }
    }

    override fun onResume() {
        super.onResume()
        setVideoBackground()
    }

    override fun onPause() {
        super.onPause()
        background_video.pause()
    }

    private fun viewPager() {
        myPagerAdapter = ViewPagerAdapter(supportFragmentManager)
        myPagerAdapter.addFragment(
            BlankFragment.newInstance(
                getString(R.string.dock_less),
                getString(R.string.the_bike_right)
            )
        )
        myPagerAdapter.addFragment(
            BlankFragment.newInstance(
                getString(R.string.smart_app),
                getString(R.string.pay_as_you_go)
            )
        )
        myPagerAdapter.addFragment(
            BlankFragment.newInstance(
                getString(R.string.join_us_today),
                getString(R.string.register_to_Start_first)
            )
        )
        pager.adapter = myPagerAdapter
        worm_dots_indicator.setViewPager(pager)
        viewPagerChangeEvery10Secs()
    }

    private fun viewPagerChangeEvery10Secs() {
        pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {}
            override fun onPageSelected(p0: Int) {
                i = p0
            }

            override fun onPageScrollStateChanged(p0: Int) {}
        })
        val handler = Handler()
        runnable = Runnable {
            pager.setCurrentItem(i, true)
            if (i == 3) {
                i = 0
            } else {
                i++
            }
        }
        Timer().schedule(object : TimerTask() {
            override fun run() {
                handler.post(runnable)
            }
        }, 100, 10000)
    }

    private fun openPhoneLayout() {
        join_mimo_now.setOnClickListener {
            phoneNumberFragment = PhoneNumberFragment()
            phoneNumberFragment.show(supportFragmentManager, "phone_number_bottom")
            supportFragmentManager.executePendingTransactions()
        }
    }

    private fun doItLater() {
        do_it_later.setOnClickListener {
            openMapActivity()
            isAtJoin = false
            finish()
        }
    }

    private fun openMapActivity() {
        val intent = Intent(this, MapActivity::class.java)
        startActivity(intent)
    }
}
