package com.imed.mimo.bike.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.FrameLayout
import android.widget.PopupMenu
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.imed.mimo.bike.R
import com.imed.mimo.bike.helpers.Constants
import com.imed.mimo.bike.helpers.Utils
import com.imed.mimo.bike.helpers.Utils2
import kotlinx.android.synthetic.main.activity_report.*

class ReportActivity : AppCompatActivity(R.layout.activity_report) {

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var path = ""
    private var paths = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        statusBarThings()
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        layouts()
        backListener()
        deleteImages()
        choosePhoto()
    }

    private fun layouts(){
        when {
            intent.getStringExtra("layout") == "bike_deffect" -> {
                select_broken_layout.visibility = VISIBLE
                report_toolbar_title.text = getString(R.string.roken_bike)
            }
            intent.getStringExtra("layout") == "lock_problem" -> {
                select_broken_layout.visibility = GONE
                report_toolbar_title.text = getString(R.string.lock_problem)
            }
            intent.getStringExtra("layout") == "wrong_parking" -> {
                select_broken_layout.visibility = GONE
                report_toolbar_title.text = getString(R.string.wrong_parking)
            }
        }
    }

    private fun choosePhoto(){
        choose_image_event.setOnClickListener {
            val popup = PopupMenu(this, choose_image_event)
            popup.inflate(R.menu.add_image)
            popup.menu.findItem(R.id.delete).isVisible = false
            popup.setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.capture_image -> {
                        Utils2.imageCapture(this)
                        true
                    }
                    R.id.gallery -> {
                        Utils2.imagePickerGranted(this)
                        true
                    }
                    else -> false
                }
            }
            popup.show()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == Constants.camera_permission) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                try {
                    val frg = supportFragmentManager.findFragmentByTag("bluetooth_fragment")!!
                    supportFragmentManager.beginTransaction().detach(frg).attach(frg).commit()
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
                Utils2.imageCapture(this)
            }
        } else if (requestCode == Constants.storage_permission) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Utils2.imagePicker(this)
            }
        }
    }

    @SuppressLint("MissingPermission")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val image: Bitmap?
        when (requestCode) {
            Constants.select_photo -> {
                try {
                    val selectedImage = data!!.data
                    val rotation = Utils.getRotation(applicationContext, selectedImage, false).toFloat()
                    val bm = Utils.decodeBitmap(applicationContext, selectedImage)
                    image = Utils.rotate(bm, rotation) as Bitmap
                    path = Utils.createFileFromBitmap(image)
                    setImages(image, path)
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
            Constants.camera_permission -> {
                try {
                    image = data?.extras!!.get("data") as Bitmap
                    path = Utils.createFileFromBitmap(image)
                    setImages(image, path)
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    private fun setImages(image: Bitmap, path: String){
        if(imageView9.visibility == VISIBLE){
            if(imageView10.visibility == VISIBLE) {
                imageView11.visibility = VISIBLE
                del_image11.visibility = VISIBLE
                imageView11.setImageBitmap(image)
                choose_image_event.visibility = GONE
                paths.add(2,path)
            } else {
                imageView10.visibility = VISIBLE
                del_image10.visibility = VISIBLE
                imageView10.setImageBitmap(image)
                paths.add(1,path)
            }
        } else {
            imageView9.visibility = VISIBLE
            del_image9.visibility = VISIBLE
            imageView9.setImageBitmap(image)
            paths.add(0,path)
        }
    }

    private fun deleteImages(){
        del_image9.setOnClickListener {
            paths.removeAt(0)
            choose_image_event.visibility = VISIBLE
            if(imageView10.visibility == VISIBLE) {
                imageView9.setImageBitmap((imageView10.drawable as BitmapDrawable).bitmap)
                if (imageView11.visibility == VISIBLE) {
                    imageView10.setImageBitmap((imageView11.drawable as BitmapDrawable).bitmap)
                    imageView11.visibility = GONE
                    del_image11.visibility = GONE
                } else {
                    imageView10.visibility = GONE
                    del_image10.visibility = GONE
                }
            } else {
                imageView9.visibility = GONE
                del_image9.visibility = GONE
            }
        }
        del_image10.setOnClickListener {
            paths.removeAt(1)
            choose_image_event.visibility = VISIBLE
            if(imageView11.visibility == VISIBLE) {
                imageView10.setImageBitmap((imageView11.drawable as BitmapDrawable).bitmap)
                imageView11.visibility = GONE
                del_image11.visibility = GONE
            } else {
                imageView10.visibility = GONE
                del_image10.visibility = GONE
            }
        }
        del_image11.setOnClickListener {
            paths.removeAt(2)
            choose_image_event.visibility = VISIBLE
            imageView11.visibility = GONE
            del_image11.visibility = GONE
        }
    }

    @SuppressLint("MissingPermission")
    private fun backListener(){
        button8.setOnClickListener {
            finish()
        }
        button_send.setOnClickListener {
            if(bikenumber_edittext.text.toString().length == 10) {
                finish()
//                if (report_toolbar_title.text == "Broken Bike") {
//                    if(parts.size > 0) {
//                        try {
//                            fusedLocationClient.lastLocation.addOnSuccessListener { location: Location? ->
//                                val latLng = LatLng(location!!.latitude, location.longitude)
//                                SendMail(
//                                    report_toolbar_title.text.toString(),
//                                    "bike number:  " + bikenumber_edittext.text.toString() + "\n" +
//                                        "broken parts:  " + parts.toString().substring(1, parts.toString().length-1) + "\n"+
//                                        "my location:  " + latLng +"\n" +
//                                        "my phone number:  " + userId,
//                                    paths,  callback).execute("")
//                                pleasewaitDialog = Utils.showConnectionDialog(this@ReportActivity,getString(R.string.please_Wait))
//                                button_send.isEnabled = false
//                                button_send.isClickable = false
//                            }
//                        } catch (e: java.lang.Exception){
//                            e.printStackTrace()
//                        }
//                    } else {
//                        Utils.showDialog(this@ReportActivity, getString(R.string.please_check_broken_part))
//                    }
//                } else {
//                    try {
//                        fusedLocationClient.lastLocation.addOnSuccessListener { location: Location? ->
//                            val latLng = LatLng(location!!.latitude, location.longitude)
//                            SendMail(
//                                report_toolbar_title.text.toString(),
//                                "bike number:  " + bikenumber_edittext.text.toString() + "\n" +
//                                    "my location:  " + latLng + "\n" +
//                                    "my phone number:  " + userId,
//                                paths, callback).execute("")
//                            pleasewaitDialog = Utils.showConnectionDialog(this@ReportActivity,getString(R.string.please_Wait))
//                            button_send.isEnabled = false
//                            button_send.isClickable = false
//                        }
//                    } catch (e: java.lang.Exception){
//                        e.printStackTrace()
//                    }
//                }
            } else {
                Utils.showDialog(this@ReportActivity, getString(R.string.qr_invalid))
            }
        }
    }

    private fun statusBarThings() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimary)
            window.navigationBarColor = ContextCompat.getColor(this, R.color.colorPrimary)
        } else {
            window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            val statusBarHeight = Utils.getStatusBarHeight(this)
            val view = View(this)
            view.layoutParams = FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            view.layoutParams.height = statusBarHeight
            (window.decorView as ViewGroup).addView(view)
            view.background = resources.getDrawable(R.drawable.navibg)
            val layoutParams = FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            layoutParams.setMargins(0, Utils.getStatusBarHeight(this), 0, 0)
            rela.layoutParams = layoutParams
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR or View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR
        }
    }
}
