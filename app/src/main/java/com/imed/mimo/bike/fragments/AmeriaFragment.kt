package com.imed.mimo.bike.fragments

import android.content.DialogInterface
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import com.imed.mimo.bike.R
import com.imed.mimo.bike.activities.MapActivity
import com.imed.mimo.bike.activities.SplashActivity
import kotlinx.android.synthetic.main.fragment_ameria.view.*

class AmeriaFragment : DialogFragment() {

    private var link = ""
    private var from_where = ""
//    private var tripId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(
            STYLE_NORMAL,
            android.R.style.Theme_NoTitleBar_Fullscreen)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_ameria, container, false)
        dialog?.window?.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialog?.window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            dialog?.window?.statusBarColor = ContextCompat.getColor((activity as MapActivity),R.color.ameria_color)
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            dialog?.window?.decorView?.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }
        arguments?.let {
            link = it.getString("ameria_link")!!
            from_where = it.getString("from_where")!!
//            tripId = it.getString("tripId")!!
        }
        dialog?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)

        view.ameria_web_view.setLayerType(View.LAYER_TYPE_HARDWARE, null)
        view.ameria_web_view.settings.domStorageEnabled = true
        view.ameria_web_view.settings.javaScriptEnabled = true
        view.ameria_web_view.settings.loadWithOverviewMode = true
        view.ameria_web_view.webChromeClient = WebChromeClient()
        view.ameria_web_view.loadUrl(link)
        view.ameria_web_view.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {
                val title = view.title
                if(title == "MIMO - Success") {
                    Handler().postDelayed({
                        try {
                            try {
                                fragmentManager!!.beginTransaction().remove(this@AmeriaFragment).commit()
                            } catch (e: Exception){
                                e.printStackTrace()
                            }
                            if(from_where == "attach") {
                                SplashActivity.callback.ameriaAttachedListener.ameriaCard(
                                    true,
                                    "")
                            } else {
                                SplashActivity.callback.payDebtStatusListener.payDebtStatusListener("OK")
                            }
                        } catch (e: Exception){
                            e.printStackTrace()
                        }

                    }, 2000)
                } else if(title == "MIMO - Fail") {
                    Handler().postDelayed({
                        try {
                            fragmentManager!!.beginTransaction().remove(this@AmeriaFragment)
                                .commit()
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                        if(from_where == "attach") {
                            SplashActivity.callback.ameriaAttachedListener.ameriaCard(false, "")
                        } else {
                            SplashActivity.callback.payDebtStatusListener.payDebtStatusListener("ERROR")
                        }
                    }, 2000)
                }
            }
        }
        return view
    }

    override fun onCancel(dialog: DialogInterface) {
        super.onCancel(dialog)
        (activity as MapActivity).cancelAmeriaDialog()
    }

    companion object {
        fun newInstance(link: String, from: String, tripId: String): AmeriaFragment {
            val args = Bundle()
            args.putString("ameria_link", link)
            args.putString("from_where", from)
//            args.putString("tripId", tripId)
            val fragment = AmeriaFragment()
            fragment.arguments = args
            return fragment
        }
    }
}
