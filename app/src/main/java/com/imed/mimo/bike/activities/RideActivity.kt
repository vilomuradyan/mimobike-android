package com.imed.mimo.bike.activities

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkRequest
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.imed.mimo.bike.R
import com.imed.mimo.bike.activities.SplashActivity.Companion.bikeId
import com.imed.mimo.bike.activities.SplashActivity.Companion.callback
import com.imed.mimo.bike.activities.SplashActivity.Companion.inRide
import com.imed.mimo.bike.activities.SplashActivity.Companion.rideBikeId
import com.imed.mimo.bike.activities.SplashActivity.Companion.rideBikeLat
import com.imed.mimo.bike.activities.SplashActivity.Companion.rideBikeLong
import com.imed.mimo.bike.connections.Requests
import com.imed.mimo.bike.helpers.Constants
import com.imed.mimo.bike.helpers.Constants.Companion.mWebSocket
import com.imed.mimo.bike.helpers.Utils
import com.imed.mimo.bike.helpers.Utils2
import com.imed.mimo.bike.listeners.BlueStatusOpened
import com.imed.mimo.bike.listeners.BookRideStatus
import com.imed.mimo.bike.listeners.SolveListener
import kotlinx.android.synthetic.main.activity_ride.*
import kotlinx.android.synthetic.main.custom_dialog_yes_no.view.*
import kotlin.system.exitProcess

class RideActivity : AppCompatActivity(), ActivityCompat.OnRequestPermissionsResultCallback {

    private var dialogShown = false
    private var dotAnim = 1
    private var mLocationX = 0.0
    private var mLocationY = 0.0
    private var bleOpened = false
    private var dontOpen = false
    private lateinit var manager: ConnectivityManager
    private lateinit var dialog: Dialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ride)
        mWebSocket.changeContext(this)
        bookRideStatus()
        bluetoothStatusOpened()
        getLocation()
        stopListener()
        statusBar()
        solveListener()
        connectivityManager()
    }

    private fun statusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR or View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR
        }
    }

    private fun getLocation() {
        val fusedLocationProviderClient:
                FusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        fusedLocationProviderClient.lastLocation
            .addOnSuccessListener(this) { location ->
                if (location != null) {
                    mLocationX = location.latitude
                    mLocationY = location.longitude
                }
            }
    }

    private fun stopListener() {
        stop_the_ride.setOnClickListener {
            Requests.solve()
        }
        support_image.setOnClickListener {
            if (Utils.isPermissionGranted(Manifest.permission.CALL_PHONE, Constants.call_permission, this)) {
                Utils2.call(this)
            }
        }
        go_to_maps_ride.setOnClickListener {
            val intent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse(
                    "http://maps.google.com/maps?w=saddr="
                            + mLocationX + "," + mLocationY + "&daddr="
                            + rideBikeLat + "," + rideBikeLong
                )
            )
            startActivity(intent)
        }
    }

    private fun solveListener(){
        callback.solveListener(SolveListener {
            runOnUiThread {
                if (it == "ok") {
                    Utils.showDialog(this, getString(R.string.please_Wait))
                    bikeId = ""
                    inRide = false
                    rideBikeId = ""
                    if (!dialogShown) {
                        dialogShown = true
                        Handler().postDelayed({
                            Constants.startedRide = false
                            finish()
                        }, 2000)
                        Utils.showDialog(this, getString(R.string.thank_you_for_ride))
                    } else {
                        Constants.startedRide = false
                        finish()
                    }
                } else {
                    try {
                        val adb = android.app.AlertDialog.Builder(this, R.style.AlertDialogCustom)
                        val view =
                            layoutInflater.inflate(R.layout.custom_dialog_stop_ride, null) as RelativeLayout
                        adb.setView(view)
                        val dialog = adb.create()
                        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                        dialog.show()
                        val tvCount = view.findViewById<TextView>(R.id.dialogText)
                        tvCount.text =
                            getString(R.string.dear_user) + "\n" + getString(R.string.you_have_currently)
                        view.button_no.setOnClickListener {
                            dialog.dismiss()
                        }
                        view.button_yes.setOnClickListener {
                            dialog.dismiss()
                            if (Utils.isPermissionGranted(
                                    Manifest.permission.CALL_PHONE,
                                    Constants.call_permission,
                                    this
                                )
                            ) {
                                Utils2.call(this)
                            }
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        })
    }

    private fun bookRideStatus() {
        if (bikeId != "") {
            bike_id.text = bikeId
        } else {
            bike_id.text = rideBikeId
        }
        try {
            callback.bookRideStatus(BookRideStatus { it, closed, duration ->
                runOnUiThread {
                    if (it == "ride") {
                        if (!closed) {
                            if (!dontOpen) {
                                lock.setImageResource(R.drawable.opened)
                                timer_ride.text = Utils.secondsToString(duration)
                            }
                        } else {
                            when (dotAnim) {
                                1 -> {
                                    timer_ride.text = getString(R.string.please_Wait) + "."
                                    dotAnim = 2
                                }
                                2 -> {
                                    timer_ride.text = getString(R.string.please_Wait) + ".."
                                    dotAnim = 3
                                }
                                else -> {
                                    timer_ride.text = getString(R.string.please_Wait)
                                    dotAnim = 1
                                }
                            }
                            if (!bleOpened) {
                                lock.setImageResource(R.drawable.closed)
                            }
                        }
                    } else {
                        bikeId = ""
                        inRide = false
                        rideBikeId = ""
                        if (!dialogShown) {
                            dialogShown = true
                            Constants.isRating = true
                            Handler().postDelayed({
                                Constants.startedRide = false
                                finish()
                            }, 2000)
                            Utils.showDialog(this, getString(R.string.thank_you_for_ride))
                        } else {
                            Constants.startedRide = false
                            finish()
                        }
                    }
                }
            })
        } catch (e: UninitializedPropertyAccessException) {
            e.printStackTrace()
        }
    }

    private fun connectivityManager() {
        manager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            manager.registerDefaultNetworkCallback(networkCallback)
        } else  if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            val builder = NetworkRequest.Builder()
            manager.registerNetworkCallback(builder.build(), networkCallback)
        }
    }


    private val networkCallback =
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            object : ConnectivityManager.NetworkCallback() {
                override fun onAvailable(network: Network) {
                    super.onAvailable(network)
                    Log.i("vvv", "connected to " + if (manager.isActiveNetworkMetered) "LTE" else "WIFI")
                    try {
                        dialog.cancel()
                    } catch (e: UninitializedPropertyAccessException){
                        e.printStackTrace()
                    }
                }

                override fun onLost(network: Network) {
                    super.onLost(network)
                    Log.i("vvv", "losing active connection")
                    try {
                        dialog = Utils.showConnectionDialog(this@RideActivity, getString(R.string.you_are_offline_internet))
                    } catch (e: java.lang.Exception) {
                        e.printStackTrace()
                    }
                }
            }
        } else {
            null
        }

    private fun bluetoothStatusOpened() {
        try {
            callback.bleStatusOpened(BlueStatusOpened { opened ->
                if (opened) {
                    bleOpened = true
                    runOnUiThread {
                        lock.setImageResource(R.drawable.opened)
                    }
                } else {
                    bleOpened = false
                    dontOpen = true
                    runOnUiThread {
                        bikeId = ""
                        inRide = false
                        rideBikeId = ""
                        lock.setImageResource(R.drawable.closed)
                        if (!dialogShown) {
                            dialogShown = true
                            Utils.showDialog(this, getString(R.string.thank_you_for_ride))
                            timer_ride.text = getString(R.string.please_Wait)
                        }
                    }
                }
            })
        } catch (e: UninitializedPropertyAccessException) {
            e.printStackTrace()
        }
    }

    @SuppressLint("InflateParams")
    override fun onBackPressed() {
        try {
            val adb = android.app.AlertDialog.Builder(this, R.style.AlertDialogCustom)
            val view = layoutInflater.inflate(R.layout.custom_dialog_yes_no, null) as RelativeLayout
            adb.setView(view)
            val dialog = adb.create()
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.show()
            val tvCount = view.findViewById<TextView>(R.id.dialogText)
            tvCount.text = getString(R.string.do_you_Want_to_leave)
            view.button_no.setOnClickListener {
                dialog.dismiss()
            }
            view.button_yes.setOnClickListener {
                dialog.dismiss()
                this.finishAffinity()
                exitProcess(0)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == Constants.call_permission) {
            try {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Utils2.call(this)
                }
            } catch (e: ArrayIndexOutOfBoundsException) {
                e.printStackTrace()
            }
        }
    }
}
