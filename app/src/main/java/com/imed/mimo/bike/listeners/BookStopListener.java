package com.imed.mimo.bike.listeners;

public interface BookStopListener {
    void stopBook(String id);
}
