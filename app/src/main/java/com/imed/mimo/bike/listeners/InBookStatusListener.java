package com.imed.mimo.bike.listeners;

public interface InBookStatusListener {
    void inbook(String bikeId, double locx, double locy, int time);
}
