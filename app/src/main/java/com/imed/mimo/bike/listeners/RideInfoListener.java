package com.imed.mimo.bike.listeners;

public interface RideInfoListener {
    void rideInfo(int bookTime, int bookSecs , int time, int seconds, double money, String charged);
}
