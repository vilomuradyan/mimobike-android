package com.imed.mimo.bike.fragments

import android.app.Dialog
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.imed.mimo.bike.R
import com.imed.mimo.bike.activities.MapActivity
import com.imed.mimo.bike.activities.SplashActivity
import com.imed.mimo.bike.activities.SplashActivity.Companion.callback
import com.imed.mimo.bike.activities.SplashActivity.Companion.userId
import com.imed.mimo.bike.adapters.CardAdapter
import com.imed.mimo.bike.connections.Requests
import com.imed.mimo.bike.helpers.Utils
import com.imed.mimo.bike.helpers.Utils2
import com.imed.mimo.bike.listeners.CardsListener
import com.imed.mimo.bike.listeners.EvocaListener
import kotlinx.android.synthetic.main.custom_dialog_yes_no.view.*
import kotlinx.android.synthetic.main.fragment_card.view.*
import me.grantland.widget.AutofitTextView
import java.util.*


class CardFragment : DialogFragment() {

    private lateinit var attachNew: Button
    private lateinit var cards_image: LinearLayout
    private lateinit var pay_amd_button: Button
    private lateinit var passport_attached: AutofitTextView
    private lateinit var dont_have_card_button: Button
    private lateinit var attach_a_new_big: Button
    private lateinit var order_now_layout: RelativeLayout
    private lateinit var progress_bar_included: RelativeLayout
    private lateinit var attention_layout: RelativeLayout
    private lateinit var card_recycler: RecyclerView
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var dialog1: Dialog
    private lateinit var evocaDialog: Dialog
    private var cardIds: ArrayList<String> = ArrayList()
    private var cardMasks: ArrayList<String> = ArrayList()
    private var cardHistory: ArrayList<Boolean> = ArrayList()
    private var cardDate: ArrayList<String> = ArrayList()
    private var date = ""
    private var basePassport = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(
            STYLE_NORMAL,
            android.R.style.Theme_NoTitleBar_Fullscreen
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_card, container, false)
        dialog?.window?.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialog?.window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            dialog?.window?.statusBarColor = ContextCompat.getColor((activity as MapActivity), R.color.colorPrimary)
            dialog?.window?.navigationBarColor =
                ContextCompat.getColor((activity as MapActivity), R.color.colorPrimary)
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            dialog?.window?.decorView?.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR or View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR
        }
        Requests.getCards()
        evocaListener()
        pay_amd_button = view.pay_amd_button
        attachNew = view.attach_a_new
        dont_have_card_button = view.dont_have_card_button
        progress_bar_included = view.progress_bar_included
        attach_a_new_big = view.attach_a_new_big
        passport_attached = view.passport_attached
        cards_image = view.cards_image
        order_now_layout = view.order_now_layout
        attention_layout = view.attention_layout
        card_recycler = view.card_recycler
        view.button_back_card.setOnClickListener {
            this.dismiss()
        }
        getCardsListener()
        attachListener()
        attach_a_new_big.visibility = GONE
        cards_image.visibility = GONE
        attachNew.visibility = VISIBLE
        card_recycler.visibility = VISIBLE
        order_now_layout.visibility = GONE
        view.date_number_event.text =
            (activity as MapActivity).getSharedPreferences("mimo_user_data", Context.MODE_PRIVATE).getString("birthday", "")!!
        view.order_now_button.setOnClickListener {
            //            Utils.showDialog(activity as MapActivity,getString(R.string.dear_user) + "\n" + getString(R.string.service_available_on_november))
            attach_a_new_big.visibility = GONE
            cards_image.visibility = GONE
            order_now_layout.visibility = GONE
            view.evoca_layout.visibility = VISIBLE
            view.textphone.text = userId
            view.select_passp.setOnClickListener {
                Utils2.imageCapturePassport(activity as MapActivity)
            }
            datePickerListener(view.dateLAyout, view.select_button, view.date_number_event)
            view.button_order.setOnClickListener {
                if (view.editText_email.text.toString() != "" &&
                    view.editText_addr.text.toString() != "" && view.editText_scn.text.toString() != "" &&
                    view.date_number_event.text.toString() != "" && passport_attached.text.toString() == getString(R.string.attached)
                ) {
                    attach_a_new_big.visibility = VISIBLE
                    cards_image.visibility = VISIBLE
                    order_now_layout.visibility = VISIBLE
                    view.evoca_layout.visibility = GONE
                    evocaDialog = Utils.showConnectionDialog(activity as MapActivity, getString(R.string.please_Wait))
                    Requests.evoca(view.editText_email.text.toString(),
                        view.editText_addr.text.toString(), view.editText_scn.text.toString(), basePassport, view.date_number_event.text.toString())
                } else {
                    Utils.showDialog(activity as MapActivity, getString(R.string.fill_all_fields))
                }
            }
        }
        return view
    }

    private fun datePickerListener(rel: RelativeLayout, button: Button, textView: TextView) {
        button.setOnClickListener {
            date = Utils2.showDialog(textView, "", activity as MapActivity, true)
        }
        rel.setOnClickListener {
            date = Utils2.showDialog(textView, "", activity as MapActivity, true)
        }
    }

    fun refresh(attached: Boolean) {
        Requests.getCards()
        if (attached) {
            card_recycler.visibility = VISIBLE
            order_now_layout.visibility = VISIBLE
        }
        attachNew.visibility = VISIBLE
        attention_layout.visibility = GONE
    }

    fun passpAttached(basePassp: String) {
        basePassport = basePassp
        passport_attached.text = getString(R.string.attached)
    }

    private fun evocaListener() {
        callback.evocaListener(EvocaListener {
            (activity as MapActivity).runOnUiThread {
                try {
                    evocaDialog.cancel()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if(it == "er") {
                    Utils.showDialog(activity as MapActivity, (activity as MapActivity).getString(R.string.evoca_declined))
                } else {
                    Utils.showDialog(activity as MapActivity, (activity as MapActivity).getString(R.string.evoca_success))
                }
            }
        })
    }

    private fun getCardsListener() {
        callback.cardsListener(CardsListener { json ->
            (activity as MapActivity).runOnUiThread {
                cardIds.clear()
                cardMasks.clear()
                cardHistory.clear()
                card_recycler.visibility = VISIBLE
                if (json.has("active")) {
                    attach_a_new_big.visibility = GONE
                    cards_image.visibility = GONE
                    attachNew.visibility = VISIBLE
                    order_now_layout.visibility = GONE
                    if (json.has("has_debt")) {
                        SplashActivity.cardDebt = json.getBoolean("has_debt")
                    }
                    cardIds.add(json.getJSONObject("active").getString("card_id"))
                    cardMasks.add(json.getJSONObject("active").getString("card_mask"))
                    cardHistory.add(false)
                    cardDate.add(getString(R.string.card_added_on) + " " + json.getJSONObject("active").getString("date"))
                } else {
                    if (json.has("history")) {
                        if (json.getJSONArray("history").length() != 0) {
                            cardIds.add("")
                            cardMasks.add("")
                            cardHistory.add(false)
                            cardDate.add("")
                        } else {
                            attach_a_new_big.visibility = VISIBLE
                            cards_image.visibility = VISIBLE
                            attachNew.visibility = GONE
                            order_now_layout.visibility = VISIBLE
                            progress_bar_included.visibility = GONE
                            card_recycler.visibility = GONE
                        }
                    } else {
                        attach_a_new_big.visibility = VISIBLE
                        cards_image.visibility = VISIBLE
                        attachNew.visibility = GONE
                        order_now_layout.visibility = VISIBLE
                        progress_bar_included.visibility = GONE
                        card_recycler.visibility = GONE
                    }
                }
                if (json.has("history")) {
                    if (json.getJSONArray("history").length() != 0) {
                        val jsonArray = json.getJSONArray("history")
                        for (i in 0 until jsonArray.length()) {
                            cardIds.add(jsonArray.getJSONObject(i).getString("card_id"))
                            cardMasks.add(jsonArray.getJSONObject(i).getString("card_mask"))
                            cardHistory.add(true)
                            cardDate.add(getString(R.string.removed_on) + " " + jsonArray.getJSONObject(i).getString("date"))
                        }
                    }
                } else {
                    if (!json.has("active")) {
                        attach_a_new_big.visibility = VISIBLE
                        cards_image.visibility = VISIBLE
                        attachNew.visibility = GONE
                        order_now_layout.visibility = VISIBLE
                        progress_bar_included.visibility = GONE
                        card_recycler.visibility = GONE
                    }
                }
                if (cardIds.size != 0) {
                    progress_bar_included.visibility = GONE
                }
                viewManager = LinearLayoutManager(context)
                viewAdapter = CardAdapter(
                    activity as MapActivity, cardMasks, cardDate, cardIds, cardHistory
                )
                viewAdapter.notifyDataSetChanged()
                card_recycler.adapter = viewAdapter
                card_recycler.layoutManager = viewManager
                removecard()
            }
        })
    }

    private fun removecard() {
        (viewAdapter as CardAdapter).setOnItemClickListener { position, _ ->
            try {
                val adb = android.app.AlertDialog.Builder(
                    activity as MapActivity,
                    R.style.AlertDialogCustom
                )
                val view =
                    layoutInflater.inflate(R.layout.custom_dialog_yes_no, null) as RelativeLayout
                adb.setView(view)
                val dialog = adb.create()
                dialog.show()
                val tvCount = view.findViewById<TextView>(R.id.dialogText)
                tvCount.text = getString(R.string.are_you_sure_delete)
                view.button_no.setOnClickListener {
                    dialog.dismiss()
                }
                view.button_yes.setOnClickListener {
                    dialog.dismiss()
                    Requests.removeCard(cardIds[position])
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

//    override fun onResume() {
//        super.onResume()
//        Requests.getCards()
//    }

    private fun attachListener() {
        attachNew.setOnClickListener {
            attach_a_new_big.visibility = VISIBLE
            cards_image.visibility = VISIBLE
            attachNew.visibility = GONE
            order_now_layout.visibility = VISIBLE
            progress_bar_included.visibility = GONE
            card_recycler.visibility = GONE
        }
        attach_a_new_big.setOnClickListener {
            card_recycler.visibility = GONE
            attachNew.visibility = GONE
            order_now_layout.visibility = GONE
            attention_layout.visibility = VISIBLE
            attach_a_new_big.visibility = GONE
            cards_image.visibility = GONE
        }
        pay_amd_button.setOnClickListener {
            dialog1 = Utils.showConnectionDialog(
                activity as MapActivity,
                (activity as MapActivity).getString(R.string.please_Wait)
            )
           // Requests.attachCard()
            Requests.evocaBinding()
        }
    }

    fun cancelDialog() {
        try {
            dialog1.cancel()
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }
}