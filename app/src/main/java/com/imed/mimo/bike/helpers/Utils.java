package com.imed.mimo.bike.helpers;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.core.app.ActivityCompat;
import androidx.exifinterface.media.ExifInterface;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.maps.android.clustering.Cluster;
import com.imed.mimo.bike.R;
import com.imed.mimo.bike.listeners.ScanButtonCallback;
import org.json.JSONArray;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import static android.content.Context.WINDOW_SERVICE;
import static com.imed.mimo.bike.helpers.Constants.sharedRequestCode;

public class Utils {

    public static void showDialog(Activity context, String text) {
        try {
            final AlertDialog.Builder adb = new AlertDialog.Builder(context, R.style.AlertDialogCustom);
            @SuppressLint("InflateParams") RelativeLayout view = (RelativeLayout) context.getLayoutInflater()
                    .inflate(R.layout.custom_dialog, null);
            adb.setView(view);
            final Dialog dialog = adb.create();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.show();
            TextView tvCount = view.findViewById(R.id.dialogText);
            tvCount.setText(text);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        dialog.dismiss();
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }, 2000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showAlarmDialog(Activity context, String text) {
        try {
            final AlertDialog.Builder adb = new AlertDialog.Builder(context, R.style.AlertDialogCustom);
            @SuppressLint("InflateParams") RelativeLayout view = (RelativeLayout) context.getLayoutInflater()
                    .inflate(R.layout.custom_dialog, null);
            adb.setView(view);
            final Dialog dialog = adb.create();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.show();
            TextView tvCount = view.findViewById(R.id.dialogText);
            tvCount.setText(text);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    dialog.dismiss();
                }
            }, 4000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static Dialog showConnectionDialog(Activity context, String text) {
        try{
            final AlertDialog.Builder adb = new AlertDialog.Builder(context, R.style.AlertDialogCustom);
            @SuppressLint("InflateParams") RelativeLayout view = (RelativeLayout) context.getLayoutInflater()
                    .inflate(R.layout.custom_dialog, null);
            adb.setView(view);
            final Dialog dialog = adb.create();
//            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setCancelable(false);
            dialog.show();
            TextView tvCount = view.findViewById(R.id.dialogText);
            tvCount.setText(text);
            return dialog;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static boolean isMatch(byte[] pattern, byte[] input, int pos) {
        for (int i = 0; i < pattern.length; i++) {
            if (pattern[i] != input[pos + i]) {
                return false;
            }
        }
        return true;
    }

    @SuppressLint("DefaultLocale")
    public static String secondsToString(int pTime) {
        return String.format("%02d:%02d", pTime / 60, pTime % 60);
    }

    public static List<byte[]> split(byte[] pattern, byte[] input) {
        List<byte[]> l = new LinkedList<>();
        int blockStart = 0;
        for (int i = 0; i < input.length; i++) {
            if (isMatch(pattern, input, i)) {
                l.add(Arrays.copyOfRange(input, blockStart, i));
                blockStart = i + pattern.length;
                i = blockStart;
            }
        }
        l.add(Arrays.copyOfRange(input, blockStart, input.length));
        return l;
    }

    public static int dpToPx(int dp, Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    private static ScanButtonCallback scanButtonCallback;
    public static void scanButtonCallback(ScanButtonCallback callback){
        scanButtonCallback = callback;
    }

    public static Bitmap createBitmapFromLayout(View view) {
        view.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        view.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        final Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        final Canvas c = new Canvas(bitmap);
        view.draw(c);
        return bitmap;
    }

    public static void createBitmapFromLayout(View view,Activity context, int permNumber) {
        view.setDrawingCacheEnabled(true);
        view.setDrawingCacheBackgroundColor(0xfffafafa);
        if(view.getDrawingCache() == null) {
            int viewWidth = view.getMeasuredWidth();
            int viewHeight = view.getMeasuredHeight();
            Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth() , view.getMeasuredHeight() , Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(bitmap);
            view.layout(dpToPx(context, 10f), 0, view.getMeasuredWidth() + viewWidth, view.getMeasuredHeight() + viewHeight);
            view.draw(c);
            if(Utils.isPermissionGranted(android.Manifest.permission.WRITE_EXTERNAL_STORAGE,permNumber,context)) {
                shareBitmap( bitmap, context);
            }
            return;
        }
        Bitmap bitmap = Bitmap.createBitmap(view.getDrawingCache());
        view.setDrawingCacheEnabled(false);
        if(Utils.isPermissionGranted(
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                permNumber,
                context
        )) {
            shareBitmap(bitmap,context);
        }
    }

    private static int dpToPx(Context context, Float float1) {
        return (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, float1,
                context.getResources().getDisplayMetrics());
    }

    private static void shareBitmap(Bitmap bitmap, Activity context){
        try {
            File imageFile = getTemporalFile();
            FileOutputStream outputStream = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            outputStream.flush();
            outputStream.close();
            Intent intent = new Intent();
            Uri uri = Uri.fromFile(imageFile);
            intent.setAction(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_STREAM, uri);
            intent.setType("image/*");
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            context.startActivityForResult(Intent.createChooser(intent, "Share image"),sharedRequestCode);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Bitmap save(View v)
    {
        Bitmap b = Bitmap.createBitmap(v.getWidth(), v.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.setDrawingCacheEnabled(true);
        v.draw(c);
        return b;
    }

    public static void animateMarker(final Marker marker, final LatLng toPosition, GoogleMap mGoogleMapObject) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = mGoogleMapObject.getProjection();
        Point startPoint = proj.toScreenLocation(marker.getPosition());
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 500;
        final Interpolator interpolator = new LinearInterpolator();
        handler.post(new Runnable() {
            @Override
            public void run() {
                try {
                    long elapsed = SystemClock.uptimeMillis() - start;
                    float t = interpolator.getInterpolation((float) elapsed
                            / duration);
                    double lng = t * toPosition.longitude + (1 - t)
                            * startLatLng.longitude;
                    double lat = t * toPosition.latitude + (1 - t)
                            * startLatLng.latitude;
                    marker.setPosition(new LatLng(lat, lng));
                    if (t < 1.0) {
                        handler.postDelayed(this, 16);
                    }
                } catch (NullPointerException e){
                    e.printStackTrace();
                }
            }
        });
    }

    public static String createFileFromBitmap(Bitmap bitmap){
        try {
            File file = new File(Environment.getExternalStorageDirectory() + "/DCIM/MimoBike/");
            if (!file.exists()) {
                file.mkdirs();
            }
            int rand = (int) (Math.random() * 1000000 + 1000000);
            String name = String.valueOf(rand) + System.currentTimeMillis();
            String avatar = "AVT-" + name + ".jpg";
            File file1 =  new File(Environment.getExternalStorageDirectory() + "/DCIM/MimoBike/" + avatar);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 50, bos);
            byte[] bitmapdata = bos.toByteArray();
            FileOutputStream fos = new FileOutputStream(file1);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
            return file1.getAbsolutePath();
        } catch (Exception e){
            e.printStackTrace();
            return "";
        }
    }

    public static void deleteFile(){
        File dir = new File(Environment.getExternalStorageDirectory() + "/DCIM/MimoBike");
        if (dir.isDirectory())  {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++)
            {
                new File(dir, children[i]).delete();
            }
            dir.delete();
        }
    }

    public static File getTemporalFile() {
        File file = new File(Environment.getExternalStorageDirectory() + "/DCIM/MimoBike/");
        if (!file.exists()) {
            file.mkdirs();
        }
        String mPath = Environment.getExternalStorageDirectory() + "/DCIM/MimoBike/" + "mimobike" + "1" + ".jpg";
        return new File(mPath);
    }

    public static boolean isPermissionGranted(String permission, int permissionNumber, Activity activity1) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (activity1.checkSelfPermission(permission)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(activity1, new String[]{permission}, permissionNumber);
                return false;
            }
        } else {
            return true;
        }
    }

    public static void unpairDevice(BluetoothDevice device) {
        try {
            Method method = device.getClass().getMethod("removeBond", (Class[]) null);
            method.invoke(device, (Object[]) null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void adjustFontScale(Configuration configuration, Context context) {
        if (configuration.fontScale > 1.30) {
            configuration.fontScale = (float) 1.30;
            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            WindowManager wm = (WindowManager) context.getSystemService(WINDOW_SERVICE);
            wm.getDefaultDisplay().getMetrics(metrics);
            metrics.scaledDensity = configuration.fontScale * metrics.density;
            context.getResources().updateConfiguration(configuration, metrics);
        }
    }

    public static int getRotation(Context context, Uri imageUri, boolean fromCamera) {
        int rotation;
        if (fromCamera) {
            rotation = getRotationFromCamera(context, imageUri);
        } else {
            rotation = getRotationFromGallery(context, imageUri);
        }
        Log.i("rotationfzsdf", "Image rotation: " + rotation);
        return rotation;
    }

    private static int getRotationFromCamera(Context context, Uri imageFile) {
        int rotate = 0;
        try {
            context.getContentResolver().notifyChange(imageFile, null);
            ExifInterface exif = new ExifInterface(Objects.requireNonNull(imageFile.getPath()));
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
                default:
                    rotate = 0;
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rotate;
    }

    public static Bitmap decodeBitmap(Context context, Uri theUri) {
        Bitmap outputBitmap = null;
        AssetFileDescriptor fileDescriptor;
        try {
            fileDescriptor = context.getContentResolver().openAssetFileDescriptor(theUri, "r");
            BitmapFactory.Options boundsOptions = new BitmapFactory.Options();
            boundsOptions.inJustDecodeBounds = true;
            if (fileDescriptor != null) {
                BitmapFactory.decodeFileDescriptor(fileDescriptor.getFileDescriptor(), null, boundsOptions);
            }
            int[] sampleSizes = new int[]{8, 4, 2, 1};
            int selectedSampleSize = 1;
            for (int sampleSize : sampleSizes) {
                selectedSampleSize = sampleSize;
                int targetWidth = boundsOptions.outWidth / sampleSize;
                int targetHeight = boundsOptions.outHeight / sampleSize;
                int minQuality = 400;
                if (targetWidth >= minQuality && targetHeight >= minQuality) {
                    break;
                }
            }
            BitmapFactory.Options decodeOptions = new BitmapFactory.Options();
            decodeOptions.inSampleSize = selectedSampleSize;
            if (fileDescriptor != null) {
                outputBitmap = BitmapFactory.decodeFileDescriptor(fileDescriptor.getFileDescriptor(),
                        null, decodeOptions);
            }
            if (fileDescriptor != null) {
                fileDescriptor.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return outputBitmap;
    }

    public static int getStatusBarHeight(Activity context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static void animateZoomCluster(GoogleMap map, Cluster<Bike> cluster){
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(
                cluster.getPosition(), (float) Math.floor(map
                        .getCameraPosition().zoom + 1)), 300,
                null);
    }

    public static Bitmap rotate(Bitmap bitmap, float degrees) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    private static int getRotationFromGallery(Context context, Uri imageUri) {
        int result = 0;
        String[] columns = {MediaStore.Images.Media.ORIENTATION};
        try (Cursor cursor = context.getContentResolver().query(imageUri, columns, null, null, null)) {
            if (cursor != null && cursor.moveToFirst()) {
                int orientationColumnIndex = cursor.getColumnIndex(columns[0]);
                result = cursor.getInt(orientationColumnIndex);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Bitmap cropImage(Bitmap srcBmp) {
        if (srcBmp.getWidth() >= srcBmp.getHeight()) {
            return Bitmap.createBitmap(
                    srcBmp,
                    srcBmp.getWidth() / 2 - srcBmp.getHeight() / 2,
                    0,
                    srcBmp.getHeight(),
                    srcBmp.getHeight()
            );
        } else {
            return Bitmap.createBitmap(
                    srcBmp,
                    0,
                    srcBmp.getHeight() / 2 - srcBmp.getWidth() / 2,
                    srcBmp.getWidth(),
                    srcBmp.getWidth()
            );
        }
    }

    public static void openKeyboard(Context context) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    public static Bitmap jsonarrayToBitmap(JSONArray arr) {
        try {
            byte[] tmp = new byte[arr.length()];
            for (int i = 0; i < arr.length(); i++) {
                tmp[i] = (byte) (((int) arr.get(i)));
            }
            tmp =  Base64.decode(tmp, 0);
            return BitmapFactory.decodeByteArray(tmp, 0, tmp.length);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}