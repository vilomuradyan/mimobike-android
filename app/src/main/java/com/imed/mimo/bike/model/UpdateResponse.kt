package com.imed.mimo.bike.model
import com.google.gson.annotations.SerializedName


data class UpdateResponse (
	@SerializedName("id") val id : String,
	@SerializedName("version") val version : Double,
	@SerializedName("updatedDate") val updatedDate : String,
	@SerializedName("osType") val osType : String
)