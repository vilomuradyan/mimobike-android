package com.imed.mimo.bike.fragments


import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import com.imed.mimo.bike.R
import com.imed.mimo.bike.activities.MapActivity
import com.imed.mimo.bike.activities.SplashActivity.Companion.bonus
import com.imed.mimo.bike.activities.SplashActivity.Companion.minimal
import com.imed.mimo.bike.activities.SplashActivity.Companion.ridePrice
import com.imed.mimo.bike.activities.SplashActivity.Companion.studentPrice
import com.imed.mimo.bike.activities.SplashActivity.Companion.weekendPrice
import kotlinx.android.synthetic.main.fragment_prices.view.*

class PricesFragment : AppCompatDialogFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_NoTitleBar_Fullscreen)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_prices, container, false)
        dialog?.window?.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialog?.window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            dialog?.window?.statusBarColor = ContextCompat.getColor((activity as MapActivity),R.color.colorPrimary)
            dialog?.window?.navigationBarColor =
                ContextCompat.getColor((activity as MapActivity), R.color.colorPrimary)
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            dialog?.window?.decorView?.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR or View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR
        }
        view.button_close_prices.setOnClickListener {
            this.dismiss()
        }
        view.basic_number_text.text = ridePrice.toString()
        view.stud_number_text.text = studentPrice.toString()
        view.weekend_number_text.text = weekendPrice.toString()
        view.textView_bonus_number.text = "+" + bonus.toString()
        view.min_fee_basic.text = getString(R.string.unlock_fee_is) + " " + minimal.toString() + " " + getString(R.string.amd)
        view.min_fee_stud.text = getString(R.string.unlock_fee_is) + " " + minimal.toString() + " " + getString(R.string.amd)
        view.min_fee_weekend.text = getString(R.string.unlock_fee_is) + " " + minimal.toString() + " " + getString(R.string.amd)
//        view.pause_textview_number.text = holdPrice.toString()
        return view
    }


}
