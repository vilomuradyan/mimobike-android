package com.imed.mimo.bike.listeners;

public interface ClusteredListener {
    void onClustered();
}
