package com.imed.mimo.bike.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.imed.mimo.bike.R
import com.imed.mimo.bike.connections.Requests
import kotlinx.android.synthetic.main.custom_layout_manually.view.*

class EnterManuallyFragment : BottomSheetDialogFragment() {

    var mLocationX = 0.0
    var mLocationY = 0.0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.custom_layout_manually, container, false)
        arguments?.let {
            mLocationX = it.getDouble("mLocationX")
            mLocationY = it.getDouble("mLocationY")
        }
        view.continue_button_manually.setOnClickListener {
            try {
                Requests.validate(view.lock_code_manually.text.toString(), mLocationX, mLocationY)
            } catch ( e: ClassCastException) {
                e.printStackTrace()
            }
            dismiss()
            fragmentManager!!.executePendingTransactions()
        }
        dialog!!.window!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        return view
    }

    companion object {
        fun newInstance(mLocationX: Double, mLocationY: Double): EnterManuallyFragment {
            val args = Bundle()
            args.putDouble("mLocationX", mLocationX)
            args.putDouble("mLocationY", mLocationY)
            val fragment = EnterManuallyFragment()
            fragment.arguments = args
            return fragment
        }
    }

}
