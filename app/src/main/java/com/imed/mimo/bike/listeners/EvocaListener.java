package com.imed.mimo.bike.listeners;

public interface EvocaListener {
    void evocaAttached(String status);
}
