package com.imed.mimo.bike.listeners;

public interface BikesListener {
    void bikes(String jsonString);
}
