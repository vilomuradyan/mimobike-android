package com.imed.mimo.bike.listeners;

import org.json.JSONArray;

public interface TripsListener {
    void trips(JSONArray json);
}
