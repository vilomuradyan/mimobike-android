package com.imed.mimo.bike.listeners;

public interface QrPermittedListener {
    void permitted(boolean permitted, String mac, String description, String blekey);
}
