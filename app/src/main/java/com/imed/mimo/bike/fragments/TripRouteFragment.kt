package com.imed.mimo.bike.fragments

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.os.StrictMode
import android.view.LayoutInflater
import android.view.View
import android.view.View.*
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.model.*
import com.imed.mimo.bike.R
import com.imed.mimo.bike.activities.MapActivity
import com.imed.mimo.bike.helpers.Constants
import com.imed.mimo.bike.helpers.Utils
import com.imed.mimo.bike.helpers.Utils2
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.fragment_trip_route.view.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject


class TripRouteFragment : DialogFragment() {

    lateinit var mMapView: MapView
    lateinit var map: GoogleMap
    lateinit var button_close_trip_route: Button
    lateinit var google_play_icon: ImageView
    lateinit var app_store_icon: ImageView
    lateinit var share_my_trip_button: Button
    lateinit var layout_to_share: RelativeLayout
    lateinit var map_image: ImageView
    private var mins: String? = null
    private var kms: String? = null
    private var cals: String? = null
    private var json: String? = null
    private var lastLatLng: LatLng? = null
    var polyLines = ArrayList<Polyline>()
    private var mLatLngList = ArrayList<LatLng>()

    @SuppressLint("MissingPermission")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_trip_route, container, false)
        dialog?.window?.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialog?.window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            dialog?.window?.statusBarColor = ContextCompat.getColor((activity as MapActivity),R.color.colorPrimary)
            dialog?.window?.navigationBarColor =
                ContextCompat.getColor((activity as MapActivity), R.color.colorPrimary)
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            dialog?.window?.decorView?.systemUiVisibility = SYSTEM_UI_FLAG_LIGHT_STATUS_BAR or SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR
        }
        arguments?.let {
            mins = it.getString("mins")
            kms = it.getString("kms")
            cals = it.getString("calories")
            json = it.getString("json")
        }
        button_close_trip_route = view.button_close_trip_route
        google_play_icon = view.google_play_icon
        layout_to_share = view.layout_to_share
        app_store_icon = view.app_store_icon
        map_image = view.map_image
        share_my_trip_button = view.share_my_trip_button
        view.mins_trip_route.text = mins
        view.kms_trip_route.text = kms
        view.cals_trip_route.text = cals
        view.button_close_trip_route.setOnClickListener {
            dismiss()
        }
        try {
            mMapView = view.findViewById(R.id.user_mapView)
            mMapView.postDelayed({
                val width = mMapView.width
                val lp = mMapView.layoutParams as RelativeLayout.LayoutParams
                lp.width = width
                lp.height = width
                mMapView.layoutParams = lp
            }, 1)
            mMapView.onCreate(savedInstanceState)
            mMapView.onResume()
            MapsInitializer.initialize(activity!!.applicationContext)

        } catch (e: Exception) {
            e.printStackTrace()
        }
        mMapView.getMapAsync { mMap ->
            map = mMap
            mMap.uiSettings.isCompassEnabled = true
            mMap.isBuildingsEnabled = true
            mMap.uiSettings.isMapToolbarEnabled = false
            map.setPadding(0, 0, 0, Utils.dpToPx(60,activity as MapActivity))
            try {
                rideRoutes(JSONObject(json).getJSONArray("trip"))
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
        return view
    }

    companion object {
        fun newInstance(mins: String, kms: String, cals : String, json: String): TripRouteFragment {
            val args = Bundle()
            args.putString("mins", mins)
            args.putString("kms", kms)
            args.putString("calories", cals)
            args.putString("json", json)
            val fragment = TripRouteFragment()
            fragment.arguments = args
            return fragment
        }
    }

    private fun rideRoutes(routes: JSONArray) {
        lastLatLng = LatLng(routes.getJSONArray(0).getDouble(0), routes.getJSONArray(0).getDouble(1))
        val view1 = LayoutInflater.from(activity as MapActivity).inflate(R.layout.flag_marker_layout, null, false)
        val markerViewFlag = view1.findViewById<RelativeLayout>(R.id.markerView_flag)
        mLatLngList.add(lastLatLng!!)
        map.addMarker(
            MarkerOptions().position(lastLatLng!!).icon(
                BitmapDescriptorFactory.fromBitmap(Utils.createBitmapFromLayout(markerViewFlag))))
        for (i in 0 until routes.length()) {
            try {
                val ltlng = LatLng(routes.getJSONArray(i).getDouble(0), routes.getJSONArray(i).getDouble(1))
                try {
                    mLatLngList.add(ltlng)
                    polyLines.add(map.addPolyline(PolylineOptions().add(lastLatLng, ltlng).width(10f).color(
                                ContextCompat.getColor(activity as MapActivity, R.color.colorPrimaryRoutes))))
                    lastLatLng = ltlng
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            } catch (e: UninitializedPropertyAccessException) {
                e.printStackTrace()
            }
        }
        val view2 = LayoutInflater.from(activity as MapActivity).inflate(R.layout.marker_view, null, false)
        val markerView = view2.findViewById<RelativeLayout>(R.id.markerView)
        val markerImage = view2.findViewById<CircleImageView>(R.id.imageViewMarker)
        val markerLayout = view2.findViewById<RelativeLayout>(R.id.markerLayout)
        val markerShadow = view2.findViewById<RelativeLayout>(R.id.shadow_pin)
        val markerOptions = MarkerOptions().position(mLatLngList[routes.length()])
        markerLayout.backgroundTintList =
            ContextCompat.getColorStateList(activity as MapActivity, android.R.color.white)
        markerImage.backgroundTintList = ContextCompat.getColorStateList(activity as MapActivity, com.imed.mimo.bike.R.color.colorBlack)
        markerShadow.backgroundTintList =
            ContextCompat.getColorStateList(activity as MapActivity, R.color.colorBlack)
        val newBitMap = Bitmap.createScaledBitmap(
            Utils.createBitmapFromLayout(markerView),
            (markerView.width * 1f).toInt(),
            (markerView.height * 1f).toInt(),
            true
        )
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(newBitMap))
        map.addMarker(markerOptions)
        val polyline = map.addPolyline(PolylineOptions().add(mLatLngList[0], mLatLngList[routes.length()- 1]).width(10f).color(
            ContextCompat.getColor(activity as MapActivity, android.R.color.transparent)))
        map.setOnMapLoadedCallback {
            Utils2.moveToBounds(polyline, map)
            shareTrip()
        }
    }

    private fun shareTrip(){
        share_my_trip_button.isEnabled = true
        share_my_trip_button.isClickable = true
        share_my_trip_button.setOnClickListener {
            if (Utils.isPermissionGranted(
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Constants.storage_permission_route,
                    activity as MapActivity
                )
            ) {
                map_image.visibility = VISIBLE
                button_close_trip_route.visibility = GONE
                google_play_icon.visibility = VISIBLE
                app_store_icon.visibility = VISIBLE
                mMapView.visibility = INVISIBLE
                (map.snapshot {
                    map_image.setImageBitmap(it)
                    Utils.createBitmapFromLayout(layout_to_share, activity as MapActivity, Constants.storage_permission_route)
                })
            }
        }
    }

    fun share(){
        map_image.visibility = VISIBLE
        button_close_trip_route.visibility = GONE
        google_play_icon.visibility = VISIBLE
        app_store_icon.visibility = VISIBLE
        mMapView.visibility = INVISIBLE
        (map.snapshot {
            map_image.setImageBitmap(it)
            Utils.createBitmapFromLayout(layout_to_share, activity as MapActivity, Constants.storage_permission_route)
        })
    }

    fun reverseLayouts(){
        button_close_trip_route.visibility = VISIBLE
        google_play_icon.visibility = GONE
        app_store_icon.visibility = GONE
        mMapView.visibility = VISIBLE
        map_image.visibility = GONE
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen)
        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
    }
}
