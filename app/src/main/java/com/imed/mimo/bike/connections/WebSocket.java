package com.imed.mimo.bike.connections;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.util.Base64;

import com.google.android.gms.maps.model.LatLng;
import com.imed.mimo.bike.R;
import com.imed.mimo.bike.activities.JoinActivity;
import com.imed.mimo.bike.activities.MapActivity;
import com.imed.mimo.bike.activities.SplashActivity;
import com.imed.mimo.bike.activities.UpdateActivity;
import com.imed.mimo.bike.helpers.Utils;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.framing.Framedata;
import org.java_websocket.handshake.ServerHandshake;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import kotlin.UninitializedPropertyAccessException;

import static com.imed.mimo.bike.activities.SplashActivity.activeOrNo;
import static com.imed.mimo.bike.activities.SplashActivity.bikeId;
import static com.imed.mimo.bike.activities.SplashActivity.bonus;
import static com.imed.mimo.bike.activities.SplashActivity.bookPrice;
import static com.imed.mimo.bike.activities.SplashActivity.bookRequested;
import static com.imed.mimo.bike.activities.SplashActivity.bookedBikeId;
import static com.imed.mimo.bike.activities.SplashActivity.bookedBikeLat;
import static com.imed.mimo.bike.activities.SplashActivity.bookedBikeLong;
import static com.imed.mimo.bike.activities.SplashActivity.calRate;
import static com.imed.mimo.bike.activities.SplashActivity.callback;
import static com.imed.mimo.bike.activities.SplashActivity.calories;
import static com.imed.mimo.bike.activities.SplashActivity.debt;
import static com.imed.mimo.bike.activities.SplashActivity.distance_km;
import static com.imed.mimo.bike.activities.SplashActivity.freeMinPrice;
import static com.imed.mimo.bike.activities.SplashActivity.freeMinutes;
import static com.imed.mimo.bike.activities.SplashActivity.holdPrice;
import static com.imed.mimo.bike.activities.SplashActivity.inBook;
import static com.imed.mimo.bike.activities.SplashActivity.inRide;
import static com.imed.mimo.bike.activities.SplashActivity.isInSplash;
import static com.imed.mimo.bike.activities.SplashActivity.isUpdate;
import static com.imed.mimo.bike.activities.SplashActivity.minimal;
import static com.imed.mimo.bike.activities.SplashActivity.mode;
import static com.imed.mimo.bike.activities.SplashActivity.paired;
import static com.imed.mimo.bike.activities.SplashActivity.profileOk;
import static com.imed.mimo.bike.activities.SplashActivity.rideBikeId;
import static com.imed.mimo.bike.activities.SplashActivity.ridePrice;
import static com.imed.mimo.bike.activities.SplashActivity.studentPrice;
import static com.imed.mimo.bike.activities.SplashActivity.support;
import static com.imed.mimo.bike.activities.SplashActivity.treeRate;
import static com.imed.mimo.bike.activities.SplashActivity.trees_saved;
import static com.imed.mimo.bike.activities.SplashActivity.userId;
import static com.imed.mimo.bike.activities.SplashActivity.weekendPrice;
import static com.imed.mimo.bike.helpers.Constants.ATOKEN;
import static com.imed.mimo.bike.helpers.Constants.BASE_HOST1;
import static com.imed.mimo.bike.helpers.Constants.cannotConnect;
import static com.imed.mimo.bike.helpers.Constants.internetLost;
import static com.imed.mimo.bike.helpers.Constants.mWebSocketClient;
import static com.imed.mimo.bike.helpers.Constants.reconnect;
import static com.imed.mimo.bike.helpers.Constants.sessionKey;


public class WebSocket {

    private Dialog cantconnect = null;
    private Activity mContext;

    public void connectWebSocket(final Activity context) {
        this.mContext = context;
        URI uri;
        try {
            uri = new URI(BASE_HOST1);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return;
        }
        Map<String, String> headers = new HashMap<>();
        headers.put("TOK", ATOKEN);
        mWebSocketClient = new WebSocketClient(uri, headers) {
            @Override
            public void onOpen(ServerHandshake serverHandshake) {
                openThings(mContext);
            }

            @Override
            public void onWebsocketPong(org.java_websocket.WebSocket conn, Framedata f) {
                super.onWebsocketPong(conn, f);
            }

            @Override
            public void onWebsocketPing(org.java_websocket.WebSocket conn, Framedata f) {
                super.onWebsocketPing(conn, f);
            }

            @Override
            public void onMessage(String s) {
                response(s);
            }

            @Override
            public void onMessage(ByteBuffer bytes) {
                responseBytes(bytes, mContext);
            }

            @Override
            public void onClose(int i, String s, boolean b) {
                reconnect = true;
//                if (!internetLost)
                mContext.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (!cannotConnect && !internetLost)
                            cantconnect = Utils.showConnectionDialog(mContext, mContext.getString(R.string.cant_connect));
                        cannotConnect = true;
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                connectWebSocket(context);
                            }
                        }, 3000);
                    }
                });
            }

            @Override
            public void onError(Exception e) {
            }
        };
        mWebSocketClient.connect();
    }

    public void changeContext(Activity context) {
        this.mContext = context;
    }

    private void openThings(final Activity context) {
        cannotConnect = false;
        try {
            cantconnect.dismiss();
        } catch (Exception ignored) {
        }
        paired = context.getSharedPreferences("mimo_user_data", Context.MODE_PRIVATE).getBoolean("paired", false);
        userId = context.getSharedPreferences("mimo_user_data", Context.MODE_PRIVATE).getString("phone", "");
        if (!userId.equals("") && paired) {
            Requests.auth(context, 0.0, 0.0);
        }
        reconnect = false;
        if (isInSplash) {
            if (!isUpdate){
                if (!userId.equals("") && paired) {
                    mContext.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    context.startActivity(new Intent(context, MapActivity.class));
                                    context.finish();
                                }
                            }, 4000);
                        }
                    });
                } else {
                    mContext.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    context.startActivity(new Intent(context, JoinActivity.class));
                                    context.finish();
                                }
                            }, 4000);
                        }
                    });
                }
            }else {
                mContext.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                context.startActivity(new Intent(context, UpdateActivity.class));
                                context.finish();
                            }
                        }, 4000);
                    }
                });
            }
        }
        if (!reconnect) {
            Requests.regPush(context);
        }
    }

    private void response(String s) {
        try {
            JSONObject json = new JSONObject(s);
            if (json.getString("action").equals("auth")) {
                if (json.getString("status").equals("ok")) {
                    sessionKey = json.getString("sk");
                    Requests.getProfile();
                    Requests.free();
                } else {
                    SharedPreferences.Editor editor = mContext.getSharedPreferences("mimo_user_data", Context.MODE_PRIVATE).edit();
                    editor.putString("phone", "")
                            .putBoolean("paired", false)
                            .putBoolean("paired", false)
                            .putString("name", "")
                            .putString("surname", "")
                            .putString("birthday", "")
                            .putString("sex", "")
                            .putString("email", "")
                            .putString("imagedata", "")
                            .putString("avatar", "").apply();
                    debt = false;
                    paired = false;
                    userId = "";
                    bookRequested = false;
                    bookedBikeId = "";
                    freeMinutes = 0;
                    rideBikeId = "";
                    bikeId = "";
                    inBook = false;
                    inRide = false;
                    activeOrNo = false;
                    profileOk = false;
//                    mContext.startActivity(new Intent(mContext, JoinActivity.class));
                }
            } else if (json.getString("action").equals("enter")) {
                enterResponse(json, "");
            } else if (json.getString("action").equals("bikes") && json.getString("status").equals("ok")) {
                callback.bikesListener.bikes(json.toString());
            } else if (json.getString("action").equals("validate")) {
                enterResponse(json, "");
            } else if (json.getString("action").equals("get-profile")) {
                enterResponse(json, "get-profile");
                if (json.has("student")) {
                    if (json.getBoolean("student")) {
                        mode = "student";
                        callback.getFreeListener.getFree(true);
                    }
                }
            } else if (json.getString("action").equals("set-profile")) {
                if (json.getString("status").equals("ok")) {
                    Requests.getProfile();
                } else {
                    mContext.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Utils.showDialog(mContext, mContext.getString(R.string.unable_to_update));
                        }
                    });
                }
            } else if (json.getString("action").equals("state")) {
                stateResponse(json);
            } else if (json.getString("action").equals("lock-response")) {
                if (json.getString("status").equals("error")) {
                    mContext.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Utils.showDialog(mContext, mContext.getString(R.string.bike_serviced));
                        }
                    });
                }
            } else if (json.getString("action").equals("free-minutes")) {
                if (json.getString("status").equals("ok")) {
                    distance_km = ((double) json.getInt("distance") / 1000);
                    calories = distance_km * calRate;
                    trees_saved = (int) (distance_km / treeRate);
                    SplashActivity.debt = json.getBoolean("debt");
                    freeMinutes = json.getInt("minutes");
                    callback.getFreeListener.getFree(false);
                }
            } else if (json.getString("action").equals("get-trips")) {
                callback.tripsListener.trips(json.getJSONArray("rides"));
            } else if (json.getString("action").equals("attach-card")) {
                if (json.getString("status").equals("ok")) {
                    Requests.getProfile();
                    callback.ameriaLinkListener.ameriaLink(json.getString("link"));
                } else {
                    callback.ameriaLinkListener.ameriaLink("error");
                }
            } else if (json.getString("action").equals("m-pay")) {
                if (json.getString("status").equals("ok")) {
                    callback.payDeptListener.ameriaLink(
                            json.getString("link"),
                            ""
                    );
                } else {
                    callback.payDeptListener.ameriaLink("error", "");
                }
            } else if (json.getString("action").equals("get-cards")) {
                if (json.getString("status").equals("ok")) {
                    callback.cardsListener.cards(json);
                }
            } else if (json.getString("action").equals("remove-card")) {
                if (json.getString("status").equals("ok")) {
                    Requests.getCards();
                    Requests.getProfile();
                }
            } else if (json.getString("action").equals("evoca-order")) {
                callback.evocaListener.evocaAttached(json.getString("status"));
            } else if (json.getString("action").equals("notify-defaults")) {
                if (json.getString("status").equals("ok")) {
                    defaultsResponse(json);
                }
            } else if (json.getString("action").equals("cancel-book")) {
                if (json.getString("status").equals("ok")) {
                    callback.bookStopListener.stopBook("");
                } else {
                    callback.bookStopListener.stopBook("");
                }
            } else if (json.getString("action").equals("book")) {
                bookResponse(json);
            } else if (json.getString("action").equals("validate-qr")) {
                qrResponse(json);
            } else if (json.getString("action").equals("solve-ride")) {
                callback.solveListener.solve(json.getString("status"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void stateResponse(JSONObject json) {
        try {
            if (json.getInt("mode") == 1) {
                SplashActivity.inBook = true;
                SplashActivity.bookedBikeId = json.getString("bike_id");
                SplashActivity.bookedBikeLat = json.getDouble("bike_x");
                SplashActivity.bookedBikeLong = json.getDouble("bike_y");
                SplashActivity.bookedBikeTime = 5 - json.getInt("duration");
                callback.bookRideStatus.bookride("book", false, 0);
            } else if (json.getInt("mode") == 0) {
                callback.bookRideStatus.bookride("none", false, 0);
            } else {
                SplashActivity.inRide = true;
                SplashActivity.rideBikeId = json.getString("bike_id");
                SplashActivity.bikeId = json.getString("bike_qr");
                SplashActivity.rideBikeLat = json.getDouble("bike_x");
                SplashActivity.rideBikeLong = json.getDouble("bike_y");
                SplashActivity.inRideStatus = true;
                int duration = 0;
                if (json.has("duration")) {
                    duration = json.getInt("duration");
                }
                callback.bookRideStatus.bookride("ride", json.getBoolean("bike_closed"), duration);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void qrResponse(JSONObject json) {
        try {
            if (json.getString("status").equals("ok")) {
                callback.qrPermittedListener.permitted(
                        true,
//                                json.getString("bikeId"),
                        json.getString("mac"),
                        "",
                        json.getString("ble"));

            } else {
                callback.qrPermittedListener.permitted(
                        false, "",
                        String.valueOf(json.getInt("error")),
                        ""
                );
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void bookResponse(JSONObject json) {
        try {
            if (json.getString("status").equals("ok")) {
                inBook = true;
                bookedBikeId = json.getString("bike_id");
                bookedBikeLat = json.getJSONArray("bike_xy").getDouble(0);
                bookedBikeLong = json.getJSONArray("bike_xy").getDouble(1);
                callback.bookListener.bookListener(
                        "",
                        bookedBikeId,
                        new LatLng(bookedBikeLat, bookedBikeLong),
                        true);
            } else {
                callback.bookListener.bookListener(
                        String.valueOf(json.getInt("error")),
//                            json.getString("bike_id"),
                        "",
                        new LatLng(0.0,
                                0.0),
                        false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void defaultsResponse(JSONObject json) {
        try {
            JSONObject feeArray = json.getJSONObject("fees");
            ridePrice = feeArray.getDouble("ride_general");
            studentPrice = feeArray.getDouble("ride_student");
            weekendPrice = feeArray.getDouble("ride_weekend");
            minimal = feeArray.getDouble("minimal");
            bookPrice = feeArray.getDouble("book");
            holdPrice = feeArray.getDouble("hold");
            support = json.getString("support");
            JSONObject rateArray = json.getJSONObject("weights");
            JSONObject minsArray = json.getJSONObject("minutes");
            calRate = rateArray.getInt("kcal");
            treeRate = rateArray.getInt("tree");
            bonus = minsArray.getInt("bonus");
            freeMinPrice = minsArray.getInt("free");
            SharedPreferences.Editor editor = mContext.getSharedPreferences("mimo_user_data", Context.MODE_PRIVATE).edit();
            editor.putFloat("ridePrice", (float) ridePrice).apply();
            editor.putFloat("bookPrice", (float) bookPrice).apply();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void enterResponse(JSONObject json, String mode) {
        try {
            Requests.getBikes();
            if (json.getString("status").equals("ok")) {
                if (json.has("sk")) {
                    sessionKey = json.getString("sk");
                }
                if (mode.equals("get-profile")) {
                    getProfileThings(json.getJSONObject("user_data"));
                }
                if (json.has("_id")) {
                    userId = json.getString("_id");
                }
                if (json.has("banking_ok")) {
                    activeOrNo = json.getBoolean("banking_ok");
                }
                if (json.has("profile_ok")) {
                    SplashActivity.profileOk = json.getBoolean("profile_ok");
                }
                if (json.has("verified")) {
                    if (json.getBoolean("verified")) {
                        getProfileThings(json.getJSONObject("user_data"));
                        paired = true;
                        SplashActivity.cardBool = 1;
                        SplashActivity.callback.verifyLoginListener.goToMap();
                    } else {
                        paired = false;
                        SplashActivity.callback.goVerifyListner.goVerify();
                    }
                } else {
                    if (json.has("banking_ok")) {
                        SplashActivity.callback.activeUserListener.activeUser();
                    }
                }
            } else {
                try {
                    SplashActivity.callback.checkNumberListener.checkNumber();
                } catch (UninitializedPropertyAccessException e) {
                    e.printStackTrace();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getProfileThings(JSONObject obj) {
        try {
            SharedPreferences.Editor editor = mContext.getSharedPreferences("mimo_user_data", Context.MODE_PRIVATE).edit();
            editor.putString("phone", userId)
                    .putBoolean("paired", true).apply();
            if (obj.has("name")) {
                if (obj.getJSONObject("name").has("first")) {
                    editor.putString("name", obj.getJSONObject("name").getString("first")).apply();
                } else {
                    editor.putString("name", "").apply();
                }
                if (obj.getJSONObject("name").has("last")) {
                    editor.putString("surname", obj.getJSONObject("name").getString("last")).apply();
                } else {
                    editor.putString("surname", "").apply();
                }
            }
            if (obj.has("sex")) {
                String sex = String.valueOf(obj.getInt("sex"));
                if (sex.equals("0")) {
                    editor.putString("sex", "M").apply();
                } else {
                    editor.putString("sex", "F").apply();
                }
            } else {
                editor.putString("sex", "").apply();
            }
            if (obj.has("birthday")) {
                editor.putString("birthday", obj.getString("birthday")).apply();
            } else {
                editor.putString("birthday", "").apply();
            }
            if (obj.has("email")) {
                editor.putString("email", obj.getString("email")).apply();
            } else {
                editor.putString("email", "").apply();
            }
            if (obj.has("avatar")) {
                String avatar = obj.getString("avatar");
                Requests.getAvatar("avatar", avatar);
                editor.putString("avatar", avatar).apply();
            }
            if (obj.has("minutes")) {
                SplashActivity.freeMinutes = obj.getInt("minutes");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void responseBytes(ByteBuffer bytes, Activity context) {
        SharedPreferences.Editor editor = context.getSharedPreferences("mimo_user_data", Context.MODE_PRIVATE).edit();
        byte[] b = new byte[bytes.remaining()];
        bytes.get(b);
        byte[] zhuk = new byte[4];
        zhuk[0] = (byte) 240;
        zhuk[1] = (byte) 159;
        zhuk[2] = (byte) 144;
        zhuk[3] = (byte) 158;
        String getAvatar = new String(Utils.split(zhuk, b).get(0), StandardCharsets.US_ASCII);
        if (getAvatar.equals("download-image")) {
            String mode = new String(Utils.split(zhuk, b).get(1), StandardCharsets.US_ASCII);
//            userId = new String(Utils.split(zhuk, b).get(2), StandardCharsets.US_ASCII);
            switch (mode) {
                case "avatar": {
                    byte[] bitmapdata = Utils.split(zhuk, b).get(3);
                    editor.putString("imagedata", Base64.encodeToString(bitmapdata, Base64.DEFAULT)).apply();
                    SplashActivity.callback.updatePhotoListener.updatePhoto(bitmapdata);
                    break;
                }
            }
        }
    }
}
