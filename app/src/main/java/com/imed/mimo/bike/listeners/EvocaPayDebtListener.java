package com.imed.mimo.bike.listeners;

public interface EvocaPayDebtListener {
    void evocaLink(String link, String tripId);
}
