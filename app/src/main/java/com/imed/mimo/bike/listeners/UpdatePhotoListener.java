package com.imed.mimo.bike.listeners;


public interface UpdatePhotoListener {
    void updatePhoto(byte[] bitmap);
}
