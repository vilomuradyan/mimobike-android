package com.imed.mimo.bike.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.imed.mimo.bike.R;

import java.util.ArrayList;

import me.grantland.widget.AutofitLayout;

import static com.imed.mimo.bike.activities.SplashActivity.cardDebt;

public class CardAdapter extends RecyclerView.Adapter {

    private Activity mContext;
    private ArrayList<String> cardIds;
    private ArrayList<String> cardMasks;
    private ArrayList<Boolean> cardHistory;
    private ArrayList<String> cardDate;
    private static CardAdapter.ClickListener clickListener;

    public CardAdapter(Activity context, ArrayList<String> cardMasks, ArrayList<String> cardDate, ArrayList<String> cardIds, ArrayList<Boolean> cardHistory) {
        this.cardMasks = cardMasks;
        this.cardIds = cardIds;
        this.cardHistory = cardHistory;
        this.cardDate = cardDate;
        mContext = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View feedView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_recycler_item, parent, false);
        return new FeedViewHolder(feedView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final FeedViewHolder feedHolder = (FeedViewHolder) holder;
        feedHolder.cmask_text.setText(cardMasks.get(position));
        if(position == 0) {
            feedHolder.card_icon_gray.setVisibility(View.GONE);
            feedHolder.card_icon.setVisibility(View.VISIBLE);
        } else {
            feedHolder.remove_card.setVisibility(View.GONE);
            feedHolder.card_icon_gray.setVisibility(View.VISIBLE);
            feedHolder.card_icon.setVisibility(View.GONE);
        }
        feedHolder.card_date_text.setText(cardDate.get(position));
        if(position == 0) {
            feedHolder.v42.setVisibility(View.VISIBLE);
            feedHolder.v6.setVisibility(View.GONE);
            if (cardDebt) {
                feedHolder.unpaid_trips_warn.setVisibility(View.VISIBLE);
            } else {
                feedHolder.unpaid_trips_warn.setVisibility(View.GONE);
            }
            if(cardIds.get(position).equals("")){
                feedHolder.no_active_found.setVisibility(View.VISIBLE);
                feedHolder.autofit124.setVisibility(View.VISIBLE);
                feedHolder.card_icon.setVisibility(View.GONE);
                feedHolder.cmask_text.setVisibility(View.GONE);
                feedHolder.card_date_text.setVisibility(View.GONE);
                feedHolder.unpaid_trips_warn.setVisibility(View.GONE);
                feedHolder.remove_card.setVisibility(View.GONE);
                RelativeLayout.LayoutParams relpar = (RelativeLayout.LayoutParams) feedHolder.v42.getLayoutParams();
                relpar.addRule(RelativeLayout.BELOW, feedHolder.autofit124.getId());
            }
        }
    }

    @Override
    public int getItemCount() {
        return cardIds.size();
    }

    public static class FeedViewHolder extends RecyclerView.ViewHolder  implements  View.OnClickListener{

        private TextView cmask_text;
        private TextView card_date_text;
        private TextView no_active_found;
        private View v6;
        private View v42;
        private AutofitLayout autofit124;
        private Button remove_card;
        private Button unpaid_trips_warn;
        private Button card_icon;
        private Button card_icon_gray;

        FeedViewHolder(View itemView) {
            super(itemView);
            cmask_text = itemView.findViewById(R.id.cmask_text);
            remove_card = itemView.findViewById(R.id.remove_card);
            card_date_text = itemView.findViewById(R.id.card_date_text);
            card_icon = itemView.findViewById(R.id.card_icon);
            autofit124 = itemView.findViewById(R.id.autofit124);
            no_active_found = itemView.findViewById(R.id.no_active_found);
            card_icon_gray = itemView.findViewById(R.id.card_icon_gray);
            unpaid_trips_warn = itemView.findViewById(R.id.unpaid_trips_warn);
            v6 = itemView.findViewById(R.id.v6);
            v42 = itemView.findViewById(R.id.v42);
            remove_card.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListener(CardAdapter.ClickListener clickListener) {
        CardAdapter.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
    }

}