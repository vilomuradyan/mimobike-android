package com.imed.mimo.bike.network

import com.imed.mimo.bike.model.PaymentModel
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST

interface PaymentService {

    @Headers("Content-Type:application/json")
    @POST("payment/register")
    fun payment(@Header("Authorization") authHeader : String,
                @Body payment : PaymentModel
    ) : Observable<PaymentModel>
}