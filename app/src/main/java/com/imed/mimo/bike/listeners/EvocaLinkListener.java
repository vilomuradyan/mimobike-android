package com.imed.mimo.bike.listeners;

public interface EvocaLinkListener {
    void evocaLink(String link);
}
