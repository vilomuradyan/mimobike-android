package com.imed.mimo.bike.listeners;

import org.json.JSONArray;

public interface RideRoutsListener {
    void rideRoutes(JSONArray obj);
}
