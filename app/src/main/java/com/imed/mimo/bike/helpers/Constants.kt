package com.imed.mimo.bike.helpers

import android.util.Patterns
import com.imed.mimo.bike.connections.WebSocket
import org.java_websocket.client.WebSocketClient
import org.json.JSONArray
import java.util.*
import java.util.regex.Pattern

class Constants {
    companion object {
        var BASE_HOST = "http://karlosmarcus.com:63/"
        var SERVICE_UUID: UUID = UUID.fromString("0000fee7-0000-1000-8000-00805f9b34fb")
        var WRITE_UUID: UUID = UUID.fromString("000036f5-0000-1000-8000-00805f9b34fb")
        var READ_UUID: UUID = UUID.fromString("000036f6-0000-1000-8000-00805f9b34fb")
        var NOTIFY_UUID: UUID = UUID.fromString("0000feff-0000-1000-8000-00805f9b34fb")
        var DESCRIPTOR_UUID: UUID = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb")
        val BLE_PERMISSION = 7346
        val location_permission = 925
        val camera_permission = 214
        val camera_permission_passp = 3246
        var login = false
        val call_permission = 325
        val select_photo = 84
        @JvmField
        val storage_permission = 173
        @JvmField
        val storage_permission_route = 2423
        @JvmField
        val sharedRequestCode = 5238
        var reportFragmentCreated = false
        var fromBackgroundStatus = false
        var fromBackgroundBikes = false
        @JvmField
        var internetLost = false
        val emailPattern = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" + "\\@" + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" + "(" + "\\." + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" + ")+"
        )
        var bookStoped = false
        var rideRoutes : JSONArray? = null
        @JvmField
        var sessionKey = ""
        @JvmField
        var token = ""
        @JvmField
        var cannotConnect = false
        @JvmField
        var reconnect = false
        var startedRide = false
        @JvmField var dontDoPush = false
        lateinit var mWebSocketClient: WebSocketClient
        var mWebSocket = WebSocket()
        @JvmField
//        val BASE_HOST1 = "wss://karlosmarkus.com:9663/"
        val BASE_HOST1 = "wss://strongsnakeriver.com:9663/"
        @JvmField
        val ATOKEN = "_iMedMiMoJw7dc3ojV1=b90zX!dt63uwTa8gq4I"
        fun validateEmailAddress(email : String) : Boolean{
            return email.isNotEmpty() && Patterns.EMAIL_ADDRESS.matcher(email).matches()
        }
        @JvmField
        var isRating = false
        const val PLAY_STORE = "PLAY STORE"
        const val APP_STORE = "https://apps.apple.com/us/app/mimo-bike-sharing/id1448465054"
        const val FACEBOOK_LINK = "https://www.facebook.com/mimobikesharing/"
        const val INSTAGRAM_LINK = "https://www.instagram.com/mimobikesharing/"
        const val YOUTUBE_LINK = "https://www.youtube.com/channel/UCQoHwIO3qNlIG3LPMGEjd6Q"
    }
}